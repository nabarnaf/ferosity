# Classes for automated solver of the linearized model from Burtschel et al.

from ferosity.GLOBAL_VARIABLES import *


class LinearBurtschell:
    """
    """
    def __init__(self, name):
        self.name   = name
        self.ys_n   = self.us_n = self.uf_n = self.p_n = None
        self.solver = None

    def setup(self, mesh, physicalParameters, externalLoads, Markers, OutputParameters, SolverClass):
        self.physicalParameters = physicalParameters
        self.externalLoads      = externalLoads
        self.outputParameters   = OutputParameters
        self.markers = Markers

        self.solver = SolverClass(mesh, self.physicalParameters, self.externalLoads, self.markers)
        mpiprint("----- Loaded solver: {}".format(self.solver.name))

        # Initialize solutions
        self.ys_n = Function(self.Vsolid(collapse=True), name = "displacement")
        self.us_n = Function(self.VsolidVel(collapse=True), name = "solid_velocity")
        self.uf_n = Function(self.Vfluid(collapse=True), name = "fluid_velocity")
        self.p_n  = Function(self.Vpressure(collapse=True), name = "pressure")

        if OutputParameters.storeSolutions:
            self.ysList = [self.ys_n.copy(True)]
            self.usList = [self.us_n.copy(True)]
            self.ufList = [self.uf_n.copy(True)]
            self.pList  = [self.p_n.copy(True)]
        if OutputParameters.exportSolutions:
            self.xdmf = XDMFFile("../images/{}/{}.xdmf".format(self.name, OutputParameters.name))
            self.xdmf.parameters["functions_share_mesh"] = True
            self.xdmf.parameters["flush_output"] = True
            self.exportSolution(0)

    def setBC(self, *args):
        self.solver.setBC(*args)

    def solveTimeStep(self, t):
        ys, us, uf, p = self.solver.solveTimeStep(self.ys_n, self.us_n, self.uf_n, self.p_n, t)
        assign(self.ys_n, ys)
        assign(self.us_n, us)
        assign(self.uf_n, uf)
        assign(self.p_n, p)

        if self.outputParameters.storeSolutions:
            self.ysList.append(self.ys_n.copy(True))
            self.usList.append(self.us_n.copy(True))
            self.ufList.append(self.uf_n.copy(True))
            self.pList.append(self.p_n.copy(True))

    def solve(self, printEvery=1):

        mpiprint("----- Solving problem")
        Niter = int((self.physicalParameters.sim_time - self.physicalParameters.t0)/self.physicalParameters.dt)

        from time import time
        for n in range(Niter):
            t = self.physicalParameters.t0+self.physicalParameters.dt*n
            current_t = time()
            self.solveTimeStep(t)
            if n%self.outputParameters.saveEvery==0 and self.outputParameters.exportSolutions:
                self.exportSolution(t+self.physicalParameters.dt)
            if n%printEvery==0:
                mpiprint("----- Solved t={:.2E} in {:.3f}s".format(t, time() - current_t))
        if self.outputParameters.exportSolutions:
            self.xdmf.close()


    def exportSolution(self, t):
        self.xdmf.write(self.ys_n, t)
        self.xdmf.write(self.us_n, t)
        self.xdmf.write(self.uf_n, t)
        self.xdmf.write(self.p_n, t)

    def Vfluid(self, **kwargs):
        return self.solver.Vfluid(**kwargs)

    def Vsolid(self, **kwargs):
        return self.solver.Vsolid(**kwargs)

    def VsolidVel(self, **kwargs):
        return self.solver.VsolidVel(**kwargs)

    def Vpressure(self, **kwargs):
        return self.solver.Vpressure(**kwargs)

    def dim(self):
        return self.solver.V.dim()