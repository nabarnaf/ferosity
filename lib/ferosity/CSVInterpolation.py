from scipy import fmod
from scipy.interpolate import interp1d
import csv

class CSVInterpolation:
    """Class in charge of interpolating values from a CSV file"""
    def __init__(self, csv_filename, tf):
        """
        This class reads a csv file with only two columns (and ignores others of existent).
        """

        self.csv_filename = csv_filename
        self.period = tf
        with open(csv_filename, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            x_vals = []
            y_vals = []
            first = True
            for row in reader:
                if first:
                    self.var_x = row[0]
                    self.var_y = row[1]
                    first = False
                else:
                    x = float(row[0])
                    y = float(row[1])
                    if x > tf:
                        break
                    else:
                        x_vals.append(x)
                        y_vals.append(y)
            self.f = interp1d(x_vals, y_vals, kind='cubic')

    def __call__(self, x):
        """ Class instance evaluation, supports numpy arrays. Note that it is periodic"""

        return self.f(fmod(x, self.period))
