"""
Projection class
"""

import dolfin as df


class Projection:

	def __init__(self, V, type="L2"):
		self.V = V
		self.type = type

		# Initialize solver
		u = df.TrialFunction(V)
		v = df.TestFunction(V)
		A = None
		if type == "L2":
			A = df.assemble(df.inner(u, v) * df.dx)
		elif type == "H1":
			h = df.CellDiameter(V.mesh())
			A = df.assemble(
			    (df.inner(u, v) + h**2 * df.inner(df.grad(u), df.grad(v))) * df.dx)
		self.solver = df.LUSolver(A)

	def project(self, rhs_function):
		v = df.TestFunction(self.V)
		F = None
		if self.type == "L2":
			F = df.assemble(df.inner(rhs_function, v) * df.dx)
		elif self.type == "H1":
			F = df.assemble(df.inner(rhs_function, v) * df.dx)  # + df.inner(df.grad(rhs_function), df.grad(v)))* df.dx)
		sol=df.Function(self.V)
		self.solver.solve(sol.vector(), F)

		return sol
