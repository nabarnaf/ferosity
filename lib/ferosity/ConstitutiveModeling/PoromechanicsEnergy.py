"""
Poromechanics Energy
"""

import dolfin as df


class PoromechanicsEnergy:
    def __init__(self, params, skeleton_energy, volume_energy):
        self.params = params
        self.rhof = params["rhof"]  # Fluid density
        self.phi0_list = params["initial porosities"]  # Initial porosities
        self.phis = sum(self.phi0_list)  # Solid porosity
        self.N_compartments = params["number compartments"]
        self.pressure_model = params["pressure model"]

        self.skeleton_energy = skeleton_energy
        self.volume_energy = volume_energy

    def getPiola(self, F, m):
        """
        Get Piola stress tensor
        """
        F = df.variable(F)

        energy = self.skeleton_energy(F)
        if self.pressure_model == "mono":
            for comp in range(self.N_compartments):
                Jphi = self.m2Jphi(m[comp], self.phi0_list[comp])
                energy += self.volume_energy((df.det(F) - Jphi) /
                                             (1 - self.phi0_list[comp]))
        elif self.pressure_model == "multi":
            Js = df.det(F)
            for comp in range(self.N_compartments):
                Js -= self.m2Jphi(m[comp], self.phi0_list[comp])
            energy += self.volume_energy(Js / self.phis)
        return df.diff(energy, F)

    def getPressure(self, F, m):
        # dim = F.geometric_dimension()
        if self.pressure_model == "mono":
            # p0 = self.getPressureMonoSolid(df.Identity(dim), m0)  # Need to define m0 correctly
            return self.getPressureMonoSolid(F, m)
        elif self.pressure_model == "multi":
            # p0 = self.getPressureMultiSolid(df.Identity(dim), m0)  # Same
            return self.getPressureMultiSolid(F, m)

    def getPressureMonoSolid(self, F, m):
        """
        Get pressure in a given compartment
        """
        m = df.variable(m)

        energy = self.skeleton_energy(F)
        for comp in range(self.N_compartments):
            Jphi = self.m2Jphi(m[comp], self.phi0_list[comp])
            energy += self.volume_energy((df.det(F) - Jphi) /
                                         (1 - self.phi0_list[comp]))
        return df.Constant(self.rhof) * df.diff(energy, m)

    def getPressureMultiSolid(self, F, m):
        m = df.variable(m)

        Js = df.det(F)
        for comp in range(self.N_compartments):
            Js -= self.m2Jphi(m[comp], self.phi0_list[comp])
        energy = self.volume_energy(Js / self.phis)
        return df.Constant(self.rhof) * df.diff(energy, m)

    def m2Jphi(self, m, phi0):
        """
        Converts added mass to Jphi.
        """
        return df.Constant(1 / self.rhof) * m + phi0

    def Jphi2m(self, Jphi, compartment):
        return df.Constant(self.rhof) * (Jphi - self.phi0_list[compartment])

    def getInversePressure(self, J, p, m, compartment):
        if self.pressure_model == "mono":
            return self.inverseMonoPressure(J, p, compartment)
        elif self.pressure_model == "multi":
            return self.inverseMultiPressure(J, p, m, compartment)

    def inverseMonoPressure(self, J, p, compartment):
        """
        Return value of compartment-th m for a given pressure.

        In mono model, Psi = sum_i psi_vol(J - Jphi_i), so that p_i = -der psi_vol(J - Jphi_i).
        """
        phi0 = self.phi0_list[compartment]  # i-th phi0, complement
        JmJphi_i = (1 - phi0) * self.volume_energy.der_inverse(p * (phi0 - 1))

        Jphi_i = J - JmJphi_i
        m_i = self.Jphi2m(Jphi_i, compartment)
        return m_i

    def inverseMultiPressure(self, J, p, m, compartment):
        """
        Return value of compartment-th m for a given pressure.

        In multi model, Psi = psi_vol(Js), so that p_i = -der psi_vol(Js) for all i
        """
        Js = self.phis * self.volume_energy.der_inverse(- p * self.phis)

        Jphi_sum = 0  # The other ones
        for c in range(self.N_compartments):
            if c == compartment:
                continue
            Jphi_sum += self.m2Jphi(m, c)
        Jphi_i = J - Js - Jphi_sum
        return self.Jphi2m(Jphi_i, compartment)


    def gradPressureSemi(self, F, m_comp, m_prev_comp, comp):
        m = df.variable(m_comp)
        m_prev = df.variable(m_prev_comp)

        Js = df.det(F) - self.m2Jphi(m, self.phi0_list[comp])
        Js_prev = df.det(F) - self.m2Jphi(m_prev, self.phi0_list[comp])
        Js_prev = df.variable(Js_prev)
        return -df.diff(df.diff(self.volume_energy(Js_prev), Js_prev), Js_prev) * df.grad(Js)




