"""
Skeleton energies
"""

import dolfin as df


class NeoHookean:
    def __init__(self, mu):
        self.mu = mu

    def __call__(self, F):
        d = F.geometric_dimension()
        return df.Constant(0.5 * self.mu) * (df.inner(F, F) - d)


class CiarletGeymonat:
    def __init__(self, k1, k2):
        self.k1 = k1
        self.k2 = k2

    def __call__(self, F):
        d = F.geometric_dimension()
        C = F.T * F
        J = df.det(F)
        I1, I2, I3 = df.tr(C), 0.5 * (df.tr(C)**2 - df.tr(C * C)), df.det(C)

        return df.Constant(self.k1) * (I1 * I3**(-1.0 / d) - d)**2 + df.Constant(self.k2) * (I2 * I3**(-2.0 / d) - d)**2


class Guccione:
    def __init__(self, f0, s0, n0):

        # All fiber functions should have this method
        self.dim = f0.geometric_dimension()
        def ufl_norm(_x): return df.sqrt(df.dot(_x, _x))
        def normalize(_x): return _x / ufl_norm(_x)

        f0 = normalize(f0)
        s0 = normalize(s0)
        n0 = normalize(n0)
        # Keep normalized fibers
        self.fibers = [f0, s0, n0]

    def __call__(self, F_):

        f0, s0, n0 = self.fibers
        # Setup all relevant constants
        Cg = .88e3   # [Pa]
        bf = 8       # [-]
        bs = 6       # [-]
        bn = 3       # [-]
        bfs = 12     # [-]
        bfn = 3      # [-]
        bsn = 3      # [-]

        J = df.det(F_)

        # Use only isochoric contribution for skeleton energy as det(J^{-1/d}F) = 1.
        F = J**(1 / self.dim) * F_
        # F = F_
        E = 0.5 * (F.T * F - df.Identity(self.dim))

        # Fiber components of tensor
        Eff, Efs, Efn = df.inner(
            E * f0, f0), df.inner(E * f0, s0), df.inner(E * f0, n0)
        Esf, Ess, Esn = df.inner(
            E * s0, f0), df.inner(E * s0, s0), df.inner(E * s0, n0)
        Enf, Ens, Enn = df.inner(
            E * n0, f0), df.inner(E * n0, s0), df.inner(E * n0, n0)

        Q = df.Constant(bf) * Eff**2 \
            + df.Constant(bs) * Ess**2 \
            + df.Constant(bn) * Enn**2 \
            + df.Constant(bfs) * 2.0 * Efs**2 \
            + df.Constant(bfn) * 2.0 * Efn**2 \
            + df.Constant(bsn) * 2.0 * Esn**2
        W = 0.5 * df.Constant(Cg) * (df.exp(Q) - 1)
        return W
