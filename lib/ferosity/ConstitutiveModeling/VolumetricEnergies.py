"""
Volumetric energies.
"""

import dolfin as df


class LinpLog:
    def __init__(self, ks):
        self.ks = ks

    def __call__(self, x):
        return df.Constant(0.5 * self.ks) * (x - 1 - df.ln(x))

    def der_inverse(self, y):
        """
        Invert the derivative. In this case
        linlog(x) = ks (x - 1 - ln x), so
        der(x) = ks (1 - 1/x), and thus
        der / ks - 1 = -1/x so 
        x = 1 / (1 - der / ks)
        """
        return 1 / (1 - y / df.Constant(self.ks))

    def der_second(self, x):
        return df.Constant(self.ks) / (x**2)


class LinxLog:
    def __init__(self, ks):
        self.ks = ks

    def __call__(self, x):
        return df.Constant(0.5 * self.ks) * (x - 1) * df.ln(x)

    def der_inverse(self, y):
        """
        Invert the derivative. In this case
        linlog(x) = ks (x - 1 - ln x), so
        der(x) = ks (1 - 1/x), and thus
        der / ks - 1 = -1/x so 
        x = 1 / (1 - der / ks)
        """
        return 1 / (1 - y / df.Constant(self.ks))

    def der_second(self, x):
        return -df.Constant(self.ks) / x


class Quadratic:
    def __init__(self, ks):
        self.ks = ks

    def __call__(self, x):
        return df.Constant(0.5 * self.ks) * (x - 1) ** 2

    def der_inverse(self, y):
        """
        Invert the derivative. 
        """
        return y / df.Constant(self.ks) + 1
