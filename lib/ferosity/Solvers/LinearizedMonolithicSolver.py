from ferosity.GLOBAL_VARIABLES import *
from ferosity.Solvers.AbstractSolver import AbstractSolver
from ferosity.Assemblers.LinearizedMonolithicAssembler import LinearizedMonolithicAssembler

class LinearizedMonolithicSolver(AbstractSolver):
    def __init__(self, mesh, PhysicalParameters, ExternalLoads, Markers):
        super().__init__("Monolithic Solver")

        mpiprint("----- Solver setup")
        self.mesh  = mesh
        self.PhysicalParameters = PhysicalParameters
        self.ExternalLoads      = ExternalLoads

        # Functional setting
        P = PhysicalParameters
        cell = mesh.ufl_cell()
        ys_el = VectorElement('CG', cell, P.ys_degree)
        uf_el = VectorElement('CG', cell, P.uf_degree)
        us_el = VectorElement('CG', cell, P.us_degree)
        p_el  = FiniteElement('CG', cell, P.p_degree)

        # Order is important! y, v, u, p
        self.ys_index = 0
        self.vs_index = 1
        self.uf_index = 2
        self.p_index  = 3
        mix_el = MixedElement(ys_el, us_el, uf_el, p_el)

        self.V = FunctionSpace(mesh, mix_el)
        self.assembler = LinearizedMonolithicAssembler(mesh, PhysicalParameters, ExternalLoads, Markers)

        mpiprint("----- Solver setup OK")


    def Vfluid(self, collapse=False):
        S = self.V.sub(self.uf_index)
        return S.collapse() if collapse else S

    def Vsolid(self, collapse=False):
        S = self.V.sub(self.ys_index)
        return S.collapse() if collapse else S

    def VsolidVel(self, collapse=False):
        S = self.V.sub(self.vs_index)
        return S.collapse() if collapse else S

    def Vpressure(self, collapse=False):
        S = self.V.sub(self.p_index)
        return S.collapse() if collapse else S

    def setBC(self, bc):
        self.bc = bc

    def applyBC(self, A, F, t):
        for b in self.bc(t):
            b.apply(A, F)

    def solveTimeStep(self, ys_n, us_n, uf_n, p_n, t):

        A = self.assembler.getBilinearForm(self.V, t)
        F = self.assembler.getLoad(self.V, t, ys_n, us_n, uf_n, p_n)

        self.applyBC(A, F, t)

        sol = Function(self.V)
        solve(A, sol.vector(), F)

        return sol.sub(0), sol.sub(1), sol.sub(2), sol.sub(3)