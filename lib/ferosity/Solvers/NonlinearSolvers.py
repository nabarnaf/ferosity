import numpy as np
import dolfin as df
from mpi4py import MPI
from petsc4py import PETSc
from time import perf_counter as time
from ferosity.AndersonAcceleration import AndersonAcceleration


class PreconditionerWoodburySchur(object):

    def __init__(self, M, is_0, is_1, type="full"):
        self.M = M
        self.is_0 = is_0
        self.is_1 = is_1
        self.type = type

        assert type in ["lower", "diag", "upper",
                        "full"], "Woodbury Schur type must be one of diag|lower|upper|full"

    def apply_lower_inv(self, x0, x1, y0, y1):
        self.ksp_0.solve(x0, self.temp0)
        self.M10.mult(self.temp0, y1)
        # Out
        y1.aypx(-1, x1)
        x0.copy(y0)

    def apply_upper_inv(self, x0, x1, y0, y1):
        self.M01.mult(x1, self.temp0)
        self.ksp_0.solve(self.temp0, y0)
        # Out
        x1.copy(y1)
        y0.aypx(-1, x0)

    def apply_diag_inv(self, x0, x1, y0, y1):
        # Solve first
        self.ksp_0.solve(x0, y0)

        # Solve second using Woodbury
        # we do inv(S)*x1 = inv(M11)*(x1 - M10*inv(-M00+M01*inv(M11)*M10)*M01*inv(M11)*x1)

        # This results in temp0
        self.ksp_1.solve(x1, self.temp1)
        self.M01.mult(self.temp1, self.temp0)

        # This results in temp1
        # first one
        self.M00.mult(self.temp0, self.temp00)
        # second one
        self.M10.mult(self.temp0, self.temp1)
        self.ksp_1.solve(self.temp1, self.temp11)
        self.M01.mult(self.temp11, self.temp0)
        self.temp0.axpy(-1, self.temp00)
        # left of parenthesis
        self.M10.mult(self.temp0, self.temp1)
        self.ksp_1.solve(self.temp1, self.temp11)

        # Final inv(M11)x1 - t11
        self.ksp_1.solve(x1, y1)
        y1.axpy(-1, self.temp11)
        y1.scale(-1)  # Flip sign for positiveness

    def setUp(self, pc):

        # Allocate matrices
        self.M00 = self.M.createSubMatrix(self.is_0, self.is_0)
        # self.M00.setBlockSize(3)
        self.M01 = self.M.createSubMatrix(self.is_0, self.is_1)
        self.M10 = self.M.createSubMatrix(self.is_1, self.is_0)
        self.M11 = self.M.createSubMatrix(self.is_1, self.is_1)

        Mdiag = self.M11.copy()
        Mdiag.zeroEntries()
        Mdiag.setDiagonal(self.M11.getDiagonal())
        self.Mschur = -self.M00 + self.M01 * Mdiag * self.M10

        # Create solvers
        self.ksp_0 = PETSc.KSP().create()
        self.ksp_0.setOptionsPrefix("ws_0_")
        self.ksp_0.setOperators(self.M00, self.M00)
        self.ksp_0.setFromOptions()

        self.ksp_1 = PETSc.KSP().create()
        self.ksp_1.setOptionsPrefix("ws_1_")
        self.ksp_1.setOperators(self.M11, self.M11)
        self.ksp_1.setFromOptions()

        self.ksp_schur = PETSc.KSP().create()
        self.ksp_schur.setOptionsPrefix("ws_schur_")
        self.ksp_schur.setOperators(self.Mschur, self.Mschur)
        self.ksp_schur.setFromOptions()

        # Create vectors
        self.x0 = PETSc.Vec().create()
        self.x1 = PETSc.Vec().create()
        self.y0 = PETSc.Vec().create()
        self.y1 = PETSc.Vec().create()
        self.temp0 = PETSc.Vec().create()
        self.temp00 = PETSc.Vec().create()
        self.temp000 = PETSc.Vec().create()
        self.temp0000 = PETSc.Vec().create()
        self.temp1 = PETSc.Vec().create()
        self.temp11 = PETSc.Vec().create()
        self.temp111 = PETSc.Vec().create()
        self.temp1111 = PETSc.Vec().create()
        self.vecs_allocated = False

    def apply(self, pc, x, y):
        # Result is y = A^{-1}x, i.e. solves Ay=x
        # y.getSubVector(self.is_0, self.y0)
        # y.getSubVector(self.is_1, self.y1)
        # x.getSubVector(self.is_0, self.x0)
        # x.getSubVector(self.is_1, self.x1)

        # if not self.vecs_allocated:
        #     self.temp0 = self.x0.copy()
        #     self.temp00 = self.x0.copy()
        #     self.temp000 = self.x0.copy()
        #     self.temp0000 = self.x0.copy()
        #     self.temp1 = self.x1.copy()
        #     self.temp11 = self.x1.copy()
        #     self.temp111 = self.x1.copy()
        #     self.temp1111 = self.x1.copy()
        #     self.vecs_allocated = True

        # if self.type == "diag":
        #     self.apply_diag_inv(self.x0, self.x1, self.y0, self.y1)
        # elif self.type == "lower":
        #     self.apply_lower_inv(self.x0, self.x1, self.temp000, self.temp111)
        #     self.apply_diag_inv(self.temp000, self.temp111, self.y0, self.y1)
        # elif self.type == "upper":
        #     self.apply_diag_inv(self.x0, self.x1, self.temp000, self.temp111)
        #     self.apply_upper_inv(self.temp000, self.temp111, self.y0, self.y1)
        # else:  # type == "full"
        #     self.apply_lower_inv(self.x0, self.x1, self.temp000, self.temp111)
        #     self.apply_diag_inv(self.temp000, self.temp111, self.temp0000, self.temp1111)
        #     self.apply_upper_inv(self.temp0000, self.temp1111, self.y0, self.y1)
        #
        # x.restoreSubVector(self.is_0, self.x0)
        # x.restoreSubVector(self.is_1, self.x1)
        # y.restoreSubVector(self.is_0, self.y0)
        # y.restoreSubVector(self.is_1, self.y1)
        # Solve second using Woodbury
        # we do inv(S)*x1 = inv(M11)*(x1 - M10(-M00+M01*inv(M11)*M10)*M01*inv(M11)*x1)

        if not self.vecs_allocated:
            self.temp1 = x.copy()
        self.temp0 = self.ksp_0.getOperators()[0].getVecRight()
        self.ksp_1.solve(x, self.temp1)
        self.M01.mult(self.temp1, self.temp0)

        # This results in temp1
        # first one
        #self.temp00 = self.temp0.copy()
        #self.M00.mult(self.temp0, self.temp00)
        # second one
        #self.M10.mult(self.temp0, self.temp1)
        #self.temp11 = self.temp1.copy()
        #self.ksp_1.solve(self.temp1, self.temp11)
        #self.M01.mult(self.temp11, self.temp0)
        #self.temp0.axpy(-1, self.temp00)
        if not self.vecs_allocated:
            self.temp00 = self.temp0.copy()
        self.ksp_schur(self.temp0, self.temp00)
        # left of parenthesis
        self.M10.mult(self.temp00, self.temp1)
        if not self.vecs_allocated:
            self.temp11 = self.temp1.copy()
            self.vecs_allocated = True
        self.ksp_1.solve(self.temp1, self.temp11)

        # Final inv(M11)x1 - t11
        self.ksp_1.solve(x, y)
        y.axpy(-1, self.temp11)
        y.scale(-1)  # TEST PETSc uses Schut with -1 scale for positivity


class Newton:

    def __init__(self, jac0, atol, rtol, maxit, alpha, verbose=False, inexact=False):
        self.atol = atol
        self.rtol = rtol
        self.maxit = maxit
        self.alpha = alpha
        self.verbose = verbose
        self.inexact = inexact
        ksp = PETSc.KSP().create()
        ksp.setType('gmres')
        ksp.setOperators(jac0)
        pc = ksp.getPC()
        pc.setType('hypre')
        if inexact:
            self.rtol_linear = 1e-2
            ksp.setTolerances(1e-2, 0.0, 1e20, 1000)
        else:
            ksp.setTolerances(1e-8, 1e-10, 1e20, 1000)
        pc.setFromOptions()
        ksp.setFromOptions()
        self.ksp = ksp
        self.do_fieldsplit = False
        self.sub_allocated = False

    def apply_bcs(self, obj, bcs):
        if bcs:
            for bb in bcs:
                bb.apply(obj)

    def set_mechanics_fieldsplit_pc(self, V0, V1):
        self.V0 = V0
        self.V1 = V1
        self.do_fieldsplit = True

    def set_mechanics_fieldsplit_pc_setup(self, jac):
        dofmap_s = self.V0.dofmap().dofs()
        dofmap_p = self.V1.dofmap().dofs()
        is_s = PETSc.IS().createGeneral(dofmap_s)
        is_p = PETSc.IS().createGeneral(dofmap_p)
        self.is_s = is_s
        self.is_p = is_p

        self.ksp = PETSc.KSP().create()
        self.ksp.setOperators(jac.mat())
        self.ksp.setType("fgmres")
        pc = self.ksp.getPC()
        pc.setType('fieldsplit')
        self.ksp.setFromOptions()
        pc.setFromOptions()
        pc.setFieldSplitIS((None, is_s), (None, is_p))
        pc.setUp()  # Must be called after set from options
        ksps = pc.getFieldSplitSubKSP()

        # Then setup everything
        for ksp in ksps:
            ksp.setFromOptions()
            pc_inner = ksp.getPC()
            pc_inner.setFromOptions()
            pc_inner.setUp()

    def solve(self, sol, res_fun, jac_fun, bcs=None, bs=None):

        if bcs:
            for bb in bcs:
                bb.apply(sol)
                bb.homogenize()
        t0 = time()
        err_abs = 1.0
        err_rel = 1.0
        it = 0
        du = sol.copy()
        du.zero()
        res = df.PETScVector()
        jac = df.PETScMatrix()

        res_fun(res)
        self.apply_bcs(res, bcs)
        jac_fun(jac)
        self.apply_bcs(jac, bcs)

        res0 = res.norm('l2')
        err_abs = res0
        err_rel = 1
        if res0 < 1e-12:
            res0 = 1
        total_krylov = 0
        if self.inexact:
            rtol = self.rtol_linear
            res_old = res0
        rtol_upper_bound = 1e-2
        while err_abs > self.atol and err_rel > self.rtol and it < self.maxit:
            it += 1

            self.ksp.setOperators(jac.mat())
            if self.do_fieldsplit:
                self.set_mechanics_fieldsplit_pc_setup(jac)
            if self.inexact:
                PHI = (1 + np.sqrt(5))/2
                if rtol**PHI > 0.1:
                    rtol = max(rtol, rtol**PHI)
                if rtol >= 1:
                    rtol = rtol_upper_bound
                self.ksp.setTolerances(rtol, 0.0, 1e14, 1000)

            self.ksp.setFromOptions()
            self.ksp.setUp()
            self.ksp.solve(res.vec(), du.vec())
            krylov_its = self.ksp.getIterationNumber()
            # krylov_its = df.solve(jac, du, res, 'gmres', 'hypre_amg')
            # solve(A, du, b)
            # du.apply("")
            sol.vec().axpy(-1, du.vec())  # we solve jac du = - res
            sol.apply("")
            res_fun(res)
            self.apply_bcs(res, bcs)
            jac_fun(jac)
            if bs:
                jac.mat().setBlockSize(bs)
            self.apply_bcs(jac, bcs)
            err_abs = res.norm('l2')
            err_rel = err_abs/res0

            # Update tolerance for inexact
            if self.inexact:
                rtol = 0.1 * abs(self.ksp.getResidualNorm() - err_abs) / \
                    res_old  # Einstat-Walker Choice 1 with a factor
                rtol = min(rtol, rtol_upper_bound)  # Never go over upper bound
                res_old = err_abs
            if self.verbose and MPI.COMM_WORLD.rank == 0:
                print('it {}, err abs = {:.3e}  err rel = {:.3e} in {} GMRES iterations'.format(
                    it, err_abs, err_rel, krylov_its), flush=True)
            total_krylov += krylov_its
            if min(err_abs, err_rel) > 1e14 or np.isnan(err_abs) or np.isnan(err_rel):
                if MPI.COMM_WORLD.rank == 0:
                    print("\t Newton diverged")
                return False
        if(MPI.COMM_WORLD.rank == 0 and it < self.maxit):
            if it > 0:
                avg_its = total_krylov / it
            else:
                avg_its = 0
            print("\t Newton converged in {} nonlinear its, {} total krylov its, {} avg krylov its, {:.3f}s".format(
                it, total_krylov, avg_its, time() - t0), flush=True)
        if it == self.maxit:
            return False
        else:
            return True


class BFGS:
    def __init__(self, jac, atol, rtol, maxit, alpha, verbose=False, anderson_depth=0, LM=False, LM_order=10, inexact=False, rtol_inexact=1e-2):
        self.jac = jac
        self.atol = atol
        self.rtol = rtol
        self.maxit = maxit
        self.alpha = alpha
        self.verbose = verbose
        self.anderson_depth = anderson_depth
        self.LM = LM  # Low memory
        self.LM_order = LM_order  # Number of iterations to be used with LM
        self.inexact = inexact
        ksp = PETSc.KSP().create()
        if inexact:
            ksp.setType('gmres')
            ksp.setTolerances(rtol_inexact, 1e-8, 1e20, 50)
        else:
            ksp.setType('preonly')
        ksp.setOperators(jac)
        pc = ksp.getPC()
        pc.setType('hypre')
        ksp.setFromOptions()
        pc.setFromOptions()
        self.ksp = ksp
        self.b = None
        self.sks = []
        self.yks = []
        self.rhoks = []

        def H0(vec, k):
            assert k == 0, "Preconditioner in BFGS must me called at last level in recursion always"
            # return 0.1*vec
            if not self.b:
                self.b = vec.copy()
            vec.copy(self.b)
            self.ksp.solve(self.b, vec)
            # return vec
        self.Hs = [H0]
        self.k = 0

    def set_mechanics_fieldsplit_pc(self, V0, V1):
        dofmap_s = V0.dofmap().dofs()
        dofmap_p = V1.dofmap().dofs()
        is_s = PETSc.IS().createGeneral(dofmap_s)
        is_p = PETSc.IS().createGeneral(dofmap_p)

        ksp = PETSc.KSP().create()
        ksp.setOperators(self.jac, self.jac)
        self.ksp = ksp
        if self.inexact:
            self.ksp.setType("fgmres")
        pc = self.ksp.getPC()
        pc.setType('fieldsplit')
        self.ksp.setFromOptions()
        pc.setFromOptions()
        pc.setFieldSplitIS((None, is_s))
        pc.setFieldSplitIS((None, is_p))
        pc.setUp()  # Must be called after set from options
        ksps = pc.getFieldSplitSubKSP()
        for ksp in ksps:
            ksp.setFromOptions()

    def update(self, sk, yk):
        self.sks.append(sk)
        self.yks.append(yk)
        self.rhoks.append(1 / (yk.dot(sk)))

        if self.LM:
            # Truncate iterations
            if len(self.sks) > self.LM_order:
                self.sks.pop(0)
                self.yks.pop(0)
                self.rhoks.pop(0)
            assert len(self.sks) <= self.LM_order, "More previous iterations than allowed."
        else:
            # Add recursive functions
            def Hk(vec, k):
                rhok = self.rhoks[k-1]
                skdvec = self.sks[k-1].dot(vec)
                vec.axpy(-rhok*skdvec, self.yks[k-1])
                self.Hs[k-1](vec, k-1)
                vec.axpy(-rhok * self.yks[k-1].dot(vec), self.sks[k-1])
                vec.axpy(rhok * skdvec, self.sks[k-1])
            self.Hs.append(Hk)

    def apply(self, vec):
        if self.LM:
            iters = len(self.sks)  # Number of iterations
            self.alphas = np.zeros(iters)
            self.betas = np.zeros(iters)

            # Use two-loop recursion
            for ii in range(iters):
                jj = iters - ii - 1  # invert direction
                self.alphas[jj] = self.rhoks[jj] * vec.dot(self.sks[jj])
                vec.axpy(-self.alphas[jj], self.yks[jj])
            self.Hs[0](vec, 0)
            for ii in range(iters):
                self.betas[ii] = self.rhoks[ii] * self.yks[ii].dot(vec)
                vec.axpy(self.alphas[ii] - self.betas[ii], self.sks[ii])
        else:
            return self.Hs[-1](vec, len(self.Hs)-1)

    def apply_bcs(self, obj, bcs):
        if bcs:
            for bb in bcs:
                bb.apply(obj)

    def apply_bc_petsc(self, vec, bcs):
        if bcs:
            for bb in bcs:
                vals = bb.get_boundary_values()
                for dof in vals:
                    vec.setValue(dof, vals[dof])

    def solve(self, sol, res_fun, jac_fun=None, bcs=None):
        t0 = time()
        # If there are BCs, homogenize them after initializing solution
        if bcs:
            for bb in bcs:
                bb.apply(sol)
                bb.homogenize()
        soln = sol.vec().copy()  # resn is PETSc vector

        res = df.PETScVector()
        res_fun(res)  # Initial residual
        self.apply_bcs(res, bcs)
        resn = res.vec().copy()
        anderson = AndersonAcceleration(self.anderson_depth)
        error0 = res.norm('l2')
        error_abs = error0
        if error0 < 1e-12:
            error0 = 1
        error_rel = error_abs / error0
        it = 0
        total_krylov = 0
        while error_abs > self.atol and error_rel > self.rtol and it < self.maxit:
            self.apply(res.vec())
            # res.apply("")
            krylov_its = self.ksp.getIterationNumber()

            do_LS = True  # TODO: Line search
            if do_LS:
                pass
            sol.vec().axpy(-self.alpha, res.vec())
            # anderson.get_next_vector(sol.vec())
            sol.apply("")
            res_fun(res)
            self.apply_bcs(res, bcs)

            # They must be allocated as the iteration count is unkown
            self.update(-soln + sol.vec(),  -resn + res.vec())  # sk, yk

            sol.vec().copy(soln), res.vec().copy(resn)
            error_abs = res.norm('l2')
            error_rel = error_abs / error0
            if(MPI.COMM_WORLD.rank == 0 and self.verbose):
                print("it {}, err_abs={:.3e}, err_rel={:.3e} in {} GMRES iterations".format(
                    it, error_abs, error_rel, krylov_its), flush=True)
            it += 1
            total_krylov += krylov_its
            if min(error_abs, error_rel) > 1e14 or np.isnan(error_abs) or np.isnan(error_rel):
                if MPI.COMM_WORLD.rank == 0:
                    print("\t BFGS diverged")
                return False
        if(MPI.COMM_WORLD.rank == 0 and it < self.maxit):

            if it > 0:
                avg_its = total_krylov/it
            else:
                avg_its = 0
            print("\t BFGS converged in {} nonlinear its, {} total krylov its, {} avg krylov its, {:.3f}s".format(
                it, total_krylov, avg_its, time() - t0), flush=True)
        if it == self.maxit:
            return False
        else:
            return True
