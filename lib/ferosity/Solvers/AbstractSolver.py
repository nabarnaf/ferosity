#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 09:54:02 2019

@author: barnafi
"""

from abc import ABC, abstractmethod

class AbstractSolver(ABC):
    # Abstract solver
    def __init__(self, name):
        self.name = name
         
    def Vfluid(self, collapse=False):
        pass
    
    def Vsolid(self, collapse=False):
        pass
    
    def VsolidVel(self, collapse=False):
        pass
    
    def Vpressure(self, collapse=False):
        pass
    
    def solveTimestep(self, ys_n, us_n, uf_n, p_n, t):
        pass