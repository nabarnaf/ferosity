#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 18:16:28 2018

@author: barnafi
"""
from dolfin import *
import numpy as np
DEBUG = False

def ufl_norm(_A):
    return sqrt(inner(_A, _A))

# Test for marks, they work OK
#ds_test = Measure('ds', domain=mesh, subdomain_data=markers)
#for i in (LEFT, RIGHT, TOP, BOTTOM):
#    x = SpatialCoordinate(mesh)
#    print( assemble(x[0]*x[1]*ds_test(i)))
#
##     Print all vertices that belong to the boundary parts
#for x in mesh.coordinates():
#    if left.inside(x, True): print('%s is on x = 0' % x)
#    if right.inside(x, True): print('%s is on x = l' % x)
#    if top.inside(x, True): print('%s is on y = l' % x)
#    if bottom.inside(x, True): print('%s is on y = 0' % x)


def setNodalValues(u_new, u_old, V=None):
    """
    Grab a function defined on one domain and create another one defined on
    another domain with the same nodal values via dof matching. An optional
    function space can be given in case dofs are ot directly matchable
    """
    if V:
        u_temp = project(u_old, V)
        u_new.vector().set_local(u_temp.vector())
    else:
        u_new.vector().set_local(u_old.vector())


class ALELinearForm:

    def __init__(self, mesh):
        self.mesh = mesh
        self.bc = []
        self.displacement_forms = []
        self.displacement_loads = []

    def addDisplacementAndForm(self, disp, form):
        self.displacement_forms.append((disp, form))


    def addBC(self, bc):
        self.bc.append(bc)

    def assemble(self):
        As = []
        bs = []
        disp = Function(VectorFunctionSpace(self.mesh, 'CG', 1))
        for d, F in self.displacement_forms:
            disp.interpolate(d)
            coor = self.mesh.coordinates().copy()
            ALE.move(self.mesh, disp)
            _A = lhs(F)
            _b = rhs(F)
            if not _A.empty():
                _A = assemble(_A)
                if _A == 0.0:
                    pass
                else:
                    As.append(_A)
            if not _b.empty():
                _b = assemble(_b)
                bs.append(_b)
            self.mesh.coordinates()[:] = coor

        A = As[0]
        for i in range(1, len(As)):
            A += As[i]
        b = bs[0]
        for i in range(1, len(bs)):
            b += bs[i]
        return A, b

    def solve(self, Sol):
        if DEBUG: print ("Solving linear ALE problem")
        A, b = self.assemble()
        for bc in self.bc:
            bc.apply(A, b)
        solve(A, Sol.vector(), b)

class ALENonLinearForm:

    def __init__(self, mesh, V):
        self.mesh = mesh
        self.V = V
        self.displacements = []
        self.displacement_forms = []
        self.displacement_loads = []
        self.bc = []

    def addDisplacementAndForm(self, disp, form):
        self.displacement_forms.append((disp, form))


    def addBC(self, bc):
        self.bc.append(bc)

    def assembleNewton(self, U):
        As = []
        bs = []
        disp = Function(VectorFunctionSpace(self.mesh, 'CG', 1))
        dU = TrialFunction(self.V)
        import dolfin as df
        for d, F in self.displacement_forms:
            disp.interpolate(d)
            coor = self.mesh.coordinates().copy()
            ALE.move(self.mesh, disp)
            dF = derivative(F, U, dU)
            if not dF.empty():
                _A = assemble(dF)
                if _A == 0.0:
                    pass
                else:
                    As.append(_A)
            if not F.empty():
                _b = assemble(-F)
                bs.append(_b)
            self.mesh.coordinates()[:] = coor
        A = As[0]
        for i in range(1, len(As)):
            A += As[i]
        b = bs[0]
        for i in range(1, len(bs)):
            b += bs[i]
        return A, b # dF and -F

    def solve(self, U, maxiter=50, tol_rel=1e-12):
        if DEBUG: print("Solving nonlinear ALE problem")
        it = 0
        err = 1

        dU = Function(self.V)
        for b in self.bc:
            b.apply(U.vector())
        while it < maxiter and err > tol_rel:
            dF, mF = self.assembleNewton(U)
            if self.bc:
                for b in self.bc:
                    new_bc = DirichletBC(b)
                    new_bc.homogenize()
                    new_bc.apply(dF, mF)
            solve(dF, dU.vector(), mF)
            U.vector()[:] += dU.vector()
            err = norm(dU.vector(), 'linf')#/norm(U.vector(), 'linf')
            it += 1
            if DEBUG:
                print( "Newton step {}, error={}".format(it, err))
        if np.isnan(err) or err>1:
            raise ValueError("Newton diverged, error={}".format(err))