# -*- coding: utf-8 -*-

from ferosity.GLOBAL_VARIABLES import *
from ferosity.Solvers.NonlinearSolvers import BFGS, Newton
from petsc4py import PETSc
from mpi4py import MPI


class MechanicsSolver:
    """
    """

    def __init__(self, name):
        self.name = name
        self.ys_n = self.ys_nn = None
        self.bcs = None

    def setup(self, mesh, physicalParametersSolid, Markers, OutputParameters):
        self.physicalParameters = physicalParametersSolid
        self.outputParameters = OutputParameters
        self.markers = Markers

        self.mesh = mesh
        self.V = VectorFunctionSpace(mesh, 'CG', self.physicalParameters.ys_degree)

        # Initialize solutions
        self.ys_n = Function(self.V, name="displacement")
        self.ys_nn = self.ys_n.copy(True)

        if OutputParameters.storeSolutions:
            self.ysList = [self.ys_n.copy(True)]
            self.usList = [self.us_n.copy(True)]
            self.ufList = [self.uf_n.copy(True)]
            self.pList = [self.p_n.copy(True)]
        if OutputParameters.exportSolutions:
            self.xdmf = XDMFFile("images/{}/{}.xdmf".format(self.name, OutputParameters.name))
            self.exportSolution(0)

        # Setup measures
        self.dx = Measure('dx', domain=mesh, metadata={'optimize': True})
        self.dx = self.dx(degree=4)
        self.ds = Measure('ds', domain=mesh, subdomain_data=Markers.markers,
                          metadata={'optimize': True})
        dsEmpty = self.ds(Markers.NONE)
        # self.dSN = sum([self.ds(i) for i in Markers.neumannMarkers], dsEmpty)
        # self.dSRob = sum([self.ds(i) for i in Markers.robinMarkers], dsEmpty)
        # Hard coded. EPI, ENDO, BASE = 10, 20, 50
        # Hard coded due to strange bugs when sum with empty measure
        self.dSN = self.ds(20) + self.ds(50)
        self.dSRob = self.ds(10)  # Same here
        self.output = {"t": [], "nl_its": [], "l_its": [], "solution_time": []}

    def setBC(self, *args):
        self.bcs = args  # tuple by construction

    def solveTimeStep(self, t, method="Newton", time_update=True):

        Sol = Function(self.V)
        Sol.assign(self.ys_n)
        F = self.generateForm(Sol, t)

        class SNESProblem():
            # No bcs as we use only Robin for simplicity
            def __init__(self, FF, uu):
                V = uu.function_space()
                ddu = TrialFunction(V)
                self.L = FF
                self.a = derivative(FF, uu, ddu)
                self.u = uu

            def F(self, snes, xx, FF):
                xx = PETScVector(xx)
                FF = PETScVector(FF)
                xx.vec().copy(self.u.vector().vec())
                self.u.vector().apply("")
                assemble(self.L, tensor=FF)

            def J(self, snes, xx, JJ, PP):
                JJ = PETScMatrix(JJ)
                JJ.mat().setBlockSize(3)
                xx.copy(self.u.vector().vec())
                self.u.vector().apply("")
                assemble(self.a, tensor=JJ)

        problem = SNESProblem(F, Sol)
        from time import perf_counter as time
        t0 = time()

        snes = PETSc.SNES().create(MPI.COMM_WORLD)
        res_vec = PETScVector()  # same as b = PETSc.Vec()
        assemble(F, tensor=res_vec)
        snes.setFunction(problem.F, res_vec.vec())
        Jac = derivative(F, Sol)
        jac = PETScMatrix()
        assemble(Jac, tensor=jac)
        jac.mat().setBlockSize(3)
        snes.setJacobian(problem.J, jac.mat())
        snes.setFromOptions()
        snes.ksp.setFromOptions()
        snes.solve(None, problem.u.vector().vec())

        if MPI.COMM_WORLD.rank == 0:
            nl_its = snes.getIterationNumber()
            l_its = 0
            if method != "BFGS-inexact":
                l_its = snes.getLinearSolveIterations()
            else:
                # BFGS doesn't add linear iterations, but this is a good estimate
                # as iteration numbers present little to no variation
                l_its = snes.ksp.getIterationNumber() * snes.getIterationNumber()
            sol_time = time() - t0
            print("--- \tConverged in: {:3.0f} nonlinear its, {:3.0f} linear its, {:3.2f}s seconds".format(
                nl_its, l_its, sol_time), flush=True)
            if MPI.COMM_WORLD.rank == 0:
                self.output["t"].append(t)
                self.output["solution_time"].append(sol_time)
                self.output["l_its"].append(l_its)
                self.output["nl_its"].append(nl_its)

        if time_update:
            self.ys_nn.assign(self.ys_n)
            self.ys_n.assign(Sol)
        return snes.converged

    def solve(self, printEvery=1):

        mpiprint("----- Solving problem")
        Niter = int(self.physicalParameters.Niter)

        from time import time
        for n in range(Niter):
            t = self.physicalParameters.t0+self.physicalParameters.dt*n
            current_t = time()
            mpiprint("----- Solving t={:.5f}".format(t))
            self.solveTimeStep(t)
            if n % self.outputParameters.saveEvery == 0 and self.outputParameters.exportSolutions:
                self.exportSolution(t+self.physicalParameters.dt)
            if n % printEvery == 0:
                mpiprint("----- Solved t={:.5f} in {:.2f}s".format(t, time() - current_t))
        if self.outputParameters.exportSolutions:
            self.xdmf.close()

    def generateForm(self, Sol, t):

        ys = Sol
        ws = TestFunction(self.V)
        dt = Constant(self.physicalParameters.dt)
        ys_n = self.ys_n
        ys_nn = self.ys_nn

        # Auxiliary variables
        idt = 1/dt
        us = idt * (ys - ys_n)
        us_n = idt * (ys_n - ys_nn)

        F = Identity(3) + grad(ys)
        F = variable(F)
        J = det(F)
        Fbar=J**(-1./3.) * F

        # Usyk,. mc Culloch 2002
        Cg = .88e3   # [Pa]
        bf = 8       # [-]
        bs = 6       # [-]
        bn = 3       # [-]
        bfs = 12      # [-]
        bfn = 3       # [-]
        bsn = 3       # [-]
        k = self.physicalParameters.ks

        E = 0.5*(Fbar.T*Fbar - Identity(3))
        f0, s0, n0 = self.getFibers()
        Eff, Efs, Efn = inner(E*f0, f0), inner(E*f0, s0), inner(E*f0, n0)
        Esf, Ess, Esn = inner(E*s0, f0), inner(E*s0, s0), inner(E*s0, n0)
        Enf, Ens, Enn = inner(E*n0, f0), inner(E*n0, s0), inner(E*n0, n0)

        Q = Constant(bf) * Eff**2 \
            + Constant(bs) * Ess**2 \
            + Constant(bn) * Enn**2 \
            + Constant(bfs) * 2.0 * Efs**2 \
            + Constant(bfn) * 2.0 * Efn**2 \
            + Constant(bsn) * 2.0 * Esn**2
        WP = 0.5*Constant(Cg)*(exp(Q)-1)
        WV = Constant(k)/2*(J-1)*ln(J)

        W = WP + WV

        P = diff(W, F)

        # Setup forms

        # Pfaller et al.
        k_perp = Constant(2e5)  # [Pa/m]
        c_perp = Constant(5e3)  # [Pa*s/m]

        n = FacetNormal(self.mesh)
        ts = Constant((0, 0, 0))
        rhos = Constant(self.physicalParameters.rhos)
        ts_robin = -outer(n, n)*(k_perp*ys + c_perp*us) - (Identity(3) - outer(n, n))*k_perp/10*ys
        Fs = (inner(P, grad(ws)) + rhos/dt*dot(us - us_n, ws))*self.dx \
            - dot(ts, ws)*self.dSN \
            - dot(ts_robin, ws)*self.dSRob

        return Fs + inner(self.getActive(F, t), grad(ws))*self.dx

    def getActive(self, F, t):
        f0 = self.f0
        C = F.T*F
        I4f = dot(f0, C*f0)
        T_wave = 0.8
        # def time_stim(_t): return sin(2*DOLFIN_PI*t)**2
        def time_stim(_t): return sin(2*DOLFIN_PI*t/T_wave) if t <= T_wave/2 else 0
        Ta = Constant(self.physicalParameters.AS*time_stim(t))
        Pa = Ta*outer(F*f0, f0)/sqrt(I4f)
        return Pa

    def setFibers(self, f0, s0, n0):
        self.f0 = f0
        self.n0 = n0
        self.s0 = s0

    def getFibers(self):
        return self.f0, self.s0, self.n0

    def exportSolution(self, t):
        self.xdmf.write(self.ys_n, t)

    def V(self):
        return self.V

    def saveIterations(self, outname):
        if MPI.COMM_WORLD.rank == 0:
            with open(outname, "w") as f:
                f.write("t,nl-its,l-its,solution-time")
                for i in range(len(self.output["t"])):
                    f.write("\n{},{},{},{}".format(
                        self.output["t"][i], self.output["nl_its"][i], self.output["l_its"][i], self.output["solution_time"][i]))
