"""
Pressure boundary conditions class
"""

class PressureBC:
	def __init__(self, markers, tag, function, compartment):
		self.markers = markers
		self.tag = tag
		self.function = function
		self.compartment = compartment

	def __call__(self, t):
		return self.function(t)
