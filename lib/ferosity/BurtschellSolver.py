#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 11:51:02 2018

@author: barnafi
"""

from dolfin import *
from lib.Functions import *
DEBUG = False

class BurtschellSolver:

    def __init__(self, mesh, f, theta, markers, name):

        self.name  = name
        self.mesh  = mesh
        self.f     = f
        self.theta = theta
        self.markers = markers
        self.dim = mesh.topology().dim()

        cell_type = mesh.ufl_cell()
        el_solid = VectorElement('Lagrange', cell_type, 1)
        elf = FiniteElement('Lagrange', cell_type, 1)
        elb = FiniteElement('Bubble', cell_type, self.dim+1)
#        el_fluid = MixedElement(self.dim*[elf])  # Manual fix for problem with enriched elements
        el_fluid = VectorElement('Lagrange', cell_type, 1) #+ VectorElement('Bubble', cell_type, self.dim+1)
        el_mass = FiniteElement('Lagrange', cell_type, 1)
        el_bubble = VectorElement('Bubble', cell_type, self.dim + 1)
        self.Vs = FunctionSpace(mesh, el_solid)
        self.Vf = FunctionSpace(mesh, el_fluid)
        self.Vm = FunctionSpace(mesh, el_mass)
        self.Vb = FunctionSpace(mesh, el_bubble)
        self.Vstep1   = FunctionSpace(mesh, MixedElement((el_fluid, el_bubble)))
        self.Vstep2a  = FunctionSpace(mesh, MixedElement((el_fluid, el_mass, el_bubble)))
        self.Vstep2b  = self.Vs
        self.BCsolid  = []
        self.BCfluid  = []
        self.Energy   = []
        self.EnergyKf = []
        self.EnergyKs = []
        self.EnergyW  = []
        self.S1       = []
        self.S2       = []
        self.S3       = []
        self.S4       = []

        # Set "current" functions
        self.us_n   = Function(self.Vs, name="displacement")
        self.us_nn  = Function(self.Vs)
        self.vs_n   = Function(self.Vs)
        self.vs_nn  = Function(self.Vs)
        self.vs_nnn = Function(self.Vs)  # Used only for postprocessing
        self.uf_n   = Function(self.Vf, name="fluid_velocity")
        self.m_n    = Function(self.Vm, name="added_mass")
        self.m_nn   = Function(self.Vm)
        self.bubble_n = Function(self.Vb, name="fluid_bubble")
        
        self.package_n  = FunctionPackage(self.uf_n, 
                                          self.m_n,
                                          self.bubble_n, 
                                          self.us_n,
                                          self.vs_n)
        self.package_nn = FunctionPackage(None, 
                                          self.m_nn, 
                                          None, 
                                          self.us_nn, 
                                          self.vs_nn)



    def setParameters(self,
                      dt=1e-3,
                      rhos0=1e3,
                      rhof=1e3,
                      phi0=0.1,
                      mu=0.035,
                      k1=2e3,
                      k2=33.,
                      kp=2e3,
                      ks=2e3,
                      etapor=0.,
                      etad=0.,
                      gamma=20.,
                      D=1e7):
        """
        Set physical parameters, default values are those used in the swelling
        test in

        B. Burtschell et. al, Effective and energy-preserving time discretization
        for a general nonlinear poromechanical formulation (2017).
        """
        self.dt    = Constant(dt)
        self.rhos0 = Constant(rhos0)
        self.rhof  = Constant(rhof)
        self.phi0  = Constant(phi0)
        self.mu    = Constant(mu)
        self.k1    = Constant(k1)
        self.k2    = Constant(k2)
        self.kp    = Constant(kp)
        self.ks    = Constant(ks)
        self.etapor= Constant(etapor)
        self.etad  = Constant(etad)
        self.gamma = Constant(gamma)
        self.D     = Constant(D)*Identity(self.dim)

        self.Functions = Functions(self.dim, k1, k2, kp, ks, etapor, phi0, rhof)

    def setNeumannBoundaryConditions(self, tNeumann, tNoSlid, tNoFlux):
        """
        Set Neumann boundary conditions, all are naturally split in the
        formulation, so setting forces is sufficient.
        """
        self.tNeumann = tNeumann
        self.tNoSlid = tNoSlid
        self.tNoFlux = tNoFlux

    def setMeasures(self, dx, ds, dsN, dsNnos, dsNnof):
        """
        Set boundaries for integration. Some can be null so that they are not
        included in the formulation. This is equivalent to setting an empty
        boundary.
        Code: N: Neumann, Nnos: No sliding, Nnof: No flux.
        """
        self.dx     = dx
        self.ds     = ds
        self.dsN    = dsN
        self.dsNnos = dsNnos
        self.dsNnof = dsNnof

    def setWeakMarkers(self, marks_NoSlip, marks_NoFluxX, marks_NoFluxY):
        self.marks_NoSlip = marks_NoSlip
        self.marks_NoFluxX = marks_NoFluxX
        self.marks_NoFluxY = marks_NoFluxY

    def setSolidBC(self, bc):
        self.BCsolid = bc

    def setFluidBC(self, bc):
        self.BCfluid = bc

    def setInitialConditions(self, us0, uf0):
        assign(self.us_n, us0)
        assign(self.us_nn, us0)
        assign(self.uf_n, uf0)

    def getCurrentSolutions(self):
        return (self.us_n, self.us_nn, 
                self.vs_n, self.vs_nn, self.vs_nnn,
                self.uf_n,
                self.m_n, self.m_nn, self.bubble_n)

    def getParameters(self):
        return (self.dt, self.rhos0, self.rhof, self.phi0, self.mu, self.k1,
                self.k2, self.kp, self.ks, self.etapor, self.etad, self.gamma, self.D)

    def getMeasures(self):
        return (self.dx ,self.ds ,self.dsN ,self.dsNnos, self.dsNnof)

    def solveTimeStep(self, t, drainageTest=False):

        # Renames for readability
        dt, rhos0, rhof, phi0, mu, k1, k2, kp, ks, etapor, etad, gamma, D = self.getParameters()
        theta, f = self.theta(t), self.f(t)
        tNeumann, tNoSlid, tNoFlux = self.tNeumann(t), self.tNoSlid(t), self.tNoFlux(t)
        mesh = self.mesh
        h = mesh.hmax()
        dim = self.dim
        dx, ds, dsN, dsNnos, dsNnof = self.getMeasures()
        import lib.burtschell_substeps as burt

        # Step 0, update phi performed internally

        # Set previous fixed point iterate
        package_k = self.package_n.copyPackage()
        

        # Step 1: fluid prediction
        bcs = self.BCfluid(t)
        uf_tilde = burt.explicitStep(rhof, phi0, gamma, mu, h, dt, dim, theta, self.package_n, self.package_nn,
                                     mesh, dx, ds, dsNnos, dsNnof, self.Vstep1, self.Functions, bcs, drainageTest=drainageTest)
        assign(package_k.uf, uf_tilde.sub(0))
        assign(package_k.bubb, uf_tilde.sub(1))
       
        package_km1 = package_k.copyPackage()

        if DEBUG:
            plot(uf_n, key="uf_n", title="uf_n")

        uf_tilde_f  = uf_tilde.sub(0)
        uf_tilde_fb = uf_tilde.sub(0) + uf_tilde.sub(1)

        # Fixed point solver
        maxiter = 100
        it      = 0
        err     = 1
        tol     = 1e-8

        while it < maxiter and err > tol:

            package_km1.updatePackage(package_k)
            # Step 2a: Fluid + mass

            # Create BC
#            vs_kpm = project(1/dt*(us_k - us_n), self.Vf)
            vs_kpm = Function(self.Vf)
            temp = Function(self.Vs)
            temp.vector()[:] = 1/dt(0)*(package_k.us.vector() - self.package_n.us.vector())
            vs_kpm.vector()[:] = temp.vector()
            bcs = self.BCfluid(t)
            for marks in self.marks_NoSlip:
#                bc = DirichletBC(self.Vstep2a.sub(0), vs_kpm, self.markers, marks)
                bc = DirichletBC(self.Vstep2a.sub(0), uf_tilde_f, self.markers, marks)
                bcs.append(bc)
            for marks in self.marks_NoFluxX:
#                bc = DirichletBC(self.Vstep2a.sub(0).sub(0), vs_kpm.sub(0), self.markers, marks)
                bc = DirichletBC(self.Vstep2a.sub(0).sub(0), uf_tilde_f.sub(0), self.markers, marks)
                bcs.append(bc)
            for marks in self.marks_NoFluxY:
#                bc = DirichletBC(self.Vstep2a.sub(0).sub(1), vs_kpm.sub(1), self.markers, marks)
                bc = DirichletBC(self.Vstep2a.sub(0).sub(1), uf_tilde_f.sub(1), self.markers, marks)
                bcs.append(bc)

            Sol = burt.fluidmassStep(tNeumann, tNoFlux, D, rhof, phi0, gamma, mu, h, dt, dim, theta, f,
                                     self.package_n, self.package_nn, package_k, uf_tilde_f, uf_tilde_fb, 
                                     mesh, dx, ds, dsN, dsNnof, self.Vstep2a, bcs, self.Functions, drainageTest=drainageTest)

            assign(package_k.uf, Sol[0])
            assign(package_k.m, Sol[1])
            assign(package_k.bubb, Sol[2])

            bcs = self.BCsolid(t)
            us = burt.solidStep(tNeumann, tNoSlid, tNoFlux,
                                D, rhof, rhos0, phi0, mu, gamma, etad, h, dt, dim, theta, f,
                                self.package_n, self.package_nn, package_k, uf_tilde_fb,
                                mesh, dx, ds, dsN, dsNnos, dsNnof, self.Vstep2b, bcs, self.Functions, drainageTest=drainageTest)

            assign(package_k.us, us)

            err = package_k.error(package_km1)
            it += 1
            print("Fixed point iteration {}, error={}".format(it, err))

        assign(self.vs_nnn, self.vs_nn)
        self.package_nn.updatePackage(self.package_n)
        self.package_n.updatePackage(package_k)
        self.vs_n.vector()[:] = 2./dt(0)*(self.us_n.vector() - self.us_nn.vector()) - self.vs_n.vector()
        return uf_tilde_fb

    def solve(self, t0, Niter, save_every, drainageTest=False):

        # Set output file
        xdmf  = XDMFFile("images/{}/result.xdmf".format(self.name))
        xdmf.parameters["functions_share_mesh"] = True
        xdmf.parameters["flush_output"] = True
        press = Function(self.Vm, name= "pressure")
        phi = Function(self.Vm, name = "porosity")
        press_exp = self.rhof*self.Functions.dPdm(self.us_n, self.m_n)
        phi_exp   = self.Functions.phi(self.m_n, self.us_n)
        phi_exp_n = self.Functions.phi(self.m_nn, self.us_nn)

        for _t in range(Niter):
            ts = _t*self.dt(0)+t0

            if _t % save_every == 0:

                print("Timestep ({}, {})".format(_t, ts))
                press.assign(project(press_exp, self.Vm))
                phi.assign(project(phi_exp, self.Vm))
                self.exportSolution(xdmf, ts, [self.us_n, self.uf_n, self.m_n, 
                                    press, phi])

            uf_tilde = self.solveTimeStep(ts, drainageTest)

            # Energy postprocessing
            F    = self.Functions.F(self.us_n)
            J    = det(F)
            dx   = self.dx
            mesh = self.mesh
            _Kf   = Kf(mesh, self.rhof, phi_exp_n, self.uf_n, self.us_nn, dx)
            _Ks   = Ks(self.rhos0, self.phi0, self.us_n, dx)
            _Psi  = self.Functions.Psi(F.T*F, self.m_n)
            _W    = W(_Psi, dx)
            Vm   = self.Vm

            self.EnergyKf.append(_Kf)
            self.EnergyKs.append(_Ks)
            self.EnergyW.append(_W)
            self.Energy.append(_W+_Ks+_Kf)

            uf_n = self.uf_n + self.bubble_n
            vs_nmm = 0.5*(self.vs_nn + self.vs_nnn)
            vs_npm = 0.5*(self.vs_n + self.vs_nn)

            self.S1.append(S1(mesh, self.rhof, uf_tilde, self.us_nn, phi_exp_n, uf_n, vs_nmm, Vm))
            self.S2.append(S2(mesh, self.rhof, self.dt, Vm, self.us_n, self.us_nn, phi_exp, phi_exp_n, uf_tilde))
            self.S3.append(S3(Vm, self.rhof, self.dt, uf_tilde, phi_exp, phi_exp_n, J, self.Functions.J(self.us_nn), self.m_n, self.m_nn, self.dx))
            self.S4.append(S4(mesh, self.dsNnos, self.us_n, vs_npm, vs_nmm, self.gamma, self.mu, self.mesh.hmax(), phi_exp))

        # Save energy evolution
        import matplotlib.pyplot as plt
        import numpy as np
        Energy = self.Energy
        arr = np.array(Energy)
        length = arr.shape[0]
        arr.dump("energy_evolution_{}.npy".format(self.name))
        domain = np.linspace(0, self.dt(0)*Niter, Niter)

        # Show energy derivative in time and contributions below
        plt.clf()
        plt.subplot(211)
        dtE = np.gradient(arr, self.dt(0))
        print(len(self.Energy), dtE.shape[0])
        plt.plot(domain, dtE)
        plt.title('dE/dt')
        plt.subplot(212)
        plt.plot(domain, self.EnergyKf)
        plt.plot(domain, self.EnergyKs)
        plt.plot(domain, self.EnergyW)
        plt.plot(domain, self.Energy)
        plt.ylim([arr.min(), arr.max()])
        plt.legend(['Kf', 'Ks', 'W', 'E'])
        plt.savefig( "images/{}/energy_evol.png".format(self.name), dpi = 1000)

        # Show energy derivative in time and dissipative terms
        plt.clf()
        arrS1 = np.array(self.S1)
        arrS2 = np.array(self.S2)
        arrS3 = np.array(self.S3)
        arrS4 = np.array(self.S4)
        plt.subplot(121)
        plt.plot(domain, dtE)
        plt.plot(domain, -arrS1-arrS2-arrS3-arrS4)
        plt.plot(domain, -arrS3)
        plt.legend(['(E - En)/dt', '-sum(Si)', '-S3'])
        plt.subplot(122)
        plt.plot(domain, -arrS1)
        plt.plot(domain, -arrS2)
        plt.plot(domain, -arrS3)
        plt.plot(domain, -arrS4)
        plt.legend(['-S1', '-S2', '-S3', '-S4'])
        plt.savefig("images/{}/energy_dissipation.png".format(self.name), dpi = 1000)
        
    def exportSolution(self, xdmf, t, functions):
        for f in functions:
            xdmf.write(f, t)