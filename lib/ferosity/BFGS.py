from time import perf_counter as time
from ferosity.AndersonAcceleration import AndersonAcceleration
from dolfin import *
from mpi4py import MPI
from petsc4py import PETSc
from time import perf_counter as time


def line_search(u, p):
    p.copy(incr.vector().vec())  # make a copy of p in incr
    a0 = 0
    aii = a0
    ai = amax/2

    def phi_fun(a):
        uu.assign(u)
        uu.vector().vec().axpy(a, p)
        PI = Pi_fun(uu)
        return assemble(PI)

    def dphi_fun(a):
        uu.assign(u)
        uu.vector().vec().axpy(a, p)
        PI = Pi_fun(uu)
        RES = derivative(PI, uu, incr)
        return assemble(RES)

    phi0 = phi_fun(0)
    dphi0 = dphi_fun(0)

    def psi(a):
        return phi_fun(a) - phi0 - c1 * a * dphi0

    def dpsi(a):
        return dphi_fun(a) - c1 * dphi0

    def zoom(aii, ai):
        a_hi = max(aii, ai)
        a_lo = min(aii, ai)
        aj = 0.5 * (a_hi + a_lo)
        phi_aj = phi_fun(aj)
        j = 1
        while j < maxiter_LS:
            if phi_aj >= phi0 + c1 * aj * dphi0 or phi_aj >= phi_fun(a_lo):
                a_hi = aj
            else:
                dphi_aj = dphi_fun(aj)
                if abs(dphi_aj) <= -c2 * dphi0:
                    return aj
                if dphi_aj * (a_hi - a_lo) >= 0:
                    a_hi = a_lo
                a_lo = aj
            # print("\t\t\t\t Zoom iteration {}, ({:.4f},{:.4f},{:.4f})".format(j, a_lo, aj, a_hi))
            j += 1
        return aj
    err, i = 1, 1
    phi_ai = phi_fun(ai)
    phi_aii = phi_fun(aii)
    while err > tol_LS and i < maxiter_LS:
        if phi_ai > phi0 + c1 * ai * dphi0 or phi_ai >= phi_aii and i > 1:
            return zoom(aii, ai)
        dphi_ai = dphi_fun(ai)
        if abs(dphi_ai) <= -c2 * dphi0:
            return ai
        if dphi_ai >= 0:
            return zoom(aii, ai)
        aii = ai
        ai = 0.5 * (ai + amax)
        phi_ai = phi_fun(ai)
        phi_aii = phi_fun(aii)
        err = abs(phi_ai - phi_aii)  # /phi0
        # if(MPI.COMM_WORLD.rank == 0 and i % print_every_LS == 0):
        # print("\t\t Line search iteration {}, step size={:.4f}, err={:.2e}".format(i, ai, err))
        i += 1
    if(MPI.COMM_WORLD.rank == 0):
        print("\t\t Line search iteration {}, step size={:.4f}, err={:.2e}".format(i, ai, err), flush=True)
    return ai


# DEPRECATED
class BFGS:
    def __init__(self, jac, atol, rtol, maxit, alpha, verbose=False, anderson_depth=0):
        self.atol = atol
        self.rtol = rtol
        self.maxit = maxit
        self.alpha = alpha
        self.verbose = verbose
        self.anderson_depth = anderson_depth
        ksp = PETSc.KSP().create()
        ksp.setType('preonly')
        ksp.setOperators(jac)
        pc = ksp.getPC()
        pc.setType('hypre')
        ksp.setTolerances(1e-8, 1e-8, 1e20, 1000)
        ksp.setFromOptions()
        pc.setFromOptions()
        self.ksp = ksp
        self.sks = [None]
        self.yks = [None]
        self.rhoks = [None]

        def H0(vec, k):
            assert k == 0, "Preconditioner in BFGS must me called at last level in recursion always"
            # return 0.1*vec
            # Alternative with AMG smoother:
            b = vec.copy()
            self.ksp.solve(b, vec)
            # return vec
        self.Hs = [H0]
        self.k = 0

    def update(self, sk, yk):
        self.sks.append(sk)
        self.yks.append(yk)
        self.rhoks.append(1 / (yk.dot(sk)))

        def Hk(vec, k):
            rhok = self.rhoks[k]
            skdvec = self.sks[k].dot(vec)
            vec.axpy(-rhok*skdvec, self.yks[k])
            self.Hs[k-1](vec, k-1)
            vec.axpy(-rhok * self.yks[k].dot(vec), self.sks[k])
            vec.axpy(rhok * skdvec, self.sks[k])
            # return vec
        self.Hs.append(Hk)

    def apply(self, vec):
        return self.Hs[-1](vec, len(self.Hs)-1)

    def apply_bcs(self, obj, bcs):
        if bcs:
            for bb in bcs:
                bb.apply(obj)

    def apply_bc_petsc(self, vec, bcs):
        if bcs:
            for bb in bcs:
                vals = bb.get_boundary_values()
                for dof in vals:
                    vec.setValue(dof, vals[dof])

    def solve(self, sol, res_fun, bcs=None):

        # If there are BCs, homogenize them after initializing solution
        if bcs:
            for bb in bcs:
                bb.apply(sol)
                bb.homogenize()
        # sol must be a FEM vector so that we can update the ghost dofs.
        pk = sol.vector().vec().copy()  # pk is PETSc vector
        pk.zeroEntries()
        res = sol.copy(True)  # res is DOLFIN vector
        resn = pk.copy()  # resn is PETSc vector
        soln = pk.copy()  # soln is PETSc vector

        res_fun(res)  # Initial residual
        self.apply_bcs(res, bcs)
        anderson = AndersonAcceleration(self.anderson_depth)
        error0 = res.vector().norm('linf')
        error_rel = error_abs = 1
        it = 0
        while error_abs > self.atol and error_rel > self.rtol and it < self.maxit:
            self.apply(res.vector().vec())
            res.vector().vec().copy(pk)
            do_LS = False  # TODO: Line search
            if do_LS:
                pass
            sol.vector().vec().axpy(-self.alpha, pk)
            anderson.get_next_vector(sol.vector().vec())
            sol.vector().apply("")
            res_fun(res)
            self.apply_bcs(res, bcs)
            # for bc in bcs: # NO BCS
            # bc.apply(res)
            # They must be allocated as the iteration count is unkown
            sk = sol.vector().vec() - soln
            yk = res.vector().vec() - resn
            self.update(sk, yk)

            sol.vector().vec().copy(soln)
            res.vector().vec().copy(resn)
            error_abs = res.vector().norm('linf')
            error_rel = error_abs / error0
            print_every = 5
            if(MPI.COMM_WORLD.rank == 0 and it % print_every == 0 and self.verbose):
                print("it {}, err_abs={:.3e}, err_rel={:.3e}".format(
                    it, error_abs, error_rel), flush=True)
            it += 1
        if(MPI.COMM_WORLD.rank == 0):
            print("\t\t BFGS converged in {} iterations".format(it), flush=True)
