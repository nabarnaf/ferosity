import dolfin as df
from ferosity.Physics.AbstractPhysics import AbstractPhysics
from ferosity.ConstitutiveModeling.PoromechanicsEnergy import PoromechanicsEnergy
from ferosity.Projection import Projection
from ferosity.AndersonAcceleration import AndersonAcceleration
from petsc4py import PETSc
import numpy as np


class CooksonMonolithic(AbstractPhysics):
    def __init__(self, mesh, name, params, skeleton_energy, volume_energy, dsNeumann):

        super().__init__(mesh, name)

        # FE
        self.degree_solid = params["degree solid"]
        self.degree_mass = params["degree mass"]
        self.N_compartments = params["number compartments"]

        self.dsNeumann = dsNeumann

        # Time
        self.t0 = params["t0"]
        self.tf = params["tf"]
        self.dt = params["dt"]
        self.t = self.t0 + self.dt

        # Physical parameters
        self.rhof = params["rhof"]
        self.rhos = params["rhos"]
        self.phi0_vec = params["initial porosities"]
        self.phis = 1 - np.sum(self.phi0_vec)
        self.kf_vec = params["permeabilities"]

        assert isinstance(self.phi0_vec, list)
        assert isinstance(self.kf_vec, list)

        self.pressure_model = params["pressure model"]  # Either mono or mult
        self.pressure_bc_flag = params["pressure bc use"]

        self.muf = params["muf"]
        self.beta = params["compartments interaction"]

        # If using pressure boundary conditions, set parameters for fixed point iteration
        if self.pressure_bc_flag:

            self.pressure_bc_log = params["pressure bc log"]
            self.pressure_bc_tol = params["pressure bc tol"]
            self.pressure_bc_maxiter = params["pressure bc maxiter"]
            self.projection_L2_smooth = Projection(df.FunctionSpace(
                mesh, "CG", self.degree_mass), type="H1")

        self.convergence_flag = params["convergence_flag"]
        self.flag_semi = params["flag semi implicit"]
        # Output
        self.projection_L2 = Projection(df.FunctionSpace(
            mesh, "CG", self.degree_mass), type="L2")
        self.export_solutions = params["export solutions"]
        self.save_every = params["save every"]

        # Functional setting
        el_s = df.VectorElement("CG", mesh.ufl_cell(), self.degree_solid)
        el_m = df.VectorElement("CG", mesh.ufl_cell(),
                                self.degree_mass, self.N_compartments)
        self.V = df.FunctionSpace(mesh, df.MixedElement([el_s, el_m]))
        if self.convergence_flag:
            self.y_sol = []
            self.m_sol = []
            self.Vs = df.FunctionSpace(mesh, el_s)
            self.Vm = df.FunctionSpace(mesh, el_m)
        self.solution = df.Function(self.V)
        self.solution_n = df.Function(self.V)
        self.solution_nn = df.Function(self.V)
        self.iterations=params["compare iterations"]
        if self.iterations:
            self.newton_iter = []
        self.energy = PoromechanicsEnergy(
            params, skeleton_energy, volume_energy)

    def setup_loads(self, f_vol_solid, f_sur_solid, f_vol_mass, f_sur_mass):
        self.f_vol_solid = f_vol_solid
        self.f_sur_solid = f_sur_solid
        self.f_vol_mass = f_vol_mass
        self.f_sur_mass = f_sur_mass

    def solve_timestep(self):
        if self.pressure_bc_flag:
            Fp = self.form(self.t)
            Jac = df.derivative(Fp, self.solution)

            error, it = 1, 0

            sol_k = self.solution.copy(True)
            residual = df.PETScVector()
            df.assemble(Fp, tensor=residual)
            norm_sol_k = df.norm(residual)
            error_normalization = 1 if norm_sol_k <= 1e-14 else norm_sol_k

            # Elements for acceleration
            dofs0 = self.V.sub(0).dofmap().dofs()
            dofs1 = self.V.sub(1).dofmap().dofs()
            weight0_npvec = residual.vec().getValues(dofs0)
            weight0 = np.linalg.norm(weight0_npvec)
            weight1_npvec = residual.vec().getValues(dofs1)
            weight1 = np.linalg.norm(weight1_npvec)

            if weight0 < 3e-16:
                weight0 = 1
            if weight1 < 3e-16:
                weight1 = 1

            diagonal_weights = PETSc.Vec().createSeq(self.V.dim())
            diagonal_weights.setValues(dofs0, [weight0] * len(dofs0))
            diagonal_weights.setValues(dofs1, [weight1] * len(dofs1))
            anderson = AndersonAcceleration(
                {"anderson_order": 5}, diagonal_weights=diagonal_weights)
            xk_list = [sol_k.vector().vec()]
            fk_list = []

            while it < self.pressure_bc_maxiter and error > self.pressure_bc_tol:

                petsc_init = sol_k.vector().vec()
                bcs = []

                # Generate boundary conditions
                for pbc in self.pressure_bc_list:
                    comp = pbc.compartment
                    markers = pbc.markers
                    tag = pbc.tag
                    functino = pbc.function

                    # Compute added mass in current compartment as projection
                    F = df.grad(self.solution.sub(0)) + df.Identity(self.dim)
                    m_ufl = self.energy.getInversePressure(
                        df.det(F), pbc.function(self.t), self.solution.sub(1), comp)
                    m_val = self.projection_L2_smooth.project(m_ufl)
                    bc = df.DirichletBC(self.V.sub(1).extract_sub_space(
                        [comp]), m_val, markers, tag)
                    bcs.append(bc)

                problem = df.NonlinearVariationalProblem(
                    Fp, self.solution, self.bcs(self.t) + bcs, Jac)
                solver = df.NonlinearVariationalSolver(problem)
                prm = solver.parameters
                prm['newton_solver']['absolute_tolerance'] = 1E-11
                prm['newton_solver']['relative_tolerance'] = 1E-7
                prm['newton_solver']['maximum_iterations'] = 100
                # prm['newton_solver']['relaxation_parameter'] = 0.8
                solver.solve()

                # Accelerate after solution
                petsc_new = self.solution.vector().vec()
                fk_list.append(petsc_new - petsc_init)
                petsc_new = anderson.solve_iteration(
                    it, xk_list, fk_list, gk=petsc_new)
                xk_list.append(petsc_new)

                # error = df.errornorm(self.solution, sol_k,
                # degree_rise=0) / error_normalization
                error = (petsc_new - petsc_init).norm() / error_normalization
                it += 1

                # sol_k.assign(self.solution)
                petsc_new.copy(sol_k.vector().vec())
                if self.pressure_bc_log:
                    print(
                        "--------- Fixed point iteration {}, error={:.2e}".format(it, error))

            print(
                "--------- Finished in {} fixed point iterations, error={:.2e}".format(it, error))

        else:
            F = self.form(self.t)
            problem = df.NonlinearVariationalProblem(
                F, self.solution, self.bcs(self.t), df.derivative(F, self.solution))
            solver = df.NonlinearVariationalSolver(problem)
            prm = solver.parameters
            prm['newton_solver']['absolute_tolerance'] = 1E-11
            prm['newton_solver']['relative_tolerance'] = 1E-7
            prm['newton_solver']['maximum_iterations'] = 1000

            (iteration, conv) = solver.solve()
            if self.iterations:
                self.newton_iter.append(iteration)
        if self.convergence_flag:
            yy, mm = self.solution.split(True)
            self.y_sol.append(yy)
            self.m_sol.append(mm)
        df.assign(self.solution_nn, self.solution_n)
        df.assign(self.solution_n, self.solution)

    def form(self, t):

        ys, m = df.split(self.solution)
        ys_n, m_n = df.split(self.solution_n)
        ys_nn, m_nn = df.split(self.solution_nn)
        ws, q = df.TestFunctions(self.V)
        F = df.grad(ys) + df.Identity(self.dim)
        J = df.det(F)

        Fp = df.Constant(self.rhos * (self.phis)) * df.dot(ys - 2 * ys_n + ys_nn, ws) * df.dx + \
            df.Constant(self.dt**2) * \
            df.inner(self.energy.getPiola(F, m), df.grad(ws)) * df.dx - \
            df.Constant(self.dt**2) * \
            df.inner(self.f_sur_solid(t), ws) * self.dsNeumann -\
            df.Constant(self.dt**2) * \
            df.inner(self.f_vol_solid(t), ws) * df.dx

        if self.flag_semi:
            p = self.energy.getPressure(F, m_n)  # Semi-implicit scheme
        else:
            p = self.energy.getPressure(F, m)  # Implicit scheme
        for comp in range(self.N_compartments):
            Ki = df.det(F) * df.inv(F) * \
                df.Constant(self.kf_vec[comp]) * df.inv(F.T)
            if (self.flag_semi):
                Fp += (m[comp] - m_n[comp]) * q[comp] * df.dx + df.Constant(
                    self.dt * self.rhof) * df.inner(Ki * self.energy.gradPressureSemi(F, m[comp], m_n[comp], comp), df.grad(q[comp])) * df.dx - \
                    df.Constant(self.dt * self.rhof) * \
                    df.inner(self.f_vol_mass(t)[comp], q[comp]) * df.dx
            else:
                Fp += (m[comp] - m_n[comp]) * q[comp] * df.dx + df.Constant(
                    self.dt * self.rhof) * df.inner(Ki * df.grad(p[comp]), df.grad(q[comp])) * df.dx - \
                    df.Constant(self.dt * self.rhof) * \
                    df.inner(self.f_vol_mass(t)[comp],
                             q[comp]) * df.dx(domain=self.mesh)
            for comp2 in range(self.N_compartments):
                Fp += df.Constant(self.dt * self.rhof) * J * df.inner(self.beta[comp][comp2] *
                                                          (p[comp] - p[comp2]), q[comp]) * df.dx(domain=self.mesh)

        return Fp

    def export(self, t):
        ys = df.Function(self.V.sub(0).collapse(), name="displacement")
        m = df.Function(self.V.sub(1).collapse(), name="added mass")
        p = df.Function(self.V.sub(1).collapse(), name="pressure")

        Vm = df.FunctionSpace(self.mesh, "CG", self.degree_mass)
        m_alloc = df.Function(Vm)
        p_alloc = df.Function(Vm)

        df.assign(ys, self.solution.sub(0))
        df.assign(m, self.solution.sub(1))

        p_ufl = self.energy.getPressure(df.grad(ys) + df.Identity(self.dim), m)
        df.assign(p, df.project(p_ufl, self.V.sub(1).collapse()))

        self.xdmf.write(ys, t)
        self.xdmf.write(m, t)
        for comp in range(self.N_compartments):
            m_alloc.rename("m{}".format(comp), "m{}".format(comp))
            p_alloc.rename("p{}".format(comp), "p{}".format(comp))

            df.assign(m_alloc, self.projection_L2.project(m[comp]))
            df.assign(p_alloc, self.projection_L2.project(p[comp]))

            self.xdmf.write(m_alloc, t)
            self.xdmf.write(p_alloc, t)

    def set_bcs(self, bcs, pressure_bcs=None):
        """
        Extend boundary conditions to include pressure conditions
        """
        assert isinstance(bcs(0), list), "Bcs must be a list"
        self.bcs = bcs
        if self.pressure_bc_flag:
            assert isinstance(pressure_bcs, list), "Bcs must be a list"
            self.pressure_bc_list = pressure_bcs
