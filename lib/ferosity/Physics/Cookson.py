"""
@author: Giulia Ferla, Micol Bassanini
"""

import dolfin as df
from ferosity.Physics.AbstractPhysics import AbstractPhysics
from ferosity.ConstitutiveModeling.PoromechanicsEnergy import PoromechanicsEnergy
from ferosity.Projection import Projection
from ferosity.AndersonAcceleration import AndersonAcceleration
from ferosity.MeshCreation import generate_boundary_measure
from petsc4py import PETSc
import numpy as np


class Cookson(AbstractPhysics):
    def __init__(self, mesh, name, params, skeleton_energy, volume_energy, markers, NeumannMarkers, RobinMarkers):

        super().__init__(mesh, name)

        # FE
        self.degree_solid = params["degree solid"]
        self.degree_mass = params["degree mass"]
        self.N_compartments = params["number compartments"]

        self.geometry = params["geometry"]
        # Check:
        assert self.geometry in [
            "heart", "square"], "Not valid geometry! Insert square or heart."

        self.cardiac_mechanics = True if self.geometry == "heart" else False

        self.ds = df.Measure(
            'ds', domain=mesh, subdomain_data=markers, metadata={'optimize': True})
        self.dsEmpty = self.ds(None)

        self.dSN = generate_boundary_measure(mesh, markers, NeumannMarkers)
        self.dSRob = generate_boundary_measure(mesh, markers, RobinMarkers)

        # Time
        self.t0 = params["t0"]
        self.tf = params["tf"]
        self.dt = params["dt"]
        self.t = self.t0

        # Physical parameters
        self.rhof = params["rhof"]
        self.rhos = params["rhos"]
        self.phi0_vec = params["initial porosities"]
        self.phis = 1 - np.sum(self.phi0_vec)
        self.kf_vec = params["permeabilities"]

        # Pfaller et al.
        self.k_perp = df.Constant(2e5)  # [Pa/m]
        self.c_perp = df.Constant(5e3)  # [Pa*s/m]
        self.AS = 3e4  # Active stress

        assert isinstance(self.phi0_vec, list)
        assert isinstance(self.kf_vec, list)

        self.pressure_model = params["pressure model"]  # Either mono or mult
        self.pressure_bc_flag = params["pressure bc use"]

        self.muf = params["muf"]
        self.beta = params["compartments interaction"]
        self.gamma = params["gamma"]
        self.venous_return = params["venous return"]

        # If using pressure boundary conditions, set parameters for fixed point iteration
        if self.pressure_bc_flag:
            self.pressure_bc_log = params["pressure bc log"]
            self.pressure_bc_tol = params["pressure bc tol"]
            self.pressure_bc_maxiter = params["pressure bc maxiter"]
            self.projection_L2_smooth = Projection(df.FunctionSpace(
                mesh, "CG", self.degree_mass), type="H1")

        self.convergence_flag = params["convergence_flag"]
        self.flag_semi = params["flag semi implicit"]

        self.method = params["method"]
        # Check:
        assert self.method in ["monolithic", "fixed point",
                               "staggered"], "Not a valid method! Insert monolithic, fixed point or staggered."

        if self.method != "monolithic":
            self.scheme_abs_tol = params["scheme absolute tol"]
            self.scheme_rel_tol = params["scheme relative tol"]
        if self.method == "fixed point":
            self.maxiter = params["maxiter"]
        else:
            self.maxiter = 1

        # Functional setting
        el_s = df.VectorElement("CG", mesh.ufl_cell(), self.degree_solid)
        el_m = df.VectorElement("CG", mesh.ufl_cell(),
                                self.degree_mass, self.N_compartments)
        self.V = df.FunctionSpace(mesh, df.MixedElement([el_s, el_m]))
        self.Vs = df.FunctionSpace(mesh, el_s)
        self.Vm = df.FunctionSpace(mesh, el_m)
        self.mesh = mesh
        self.volume = df.assemble(1 * df.dx(mesh))
        self.iterations=params["compare iterations"]

        # Output
        self.projection_L2 = Projection(self.Vm, type="L2")
        self.projection_H1 = Projection(self.Vm, type="H1")
        self.export_solutions = params["export solutions"]
        self.save_every = params["save every"]

        # Initialize Vectors
        if self.method == "monolithic":
            # Initialize solutions
            self.solution = df.Function(self.V)
            self.ys, self.m = df.split(self.solution)
            self.solution_n = df.Function(self.V)
            self.solution_nn = df.Function(self.V)

            # Keep track of Newton iterations
            if self.iterations:
                self.newton_iter = []
        else:
            # Initialize solutions            
            self.ys = df.Function(self.Vs)
            self.m = df.Function(self.Vm)
            self.ys_prev = df.Function(self.Vs)
            # Keep track of Newton iterations
            if self.iterations:
                self.newton_y = []
                self.newton_mass = []
                self.iter = []

        # Previous timesteps
        self.ys_n = df.Function(self.Vs)
        self.m_n = df.Function(self.Vm)
        self.ys_nn = df.Function(self.Vs)
        self.m_nn = df.Function(self.Vm)

        if self.convergence_flag:
            self.y_sol = []
            self.m_sol = []

        self.energy = PoromechanicsEnergy(
            params, skeleton_energy, volume_energy)

    def setup_loads(self, f_vol_solid, f_sur_solid, f_vol_mass, f_sur_mass):
        self.f_vol_solid = f_vol_solid
        self.f_sur_solid = f_sur_solid
        self.f_vol_mass = f_vol_mass
        self.f_sur_mass = f_sur_mass

    def initialize_solutions(self, y0, m0):
        self.ys_n.assign(y0)
        self.m_n.assign(m0)

        if self.method == "monolithic":
            df.assign(self.solution.sub(0), self.ys_n)
            df.assign(self.solution.sub(1), self.m_n)
        else:
            df.assign(self.ys, self.ys_n)
            df.assign(self.m ,self.m_n)


    def generateFunctions(self):
        if self.method == "monolithic":
            ws, q = df.TestFunctions(self.V)
            return ws, q
        else:
            ws = df.TrialFunction(self.Vs)
            q = df.TrialFunction(self.Vm)
            return ws, q

    def getActive(self, F, t):
        from numpy import fmod
        f0 = self.f0
        C = F.T * F
        I4f = df.dot(f0, C * f0)
        T_wave = 0.8
        def time_stim(_t): return df.sin(2 * df.DOLFIN_PI *
                                         _t / T_wave) if _t <= T_wave / 2 else 0
        Ta = df.Constant(self.AS * time_stim(fmod(t, T_wave)))
        Pa = Ta * df.outer(F * f0, f0) / df.sqrt(I4f)
        return Pa

    def solve_timestep(self):
        ws, q = self.generateFunctions()
        it = 0
        it1 = 0
        it2 = 0
        Fy0_norm = 0
        Fm0_norm = 0

        # Residual vectors
        Fyvec = df.PETScVector()
        Fmvec = df.PETScVector()

        n = df.FacetNormal(self.mesh)

        while it < self.maxiter:

            # Formulate mechanics problem
            F = df.grad(self.ys) + df.Identity(self.dim)
            J = df.det(F)
            us = (self.ys - self.ys_n) / self.dt

            # Boundary term for Robin conditions in cardiac mechanics, not used if meas(dSRob)=0.
            ts_robin = -df.outer(n, n) * (self.k_perp * self.ys + self.c_perp * us) - (
                df.Identity(self.dim) - df.outer(n, n)) * self.k_perp / 10 * self.ys

            P = self.getPiola(F, self.m)

            # Fy = df.inner(P, df.grad(ws)) * df.dx - df.inner(self.f_sur_solid(self.t), ws) * self.dSN - \
            # df.inner(self.f_vol_solid(self.t), ws) * \
            # df.dx - df.dot(ts_robin, ws) * self.dSRob
            Fy = df.Constant(self.rhos * (self.phis)) * df.dot(self.ys - 2 * self.ys_n + self.ys_nn, ws) * df.dx + \
                df.Constant(self.dt**2) * \
                df.inner(P, df.grad(ws)) * df.dx - \
                df.Constant(self.dt**2) * \
                df.inner(self.f_sur_solid(self.t), ws) * self.dSN -\
                df.Constant(self.dt**2) * \
                df.inner(self.f_vol_solid(self.t), ws) * \
                df.dx - df.Constant(self.dt**2) * \
                df.dot(ts_robin, ws) * self.dSRob

            if self.method != "monolithic":
                problem = df.NonlinearVariationalProblem(
                    Fy, self.ys, self.bcs_y(self.t), df.derivative(Fy, self.ys))
                solver = df.NonlinearVariationalSolver(problem)
                prm = solver.parameters
                prm['newton_solver']['absolute_tolerance'] = 1e-3 * \
                    self.scheme_abs_tol
                prm['newton_solver']['relative_tolerance'] = 1e-3 * \
                    self.scheme_rel_tol
                prm['newton_solver']['maximum_iterations'] = 100
                prm['newton_solver']['linear_solver'] = 'mumps'
                #prm['newton_solver']['linear_solver'] = 'gmres'
                #prm['newton_solver']['preconditioner'] = 'jacobi'
                #prm['newton_solver']['krylov_solver']['maximum_iterations'] = 2 * self.V.dim()
                #prm['newton_solver']['krylov_solver']["nonzero_initial_guess"] = True
                
                # Initial residual norm
                if it == 0:
                    df.assemble(Fy, tensor=Fyvec)
                    Fy0_norm = Fyvec.norm('linf')
                    if Fy0_norm < 3e-15:
                        Fy0_norm = 1

                (iteration1, conv1) = solver.solve()
                it1 += iteration1

            # Formulate mass problem
            Fm = 0
            if self.flag_semi:
                p = self.energy.getPressure(
                    F, self.m_n)  # Semi-implicit scheme
            else:
                p = self.energy.getPressure(F, self.m)  # Implicit scheme

            for comp in range(self.N_compartments):
                Ki = df.det(F) * df.inv(F) * \
                    df.Constant(self.kf_vec[comp]) * df.inv(F.T)
                if (self.flag_semi):
                    Fm += (self.m[comp] - self.m_n[comp]) * q[comp] * df.dx + df.Constant(
                        self.dt * self.rhof) * df.inner(Ki * self.energy.gradPressureSemi(F, self.m[comp], self.m_n[comp], comp), df.grad(q[comp])) * df.dx - \
                        df.Constant(self.dt * self.rhof) * \
                        df.inner(self.f_vol_mass(self.t)
                                 [comp], q[comp]) * df.dx
                else:
                    Fm += (self.m[comp] - self.m_n[comp]) * q[comp] * df.dx + df.Constant(
                        self.dt * self.rhof) * df.inner(Ki * df.grad(p[comp]), df.grad(q[comp])) * df.dx - \
                        df.Constant(self.dt * self.rhof) * \
                        df.inner(self.f_vol_mass(self.t)[comp],
                                 q[comp]) * df.dx(domain=self.mesh)

                # Add venous return and compartment interaction
                if(self.cardiac_mechanics):
                    Fm += df.Constant(self.dt * self.rhof) * self.gamma * \
                        (p[-1] - self.venous_return) * q[-1] * df.dx
                for comp2 in range(self.N_compartments):
                    Fm += df.Constant(self.dt * self.rhof) * J * df.inner(self.beta[comp][comp2] *
                                                                          (p[comp] - p[comp2]), q[comp]) * df.dx(domain=self.mesh)

            if self.method == "monolithic":
                Ftot = Fy + Fm
                problem = df.NonlinearVariationalProblem(
                    Ftot, self.solution, self.bcs(self.t), df.derivative(Ftot, self.solution))
                solver = df.NonlinearVariationalSolver(problem)
                prm = solver.parameters
                prm['newton_solver']['absolute_tolerance'] = 1E-11
                prm['newton_solver']['relative_tolerance'] = 1E-7
                prm['newton_solver']['maximum_iterations'] = 1000
                prm['newton_solver']['linear_solver'] = 'mumps'
                # prm['newton_solver']['preconditioner'] = 'jacobi'
                # prm['newton_solver']['krylov_solver']['maximum_iterations'] = 2 * self.V.dim()
                # prm['newton_solver']['krylov_solver']["nonzero_initial_guess"] = True

                (iteration, conv) = solver.solve()
                if self.iterations:
                    self.newton_iter.append(iteration)

            else:
                problem = df.NonlinearVariationalProblem(
                    Fm, self.m, self.bcs_m(self.t), df.derivative(Fm, self.m))
                solver = df.NonlinearVariationalSolver(problem)
                prm = solver.parameters
                prm['newton_solver']['absolute_tolerance'] = 1e-3 * \
                    self.scheme_abs_tol
                prm['newton_solver']['relative_tolerance'] = 1e-3 * \
                    self.scheme_rel_tol
                prm['newton_solver']['maximum_iterations'] = 100
                prm['newton_solver']['linear_solver'] = 'mumps'
                #prm['newton_solver']['linear_solver'] = 'gmres'
                #prm['newton_solver']['preconditioner'] = 'jacobi'
                #prm['newton_solver']['krylov_solver']['maximum_iterations'] = 2 * self.V.dim()
                #prm['newton_solver']['krylov_solver']["nonzero_initial_guess"] = True
                # Initial residual norm
                if it == 0:
                    df.assemble(Fm, tensor=Fmvec)
                    Fm0_norm = Fmvec.norm('linf')
                    if Fm0_norm < 3e-15:
                        Fm0_norm = 1

                (iteration2, conv2) = solver.solve()

                it2 += iteration2

                df.assemble(Fy, tensor=Fyvec)
                df.assemble(Fm, tensor=Fmvec)

                for b in self.bcs_y(self.t):
                    new_bc = df.DirichletBC(b)
                    new_bc.homogenize()
                    new_bc.apply(Fyvec)
                for b in (self.bcs_m(self.t)):
                    new_bc = df.DirichletBC(b)
                    new_bc.homogenize()
                    new_bc.apply(Fmvec)

                Fy_norm = Fyvec.norm('linf')
                Fm_norm = Fmvec.norm('linf')
                error = np.max([Fy_norm, Fm_norm])

                if error < self.scheme_abs_tol:
                    print(
                        "-----------Convergence, absolute tolerance attained in {} iterations".format(it+1))
                    break

                if np.max([Fy_norm / Fy0_norm, Fm_norm / Fm0_norm]) < self.scheme_rel_tol:
                    print(
                        "-----------Convergence, relative tolerance attained in {} iterations".format(it+1))
                    break

                print(
                    "--------- Fixed point iteration {}, error_abs={:.2e}, error_rel={:.2e}".format(it+1, error, np.max([Fy_norm / Fy0_norm, Fm_norm / Fm0_norm])))
            it += 1

        if self.iterations and self.method != "monolithic":
            self.newton_y.append(it1)
            self.newton_mass.append(it2)
            self.iter.append(it+1)
        if self.convergence_flag:
            if self.method != "monolithic":
                self.y_sol.append(self.ys.copy(True))
                self.m_sol.append(self.m.copy(True))
            else:
                yy, mm = self.solution.split(True)
                self.y_sol.append(yy)
                self.m_sol.append(mm)

        # Update previous timestep
        df.assign(self.ys_nn, self.ys_n)
        ys = self.solution.sub(0) if self.method == "monolithic" else self.ys
        m = self.solution.sub(1) if self.method == "monolithic" else self.m
        df.assign(self.ys_n, ys)
        df.assign(self.m_n, m)

    def setFibers(self, f0, s0, n0):
        self.f0 = f0
        self.n0 = n0
        self.s0 = s0

    def getPiola(self, F, m):
        if self.cardiac_mechanics:
            return self.energy.getPiola(F, m) + self.getActive(F, self.t)
        else:
            return self.energy.getPiola(F, m)

    def getAveragePressure(self, compartment):
        ys = self.ys_n
        m = self.m_n
        F = df.grad(ys) + df.Identity(self.dim)
        p = self.energy.getPressure(F, m)

        return df.assemble(p[compartment] * df.dx(self.mesh)) / self.volume

    def export(self, t):
        ys = df.Function(self.V.sub(0).collapse(), name="displacement")
        m = df.Function(self.V.sub(1).collapse(), name="added mass")
        p = df.Function(self.V.sub(1).collapse(), name="pressure")

        if self.method == "monolithic":
            df.assign(ys, self.solution.sub(0))
            df.assign(m, self.solution.sub(1))
        else:
            df.assign(ys, self.ys)
            df.assign(m, self.m)

        p_ufl = self.energy.getPressure(df.grad(ys) + df.Identity(self.dim), m)
        df.assign(p, self.projection_H1.project(p_ufl))

        f_alloc = df.Function(self.Vm.extract_sub_space([0]).collapse())

        self.xdmf.write(ys, t)
        if self.N_compartments == 1:
            f_alloc.rename("m", "m")
            df.assign(f_alloc, m)
            self.xdmf.write(f_alloc, t)
            f_alloc.rename("p", "p")
            df.assign(f_alloc, p)
            self.xdmf.write(f_alloc, t)
        else:
            for comp in range(self.N_compartments):

                f_alloc.rename("m{}".format(comp), "m{}".format(comp))
                #df.assign(f_alloc, m[0])
                df.assign(f_alloc, m.sub(0))
                self.xdmf.write(f_alloc, t)
                f_alloc.rename("p{}".format(comp), "p{}".format(comp))
                df.assign(f_alloc, p.sub(comp))
                self.xdmf.write(f_alloc, t)

    def set_bcs(self, bcs, pressure_bcs=None):
        """
        Extend boundary conditions to include pressure conditions
        """
        if self.method == "monolithic":
            assert isinstance(bcs(0), list), "Bcs must be a list"
            self.bcs = bcs
        else:
            bcs_y = bcs[0]
            bcs_m = bcs[1]
            assert isinstance(bcs_y(0), list), "Bcs must be a list"
            assert isinstance(bcs_m(0), list), "Bcs must be a list"
            self.bcs_y = bcs_y
            self.bcs_m = bcs_m

        if self.pressure_bc_flag:
            assert isinstance(pressure_bcs, list), "Bcs must be a list"
            self.pressure_bc_list = pressure_bcs
