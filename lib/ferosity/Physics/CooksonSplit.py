"""
@author: Giulia Ferla, Micol Bassanini
"""

import dolfin as df
from ferosity.Physics.AbstractPhysics import AbstractPhysics
from ferosity.ConstitutiveModeling.PoromechanicsEnergy import PoromechanicsEnergy
from ferosity.Projection import Projection
from ferosity.AndersonAcceleration import AndersonAcceleration
from petsc4py import PETSc
import numpy as np


class CooksonSplit(AbstractPhysics):
    def __init__(self, mesh, name, params, skeleton_energy, volume_energy, dsNeumann):

        super().__init__(mesh, name)

        self.dsNeumann = dsNeumann

        # FE
        self.degree_solid = params["degree solid"]
        self.degree_mass = params["degree mass"]
        self.N_compartments = params["number compartments"]

        # Time
        self.t0 = params["t0"]
        self.tf = params["tf"]
        self.dt = params["dt"]
        self.t = self.t0 + self.dt

        # Physical parameters
        self.rhof = params["rhof"]
        self.rhos = params["rhos"]
        self.phi0_vec = params["initial porosities"]
        assert isinstance(self.phi0_vec, list)
        self.phis = 1 - np.sum(self.phi0_vec)
        self.kf_vec = params["permeabilities"]
        assert isinstance(self.kf_vec, list)

        self.muf = params["muf"]
        self.beta = params["compartments interaction"]
        self.pressure_model = params["pressure model"]  # Either mono or mult
        self.pressure_bc_flag = params["pressure bc use"]

        self.scheme = params["scheme"]
        self.scheme_abs_tol = params["scheme absolute tol"]
        self.scheme_rel_tol = params["scheme relative tol"]
        # If using pressure boundary conditions, set parameters for fixed point iteration
        if self.pressure_bc_flag:

            self.pressure_bc_log = params["pressure bc log"]
            self.pressure_bc_tol = params["pressure bc tol"]

            self.projection_L2_smooth = Projection(df.FunctionSpace(
                mesh, "CG", self.degree_mass), type="H1")
        self.maxiter = params["maxiter"]
        # Output
        self.projection_L2 = Projection(df.FunctionSpace(
            mesh, "CG", self.degree_mass), type="L2")
        self.export_solutions = params["export solutions"]
        self.save_every = params["save every"]
        self.flag_semi = params["flag semi implicit"]

        # Functional setting
        el_s = df.VectorElement("CG", mesh.ufl_cell(), self.degree_solid)
        el_m = df.VectorElement("CG", mesh.ufl_cell(),
                                self.degree_mass, self.N_compartments)
        self.Vs = df.FunctionSpace(mesh, el_s)
        self.Vm = df.FunctionSpace(mesh, el_m)
        self.V = df.FunctionSpace(mesh, df.MixedElement([el_s, el_m]))

        self.solution = df.Function(self.V)
        self.solution_n = df.Function(self.V)
        self.solution_nn = df.Function(self.V)
        self.ys = df.Function(self.Vs)
        self.m = df.Function(self.Vm)
        self.ys_n = df.Function(self.Vs)
        self.m_n = df.Function(self.Vm)
        self.ys_nn = df.Function(self.Vs)
        self.m_nn = df.Function(self.Vm)
        self.ys_prev = df.Function(self.Vs)
        self.ms_prev = df.Function(self.Vm)
        self.iterations = params["compare iterations"]
        if self.iterations:
            self.iter = []  # iteration fixed point
            self.newton_mass = []  # newton iterations m
            self.newton_y = []  # newton iterations y
        self.convergence_flag = params["convergence_flag"]
        if self.convergence_flag:
            self.y_sol = []
            self.m_sol = []
        self.energy = PoromechanicsEnergy(
            params, skeleton_energy, volume_energy)

    def setup_loads(self, f_vol_solid, f_sur_solid, f_vol_mass, f_sur_mass):
        self.f_vol_solid = f_vol_solid
        self.f_sur_solid = f_sur_solid
        self.f_vol_mass = f_vol_mass
        self.f_sur_mass = f_sur_mass

    def solve_timestep(self):

        ws = df.TrialFunction(self.Vs)
        q = df.TrialFunction(self.Vm)

        it = 0
        it1 = 0
        it2 = 0
        Fy0_norm = 0
        Fm0_norm = 0
        # assemble

        # Residual vectors
        Fyvec = df.PETScVector()
        Fmvec = df.PETScVector()
        while it < self.maxiter:
            F = df.grad(self.ys) + df.Identity(self.dim)

            Fy = df.Constant(self.rhos * (self.phis)) * df.dot(self.ys - 2 * self.ys_n + self.ys_nn, ws) * df.dx + \
                df.Constant(self.dt**2) * \
                df.inner(self.energy.getPiola(F, self.m), df.grad(ws)) * df.dx -\
                df.Constant(self.dt**2) * \
                df.inner(self.f_sur_solid(self.t), ws) * self.dsNeumann -\
                df.Constant(self.dt**2) * \
                df.inner(self.f_vol_solid(self.t), ws) * df.dx
            problem = df.NonlinearVariationalProblem(
                Fy, self.ys, self.bcs_y(self.t), df.derivative(Fy, self.ys))
            solver = df.NonlinearVariationalSolver(problem)
            prm = solver.parameters
            prm['newton_solver']['absolute_tolerance'] = 1e-3 * self.scheme_abs_tol
            prm['newton_solver']['relative_tolerance'] = 1e-3 * self.scheme_rel_tol
            # prm['newton_solver']['maximum_iterations'] = 100

            # Initial residual norm
            if it == 0:
                df.assemble(Fy, tensor=Fyvec)
                Fy0_norm = Fyvec.norm('linf')
                if Fy0_norm < 3e-15:
                    Fy0_norm = 1

            (iteration1, conv1) = solver.solve()
            it1 += iteration1

            bcs = []
            if self.pressure_bc_flag:

                # Generate boundary conditions
                for pbc in self.pressure_bc_list:
                    comp = pbc.compartment
                    markers = pbc.markers
                    tag = pbc.tag
                    function = pbc.function

                    # Compute added mass in current compartment as projection
                    m_ufl = self.energy.getInversePressure(
                        df.det(F), function(self.t), self.m, comp)
                    m_val = self.projection_L2_smooth.project(m_ufl)
                    bc = df.DirichletBC(self.Vm.extract_sub_space(
                        [comp]), m_val, markers, tag)
                    bcs.append(bc)

            Fm = 0
            J = df.det(F)
            if self.flag_semi:
                p = self.energy.getPressure(F, self.m_n)  # Implicit scheme
            else:
                p = self.energy.getPressure(F, self.m)  # Semi-implicit scheme
            for comp in range(self.N_compartments):
                Ki = df.det(F) * df.inv(F) * \
                    df.Constant(self.kf_vec[comp]) * df.inv(F.T)
                if (self.flag_semi):
                    Fm += (self.m[comp] - self.m_n[comp]) * q[comp] * df.dx + df.Constant(
                        self.dt * self.rhof) * df.inner(Ki * self.energy.gradPressureSemi(F, self.m[comp], self.m_n[comp], comp), df.grad(q[comp])) * df.dx - \
                        df.Constant(self.dt * self.rhof) * \
                        df.inner(self.f_vol_mass(self.t)[
                                 comp], q[comp]) * df.dx(self.mesh)
                else:
                    Fm += (self.m[comp] - self.m_n[comp]) * q[comp] * df.dx + df.Constant(
                        self.dt * self.rhof) * df.inner(Ki * df.grad(p[comp]), df.grad(q[comp])) * df.dx -\
                        df.Constant(self.dt * self.rhof) * \
                        df.inner(self.f_vol_mass(self.t)[
                                 comp], q[comp]) * df.dx(self.mesh)
                for comp2 in range(self.N_compartments):
                    Fm += (df.Constant(self.dt * self.rhof) * J * df.inner(self.beta[comp][comp2] *
                                                                           (p[comp] - p[comp2]), q[comp])) * df.dx(domain=self.mesh)
            problem = df.NonlinearVariationalProblem(
                Fm, self.m, self.bcs_m(self.t) + bcs, df.derivative(Fm, self.m))
            solver = df.NonlinearVariationalSolver(problem)
            prm = solver.parameters
            prm['newton_solver']['absolute_tolerance'] = 1e-3 * self.scheme_abs_tol
            prm['newton_solver']['relative_tolerance'] = 1e-3 * self.scheme_rel_tol
            # prm['newton_solver']['maximum_iterations'] = 100

            # Initial residual norm
            if it == 0:
                df.assemble(Fm, tensor=Fmvec)
                Fm0_norm = Fmvec.norm('linf')
                if Fm0_norm < 3e-15:
                    Fm0_norm = 1

            (iteration2, conv2) = solver.solve()

            it2 += iteration2


            # self.m.vector()[:]=omega*self.m.vector()[:]+(1-omega)*self.ms_prev.vector()[:]

            df.assemble(Fy, tensor=Fyvec)
            df.assemble(Fm, tensor=Fmvec)

            for b in self.bcs_y(self.t):
                new_bc = df.DirichletBC(b)
                new_bc.homogenize()
                new_bc.apply(Fyvec)
            for b in (self.bcs_m(self.t) + bcs):
                new_bc = df.DirichletBC(b)
                new_bc.homogenize()
                new_bc.apply(Fmvec)

            Fy_norm = Fyvec.norm('linf')
            Fm_norm = Fmvec.norm('linf')
            error = np.max([Fy_norm, Fm_norm])
            #error = Fy_norm + Fm_norm
            # oppure err=abs(ys_prev-ys) and abs(ms_prev-m)
            # np.abs(self.ys.vector()[:]-self.ys_prev.vector()[:]).max()
            # error_y = (self.ys.vector().vec() - self.ys_prev.vector().vec()).norm(PETSc.NormType.NORM_INFINITY)
            # error_m = (self.m.vector().vec() - self.ms_prev.vector().vec()).norm(PETSc.NormType.NORM_INFINITY)

            it += 1

            if self.scheme == "staggered":
                break

            if error < self.scheme_abs_tol:
                print("-----------Convergence, absolute tolerance attained in {} iterations".format(it))
                break

            if np.max([Fy_norm / Fy0_norm, Fm_norm / Fm0_norm]) < self.scheme_rel_tol:
                print("-----------Convergence, relative tolerance attained in {} iterations".format(it))
                break

            print("--------- Fixed point iteration {}, it_y={:.2e}, it_m={:.2e}".format(it, it1,it2))

        if self.iterations:
            self.newton_y.append(it1)
            self.newton_mass.append(it2)
            self.iter.append(it)
        if self.convergence_flag:

            self.y_sol.append(self.ys.copy(True))
            self.m_sol.append(self.m.copy(True))

        df.assign(self.ys_nn, self.ys_n)
        df.assign(self.ys_n, self.ys)
        df.assign(self.m_n, self.m)
        df.assign(self.ms_prev, self.m)

    def export(self, t):
        ys = df.Function(self.V.sub(0).collapse(), name="displacement")
        m = df.Function(self.V.sub(1).collapse(), name="added mass")
        p = df.Function(self.V.sub(1).collapse(), name="pressure")

        Vm = df.FunctionSpace(self.mesh, "CG", self.degree_mass)
        m_alloc = df.Function(Vm)
        p_alloc = df.Function(Vm)

        df.assign(ys, self.ys)
        df.assign(m, self.m)

        p_ufl = self.energy.getPressure(df.grad(ys) + df.Identity(self.dim), m)
        df.assign(p, df.project(p_ufl, self.V.sub(1).collapse()))

        self.xdmf.write(ys, t)
        for comp in range(self.N_compartments):
            m_alloc.rename("m{}".format(comp), "m{}".format(comp))
            p_alloc.rename("p{}".format(comp), "p{}".format(comp))

            df.assign(m_alloc, self.projection_L2.project(m[comp]))
            df.assign(p_alloc, self.projection_L2.project(p[comp]))

            self.xdmf.write(m_alloc, t)
            self.xdmf.write(p_alloc, t)

    def set_bcs(self, bcs_y, bcs_m, pressure_bcs=None):
        """
        Extend boundary conditions to include pressure conditions
        """
        assert isinstance(bcs_y(0), list), "Bcs must be a list"
        assert isinstance(bcs_m(0), list), "Bcs must be a list"
        self.bcs_y = bcs_y
        self.bcs_m = bcs_m

        if self.pressure_bc_flag:
            assert isinstance(pressure_bcs, list), "Bcs must be a list"
            self.pressure_bc_list = pressure_bcs
