#!/usr/bin/env python3
"""
Created on 20/03/2020

@author: Nicolas Barnafi
"""

import dolfin as df
from mpi4py import MPI

class AbstractPhysics:
    """
    Abstract interface for time dependent physics.
    """

    def __init__(self, mesh, name):
        """
        Abstract initialization, sets up export file.
        """
        self.name = name
        self.mesh = mesh
        self.dim = mesh.topology().dim()

        # Export
        import os
        assert "FEROSITY_OUTPUT_DIR" in os.environ.keys(
        ), "Output directory not setup correctly, run setup.sh to export FEROSITY_OUTPUT_DIR"
        out_dir = os.environ["FEROSITY_OUTPUT_DIR"]
        self.xdmf = df.XDMFFile(
            "{}/{}.xdmf".format(out_dir, name))
        self.xdmf.parameters["functions_share_mesh"] = True
        self.xdmf.parameters["flush_output"] = True

    def solve_timestep(self):
        pass

    def export(self, t):
        pass

    def setup(self):
        pass

    def set_bcs(self, bcs):
        assert isinstance(bcs(0), list), "Bcs must be a list"
        self.bcs = bcs

    def solve(self):
        """
        Abstract solution method which solves the problem throughout all timesteps.
        """

        from time import time
        current_time = time()
        if MPI.rank(MPI.comm_world) == 0:
            print("\n\n--- Begining simulation of {}".format(self.name))

        if self.export_solutions:
            self.export(0)

        it = 1
        while self.t < self.tf:
            self.t += self.dt
            self.solve_timestep()
            if MPI.rank(MPI.comm_world) == 0:
                print("------ Solved time t={:.4f} in {:.2e}s".format(self.t,
                                                              time() - current_time), flush=True)
            if self.export_solutions and it % self.save_every == 0:
                self.export(self.t)
            current_time = time()
            it+=1

        self.xdmf.close()
