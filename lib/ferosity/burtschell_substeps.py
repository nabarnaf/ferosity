#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 16:52:58 2018

@author: barnafi
"""

from dolfin import *
from lib.Libs import *
from lib.Functions import *
DEBUG = False

def explicitStep(rhof, phi0, gamma, mu, h, dt, dim, theta, package_n, package_nn,
                 mesh, dx, ds, dsNnos, dsNnof, V, Functions, bcs=None, drainageTest=False):

    uf_n,  bubble_n,  m_n,  us_n,  vs_n = package_n.getFunctions()
    uf_nn, bubble_nn, m_nn, us_nn, vs_nn = package_n.getFunctions()
    phi_n  = Functions.phi(m_n , us_n)
    phi_nn = Functions.phi(m_nn, us_nn)
    # Auxiliary expressions
    vs_nmm = (us_n - us_nn)/dt

    nu = FacetNormal(mesh)

    # Functional setting
    _vf_tilde, bubb = TrialFunctions(V)
    _wf, bubb_test = TestFunctions(V)
    vf_tilde = _vf_tilde + bubb
    wf = _wf + bubb_test

    if drainageTest:
        dPdm = Functions.dPdm
        p_n = 0.5*rhof*(dPdm(us_n, m_n) + dPdm(us_n, m_nn))
        theta = -rhof*0.01*p_n

    # Problem definition
    ALE = ALELinearForm(mesh)

    eps = Functions.eps
    F_np1 = (rhof*phi_n/dt*dot(vf_tilde, wf)
        + 2*mu*phi_n*inner(eps(vf_tilde), eps(wf))
        - theta*dot(vf_tilde, wf))*dx \
        + (gamma*mu/h*phi_n*dot(vf_tilde - vs_nmm, wf)
                - 2*mu*phi_n*dot(eps(vf_tilde)*nu, wf))*dsNnos \
        + (gamma*mu/h*phi_n*dot(vf_tilde - vs_nmm, nu)*dot(wf, nu)
                - 2*mu*phi_n*dot(eps(vf_tilde)*nu, wf))*dsNnof

    ufb_n = uf_n + bubble_n
    F_n = (-rhof/dt*phi_nn*dot(ufb_n, wf)
        + rhof*dot( div(phi_nn*outer(vf_tilde, ufb_n - vs_nmm)), wf))*dx

    ALE.addDisplacementAndForm(us_nn, F_n)
    ALE.addDisplacementAndForm(us_n, F_np1)

    if bcs:
        for b in bcs:
            ALE.addBC(b)
    uf_tilde = Function(V)
    ALE.solve(uf_tilde)
    if DEBUG:
        plot(uf_tilde.sub(0), key="uf_tilde", interactive=False, title="uf_tilde")
    return uf_tilde

def fluidmassStep(tNeumann, tNoFlux, D, rhof, phi0, gamma, mu, h, dt, dim, theta, f,
                  package_n, package_nn, package_k, uf_tilde_f, uf_tilde, 
                  mesh, dx, ds, dsN, dsNnof, V, bcs, Functions, drainageTest=False):

    uf_k,  bubble_k,  m_k,  us_k,  vs_k  = package_k.getFunctions() 
    uf_n,  bubble_n,  m_n,  us_n,  vs_n  = package_n.getFunctions()
    uf_nn, bubble_nn, m_nn, us_nn, vs_nn = package_n.getFunctions()
    phi_n  = Functions.phi(m_n , us_n)

    # Auxiliary expressions
    vs_npm = 1/dt*(us_k - us_n)

    # Functional setting
    Sol = Function(V)
    _uf, m, bubb23 = split(Sol)
    _wf, q, bubble_test23 = TestFunctions(V)

    uf = _uf + bubb23
    wf = _wf + bubble_test23

    p_np1 = Functions.pressure(rhof, us_n, m, m_n)

    # Set initial Newton guess as previous step
    assign(Sol.sub(0), uf_k)
    assign(Sol.sub(1), m_k)
    assign(Sol.sub(2), bubble_k)

    ALE = ALENonLinearForm(mesh, V)
    for bc in bcs:
        ALE.addBC(bc)

    if drainageTest:
        theta = -rhof*0.01*p_np1

    # Problem definition
    F20   = 1/dt*(m - m_n)*q*dx
    F2np1 = (rhof*div(phi_n*(uf - vs_npm))*q 
            - theta*q
            + rhof*phi_n/dt*dot(uf - uf_tilde, wf)
            - p_np1*div(phi_n*wf)
            + phi_n**2*dot(D*(uf - vs_npm), wf)
            - rhof*phi_n*dot(f, wf))*dx \
            - phi_n*dot(tNeumann, wf)*dsN \
            - phi_n*dot(tNoFlux, wf)*dsNnof

    u_zero = Function(us_n.function_space())
    ALE.addDisplacementAndForm(u_zero, F20)
    ALE.addDisplacementAndForm(us_n, F2np1)
    ALE.solve(Sol)
    if DEBUG:
        plot(Sol.sub(0), interactive = False, key = "duf", title="duf")
        plot(Sol.sub(1), key="m_k", title="m", mode="color")
    return Sol.sub(0), Sol.sub(1), Sol.sub(2) # fluid, mass, bubble


def solidStep(tNeumann, tNoSlid, tNoFlux,
              D, rhof, rhos0, phi0, mu, gamma, etad, h, dt, dim, theta, f,
              package_n, package_nn, package_k, uf_tilde, 
              mesh, dx, ds, dsN, dsNnos, dsNnof, V, bcs, Functions, drainageTest=False):

    _uf_k, bubble_k,  m_k,  us_k,  vs_k  = package_k.getFunctions() 
    uf_n,  bubble_n,  m_n,  us_n,  vs_n  = package_n.getFunctions()
    uf_nn, bubble_nn, m_nn, us_nn, vs_nn = package_n.getFunctions()
    phi_n  = Functions.phi(m_n , us_n)
    phi_nn = Functions.phi(m_nn, us_nn)
    
    uf_k = _uf_k + bubble_k

    def ufl_norm(U):
        return sqrt(inner(U, U))
#    def E(disp):
#        return 0.5*(F(disp).T*F(disp) - Identity(dim))
    eps  = Functions.eps
    dE   = Functions.dE
    F    = Functions.F
    J    = Functions.J
    dPdE = Functions.dPdE

    nu = FacetNormal(mesh)
    # Functional setting
    us = Function(V)
    ws = TestFunction(V)

    
    # Auxiliary expressions
    vs_np1 = 2*(us - us_n)/dt - vs_n
    vs_npm = (us - us_n)/dt
    vs_nmm = (us_n - us_nn)/dt
    dPdE_npm = dPdE(0.5*(us + us_n), m_k)
    dE_npm   = dE( 0.5*(us+us_n), ws)
    dE_damp  = etad*dE(0.5*(us+us_n), vs_npm)
    
    # Surface traction terms
    TN    = tNeumann
    Tnos  = tNoSlid
    Tnof  = tNoFlux
    T0N   = J(us)*ufl_norm(inv(F(us).T)*nu)*TN
    T0nos = J(us)*ufl_norm(inv(F(us).T)*nu)*Tnos
    T0nof = J(us)*ufl_norm(inv(F(us).T)*nu)*Tnof
    
#    Pi = Identity(dim) - outer(nu, nu)

    # Set initial Newton guess as previous step
    assign(us, us_k)

    ALE = ALENonLinearForm(mesh, V)
    for b in bcs:
        ALE.addBC(b)

    p_np1 = Functions.pressure(rhof, us_n, m_k, m_n)
    
    if drainageTest:
        theta = -rhof*0.01*p_np1

    F30   = (rhos0*(1 - phi0)/dt*dot(vs_np1 - vs_n, ws)
            + inner(dPdE_npm + dE_damp, dE_npm)
            - rhos0*(1-phi0)*dot(f, ws))*dx \
            - (1-phi_n)*dot(T0N, ws)*dsN \
            - dot(T0nos, ws)*dsNnos \
            - dot(T0nof, ws)*dsNnof

    F3n   = (-rhof*phi_nn/dt*dot((uf_n+bubble_n), ws)
             + rhof*dot(div(phi_nn*outer(uf_tilde, (uf_n+bubble_n) - vs_nmm)), ws))*dx
    F3np1 =  (rhof*phi_n/dt*dot(uf_k, ws)
            - theta*dot(uf_tilde, ws)
            + 2*mu*phi_n*inner(eps(uf_tilde), eps(ws))
            - rhof*phi_n*dot(f, ws))*dx \
            - phi_n*dot(TN, ws)*dsN \
            + gamma*mu/h*phi_n*dot(vs_npm - vs_nmm, ws)*dsNnos \
            + gamma*mu/h*phi_n*dot(vs_npm - vs_nmm, nu)*dot(ws, nu)*dsNnof

    
    u_zero = Function(V)
    ALE.addDisplacementAndForm(u_zero, F30)
    ALE.addDisplacementAndForm(us_nn,  F3n)
    ALE.addDisplacementAndForm(us_n, F3np1)

    ALE.solve(us)
    return us
