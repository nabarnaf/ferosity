#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 19:01:23 2018

@author: barnafi
"""

from dolfin import *

class FunctionPackage:
    
    def __init__(self, uf=None, m=None, bubb=None, us=None, vs=None):
        self.uf   = uf
        self.m    = m
        self.bubb = bubb
        self.us   = us
        self.vs   = vs
        
    def updatePackage(self, package):
        if package.uf and self.uf:
            assign(self.uf, package.uf)
        if package.m and self.m:
            assign(self.m, package.m) 
        if package.us and self.us:
            assign(self.us, package.us)
        if package.vs and self.vs:
            assign(self.vs, package.vs)
        if package.bubb and self.bubb:
            assign(self.bubb, package.bubb)
        
    def copyPackage(self):
        P = FunctionPackage()
        if self.uf:
            P.uf = self.uf.copy(True)
        else:
            P.uf = None
        if self.m:
            P.m  = self.m.copy(True)
        else:
            P.m  = None
        if self.us:
            P.us = self.us.copy(True)
        else:
            P.us = None
        if self.vs:
            P.vs = self.vs.copy(True)
        else:
            P.vs = None
        if self.bubb:
            P.bubb = self.bubb.copy(True)
        else:
            P.bubb = None
        return P
    
    def getFunctions(self):
        return self.uf, self.bubb, self.m, self.us, self.vs
    
    def error(self, refPack):
        uf,   bubb,   m,   us      = self.uf, self.bubb, self.m, self.us
        uf_n, bubb_n, m_n, us_n, _ = refPack.getFunctions()
        return errornorm(us, us_n, degree_rise = 0) + errornorm(uf, uf_n, degree_rise = 0) \
                    + errornorm(m, m_n, degree_rise = 0) + errornorm(bubb, bubb_n, degree_rise = 0)
        

class Functions:

    def __init__(self, dim, k1, k2, kp, ks, etapor, phi0, rhof):
        """
        Setup all necessary variables for constitutive modelling
        """
        self.dim    = Constant(dim)
        self.rhof   = Constant(rhof)
        self.phi0   = Constant(phi0)
        self.k1     = Constant(k1)
        self.k2     = Constant(k2)
        self.kp     = Constant(kp)
        self.ks     = Constant(ks)
        self.etapor = Constant(etapor)
        self.I      = Identity(dim)

    def eps(self, vec):
        return sym(grad(vec))


    def F(self, disp):
        return grad(disp) + self.I

    def J(self, disp):
        return det(grad(disp)+self.I)

    def Psi_exp(self, C, J_s):
        """
        Inner method, used for calculating the solid energy
        """
        d = self.dim
        I1, I2, I3 = tr(C), 0.5*(tr(C)**2 - tr(C*C)), det(C)
        pressure_reg = lambda J: ( J - 1 - ln(J) )
        return self.k1*(I1*I3**(-1./d) - d) + self.k2*(I2*I3**(-2./d) - d) \
            + self.kp*pressure_reg(J_s/(1 - self.phi0)) \
            + self.ks*pressure_reg(sqrt(I3)) \
            - self.etapor*ln(sqrt(I3) - J_s)

    def Psi(self, C, m):
        _J = sqrt(det(C))
        J_s = _J - m/self.rhof - self.phi0
        return  self.Psi_exp(C, J_s)

#    def PsiJs(self, C, Js, m):
#        return  self.Psi_exp(C, Js, m)

    def dE(self, disp, vec):
        F = self.F(disp)
        return 0.5*(F.T*grad(vec) + grad(vec).T*F)

    def dPdE(self, disp, m):
        # We use dP/dE = 2 dP/dC
        F = self.F(disp)
        C = F.T*F
        C = variable(C)    
        return 2*diff(self.Psi(C, m), C)
#        J = sqrt(det(C))
#        J_s = J - m/self.rhof - self.phi0
#        return 2*diff(self.PsiJs(C, J_s, m), C)

    def dPdm(self, disp, m):
        F = self.F(disp)
        C = F.T*F
        m = variable(m)
        return diff(self.Psi(C, m), m)

    def phi(self, m, us):
        return (m/self.rhof + self.phi0)/self.J(us)
    
    def pressure(self, rhof, us, m, m_n):
        F = self.F(us)
        C = F.T*F
#        return rhof*(self.Psi(C, m) - self.Psi(C, m_n))/(m - m_n)
        return 0.5*rhof*(self.dPdm(us, m) + self.dPdm(us, m_n))



def Ks(rhos0, phi0, us, dx):
    return assemble(rhos0/2*(1-phi0)*dot(us, us)*dx)

def Kf(mesh, rhof, phi, uf, us_nn, dx):
    coor = mesh.coordinates().copy()
    ALE.move(mesh, us_nn)
    out = assemble(rhof/2*phi*dot(uf, uf)*dx)
    mesh.coordinates()[:] = coor
    return out

def W(Psi, dx):
    return assemble(Psi*dx)

def S1(mesh, rhof, uf_tilde, us_nn, phi_nm1, uf_n, vs_nmm, Vm):
    coor = mesh.coordinates().copy()
    ALE.move(mesh, us_nn)
    ufnorm = dot(uf_tilde, uf_tilde)
    Pufnorm = project(ufnorm, Vm)

    form = (ufnorm - Pufnorm)*div(rhof*phi_nm1*(uf_n - vs_nmm))*dx
    out = 0.5*assemble(form)
    mesh.coordinates()[:] = coor
    return out


def S2(mesh, rhof, dt, Vm, us_n, us_nn, phi_n, phi_nn, uf_tilde):
    coor = mesh.coordinates().copy()
    ufnorm = dot(uf_tilde, uf_tilde)
    Pufnorm = project(ufnorm, Vm)

    # First perform integration on Omega_n
    ALE.move(mesh, us_nn)
    O1 = rhof/(2*dt)*(phi_nn*Pufnorm - phi_nn*ufnorm)*dx
    O1 = assemble(O1)
    mesh.coordinates()[:] = coor
    # Then perform integration on Omega_nn
    ALE.move(mesh, us_n)
    O2 = rhof/(2*dt)*(-phi_n*Pufnorm + phi_n*ufnorm)*dx
    O2 = assemble(O2)
    mesh.coordinates()[:] = coor
    return O1 + O2


def S3(Vm, rhof, dt, uf_tilde, phi_n, phi_nn, J_n, J_nn, m_n, m_nn, dx):
    ufnorm = dot(uf_tilde, uf_tilde)
    Pufnorm = project(ufnorm, Vm)
    P1 = (phi_n*J_n - phi_nn*J_nn)
    P2 = (m_n - m_nn)
    return 0.5*assemble( (rhof*P1 - P2)/dt*Pufnorm*dx )

def S4(mesh, dSNnos, us_n, vs_npm, vs_nmm, gamma, mu, h, phi_n):
    coor = mesh.coordinates().copy()
    ALE.move(mesh, us_n)
    form = gamma*mu/(2*h)*phi_n*(dot(vs_npm, vs_npm) - dot(vs_nmm, vs_nmm))*dSNnos
    out = assemble(form)
    mesh.coordinates()[:] = coor
    return out