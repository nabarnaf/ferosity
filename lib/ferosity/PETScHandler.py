#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 11:15:11 2019

@author: barnafi
"""

from petsc4py import PETSc
from dolfin import PETScMatrix, PETScVector

def genPETScMat(n, m):
    A = PETSc.Mat().create()
    A.setSizes([n, m])
    A.setType("aij")
    A.setUp()
    return PETScMatrix(A)

def genPETScVec(n):
    return PETScVector(PETSc.Vec().createSeq(n))

def getTranspose(A):
    out = PETSc.Mat()
    A.mat().transpose(out = out)
    return PETScMatrix(out)

def getSubBlock(iArr, jArr, A):
    from scipy.sparse import csr_matrix
    out = genPETScMat(len(iArr), len(jArr))
    csr = asScipySparse(A)[iArr][:, jArr]
    out.mat().setValuesCSR(csr.indptr, csr.indices, csr.data)
    out.mat().assemble()
    return out

def asScipySparse(A):
    from scipy.sparse import csr_matrix
    # Can't get CSR of unassembled matrix
    assert(A.mat().isAssembled())
    CSR = A.mat().getValuesCSR()
    csr = csr_matrix(CSR[::-1], shape=(A.size(0), A.size(1)))
    # Filtering is strange in csr
    return csr