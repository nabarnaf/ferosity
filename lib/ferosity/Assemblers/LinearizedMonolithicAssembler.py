#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 12:12:06 2019

@author: barnafi
"""

from ferosity.GLOBAL_VARIABLES import *

class LinearizedMonolithicAssembler:

    def __init__(self, mesh, PhysicalParameters, ExternalLoads, Markers):

        self.externalLoads = ExternalLoads

        # Set parameters
        self.dt    = Constant(PhysicalParameters.dt)
        self.rhof  = Constant(PhysicalParameters.rhof)
        self.rhos0 = Constant(PhysicalParameters.rhos0)
        self.E     = Constant(PhysicalParameters.E)
        self.nu    = Constant(PhysicalParameters.nu)
        self.mesh  = mesh
        E  = PhysicalParameters.E
        nu = PhysicalParameters.nu
        self.lmbda = Constant(E*nu/((1+nu)*(1-2*nu)))
        self.mu_s  = Constant(E/(2*(1+nu)))
        self.ks    = Constant(PhysicalParameters.ks)
        self.mu_f  = Constant(PhysicalParameters.mu_f)
        self.gamma = Constant(PhysicalParameters.gamma)
        self.phi   = PhysicalParameters.phi
        self.D     = Constant(PhysicalParameters.D)
        self.dim   = PhysicalParameters.dim
        self.AS    = Constant(PhysicalParameters.AS)
        self.active_stress = PhysicalParameters.active_stress
        self.approach = PhysicalParameters.approach

        # Set measures
        self.dx = Measure('dx', domain=mesh, metadata={'optimize':True})
        self.ds = Measure('ds', domain=mesh, subdomain_data=Markers.markers, metadata={'optimize':True})
        dsEmpty = ds(Markers.NONE)
        self.dSNs  = sum([self.ds(i) for i in Markers.neumannSolidMarkers], dsEmpty)
        self.dSNf  = sum([self.ds(i) for i in Markers.neumannFluidMarkers], dsEmpty)
        self.dSnos = sum([self.ds(i) for i in Markers.noSlipMarkers], dsEmpty)

        self.steadyMatrixAssembled=False
        if self.active_stress:
            mpiprint("----- Setting active stress")
            # Load fibers
            V = VectorFunctionSpace(self.mesh, 'CG', 1)
            rr = HDF5File(MPI.comm_world, "fibers/fibers_4mm.h5", "r")
            f0 = Function(V)
            rr.read(f0, "f0")
            ufl_norm = lambda _x: sqrt(dot(_x, _x))
            self.f0 = f0/ufl_norm(f0)

    def getBilinearForm(self, V, t):

        # Get trial and test functions
        ys, us, uf, p = split(TrialFunction(V))
        ws, vs, vf, q = split(TestFunction(V))

        dx = self.dx
        theta = self.externalLoads.theta(t)
        h     = Constant(V.mesh().hmax())
        phi   = self.phi(t)
        phis  = 1 - phi

        if not self.steadyMatrixAssembled:

            sigma_skel = lambda arg: self.C(self.eps(arg))
            sigma_vis  = 2*self.mu_f*self.eps(uf)
            dt = self.dt

            # Setup forms
            n = FacetNormal(self.mesh)
            if self.approach == "Default":
                aafnotheta  = (phi*inner(sigma_vis, self.eps(vf)) - p*div(phi*vf)
                        + phi**2*dot(self.D*uf, vf)  )*dx \
                        + (self.gamma/h*dot(uf - us, vf) - dot((sigma_vis - p*Identity(self.dim))*n, vf))*self.dSnos

                aaf  = aafnotheta - theta*dot(uf, vf)*dx

                aam  =  q*div(phi*uf)*dx

                aas  = (inner(sigma_skel(ys), self.eps(ws)) - p*div((1-phi)*ws)
                    - phi**2*dot(self.D*uf , ws))*dx

                aavs = dot(us, vs)*dx

                

                aafnotheta = dt * aafnotheta + (self.rhof*phi*dot(uf, vf)
                         - phi**2*dot(self.D*ys, vf))*dx
                aaf  = dt * aaf + (self.rhof*phi*dot(uf, vf)
                         - phi**2*dot(self.D*ys, vf))*dx
                aam  = dt * aam + ((1-phi)**2/(self.ks)*p*q
                         + q*div((1-phi)*ys))*dx
                aas  = dt * aas + (self.rhos0*(1 - phi)*dot(us, ws)
                        + phi**2*dot(self.D*ys, ws) )*dx
                aavs = dt * aavs - dot(ys, vs)*dx

            elif self.approach == "Burtschell":
                aafnotheta  = (phi*inner(sigma_vis, self.eps(vf)) - p*div(phi*vf)
                        + phi**2*dot(self.D*(uf-us), vf)  )*dx \
                        + (self.gamma/h*dot(uf - us, vf) - dot((sigma_vis - p*Identity(self.dim))*n, vf))*self.dSnos

                aaf  = aafnotheta - theta*dot(uf, vf)*dx

                aam  =  q*div(phi*uf + (1-phi)*us)*dx

                aas  = (inner(sigma_skel(ys), self.eps(ws)) - p*div((1-phi)*ws)
                    - phi**2*dot(self.D*(uf-us) , ws))*dx

                aavs = -inner(sigma_skel(us), self.eps(vs))*dx

                # Inertia terms

                aafnotheta = dt * aafnotheta + (self.rhof*phi*dot(uf, vf))*dx
                aaf  = dt * aaf + self.rhof*phi*dot(uf, vf)*dx
                aam  = dt * aam + (1-phi)**2/self.ks*p*q*dx
                aas  = dt * aas + (self.rhos0*(1 - phi)*dot(us, ws) )*dx
                aavs = dt * aavs + inner(sigma_skel(ys), self.eps(vs))*dx

            A = assemble(aafnotheta + aas + aam + aavs)
            self.A = A
            self.steadyMatrixAssembled = True

            Aout = assemble(aaf + aas + aam + aavs)
            return Aout
        else:

            # Assemble missing block
            atheta = -self.dt*theta*dot(uf, vf)*dx

            Atheta = assemble(atheta)
            return Atheta + self.A


    def getLoad(self, V, t, ys_n, us_n, uf_n, p_n):

        dx = self.dx
        ws, vs, vf, q = split(TestFunction(V))

        ff = self.externalLoads.ff(t)
        fs = self.externalLoads.fs(t)
        tf = self.externalLoads.tf(t)
        ts = self.externalLoads.ts(t)
        theta = self.externalLoads.theta(t)
        phi = self.phi(t)
        phis = 1 - phi
        dt = self.dt

        if self.approach == "Default":
            Ff  = (self.rhof*phi*dot(ff, vf))*dx \
                + phi*dot(tf, vf)*self.dSNf
            Fm  = 1/self.rhof*theta*q*dx
            Fs  = self.rhos0*phis*dot(fs, ws)*dx \
                + dot(ts, ws)*self.dSNs

            Ff  = dt * Ff + (self.rhof*phi*dot(uf_n,vf)
                    - phi**2*dot(self.D*ys_n, vf) )*dx
            Fm  = dt * Fm + (phis**2/self.ks*p_n*q
                     + q*div( phis*ys_n))*dx
            Fs  = dt * Fs + (self.rhos0*phis*dot(us_n, ws)
                    + phi**2*dot(self.D*ys_n, ws))*dx
            Fvs  = -dot(ys_n, vs)*dx
        elif self.approach == "Burtschell":
            Ff  = (self.rhof*phi*dot(ff, vf))*dx \
                + phi*dot(tf, vf)*self.dSNf
            Fm  = 1/self.rhof*theta*q*dx
            Fs  = self.rhos0*phis*dot(fs, ws)*dx \
                + dot(ts, ws)*self.dSNs

            
            Ff  = dt * Ff + self.rhof*phi*dot(uf_n,vf)*dx
            Fm  = dt * Fm + phis**2/self.ks*p_n*q*dx
            Fs  = dt * Fs + self.rhos0*phis*dot(us_n, ws)*dx
            Fvs = inner(self.C(self.eps(ys_n)), self.eps(vs))*dx

        if self.active_stress:
            f0 = self.f0

            # Set active Piola
            F = Identity(self.dim) + grad(ys_n)
            C = F.T*F
            I4f = dot(f0, C*f0)
            Ta = Constant(self.AS*(sin(DOLFIN_PI*t))**2)  # Heartbeat in 1 second
            Fs += -dt*Ta*inner(outer(f0, F*f0)/sqrt(I4f), grad(ws))*dx
        return assemble(Ff + Fm + Fs + Fvs)

    def C(self, ten):
        return 2*self.mu_s*ten + self.lmbda*tr(ten)*Identity(self.dim)

    def eps(self, vec):
        return sym(grad(vec))

    def m(self, p, ys):
        return self.rhof*((1 - self.phi)**2/self.ks*p + div(ys))