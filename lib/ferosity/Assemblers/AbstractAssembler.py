#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 10:21:31 2019

@author: barnafi
"""

from dolfin import *

class AbstractLinearAssembler:

    def __init__(self, name, mesh, PhysicalParameters, ExternalLoads, Markers):

        self.name = name
        self.externalLoads = ExternalLoads

        # Set parameters
        self.dt    = Constant(PhysicalParameters.dt)
        self.rhof  = Constant(PhysicalParameters.rhof)
        self.rhos0 = self.rhos = Constant(PhysicalParameters.rhos0)
        self.E     = Constant(PhysicalParameters.E)
        self.nu    = Constant(PhysicalParameters.nu)
        self.mesh  = mesh
        E  = PhysicalParameters.E
        nu = PhysicalParameters.nu
        self.lmbda = Constant(E*nu/((1+nu)*(1-2*nu)))
        self.mu_s  = Constant(E/(2*(1+nu)))
        self.ks    = Constant(PhysicalParameters.ks)
        self.mu_f  = Constant(PhysicalParameters.mu_f)
        self.gamma = Constant(PhysicalParameters.gamma)
        self.phi   = PhysicalParameters.phi
        self.D     = Constant(PhysicalParameters.D)
        self.dim   = PhysicalParameters.dim
        self.active_stress = PhysicalParameters.active_stress
        self.AS    = Constant(PhysicalParameters.AS)
        self.beta  = Constant(PhysicalParameters.beta)

        # Set measures
        self.dx = Measure('dx', domain=mesh, metadata={'optimize':True})
        self.ds = Measure('ds', domain=mesh, subdomain_data=Markers.markers, metadata={'optimize':True})
        dsEmpty = ds(Markers.NONE)
        self.dSNs  = sum([self.ds(i) for i in Markers.neumannSolidMarkers], dsEmpty)
        self.dSNf  = sum([self.ds(i) for i in Markers.neumannFluidMarkers], dsEmpty)
        self.dSnos = sum([self.ds(i) for i in Markers.noSlipMarkers], dsEmpty)

        self.steadyMatrixAssembled=False

    def C(self, ten):
        return 2*self.mu_s*ten + self.lmbda*tr(ten)*Identity(self.dim)

    def eps(self, vec):
        return sym(grad(vec))