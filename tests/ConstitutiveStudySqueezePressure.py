from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.CooksonMonolithic import CooksonMonolithic
from ferosity.MeshCreation import generateSquare
from ferosity.PressureBC import PressureBC

set_log_active(False)
parameters['form_compiler']['optimize'] = True
parameters['form_compiler']["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 10


name_noextension = "constitutive_squeezing/solution"


k1 = 2e3
k2 = 33

n_comps = 1
params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 2e3,
          "initial porosities": [0.1] * n_comps,
          "number compartments": n_comps,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 2,
          "dt": 5e-3,
          "export solutions": True,
          "save every": 2,
          "pressure model": "mono",
          "pressure bc use": True,
          "pressure bc tol": 1e-10,
          "pressure bc maxiter": 1000,
          "pressure bc log": False}


# Problem definition
def solve_problem(name, kp, ks):
  N = 10
  length = 1e-2

  mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

  skel_energy = CiarletGeymonat(k1, k2)
  vol_skel = LinpLog(kp)
  vol_energy = LinpLog(ks)

  def evalF(F): return skel_energy(F) + vol_skel(det(F))

  cookson = CooksonMonolithic(
      mesh, name, params, evalF, vol_energy, None, None)

  def ys_right(t): return Constant(length / 8 *
                                   sin(2 * pi * t)) if t <= 1 else Constant(0)

  def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                      DirichletBC(cookson.V.sub(0).sub(
                          1), Constant(0), markers, BOTTOM),
                      DirichletBC(cookson.V.sub(0).sub(0), ys_right(t), markers, RIGHT)]

  def pressure_func(t): return Constant(0.0)

  pressure_bc = [PressureBC(markers, LEFT, pressure_func, 0)]

  cookson.set_bcs(bcs, pressure_bc)
  cookson.solve()


def name(kp, ks): return name_noextension + "_{:.0e}_{:.0e}".format(kp, ks).replace("+", "")


k_list = [1e2, 1e3, 1e4]

for kp in k_list:
  for ks in k_list:
    solve_problem(name(kp, ks), kp, ks)
    solve_problem(name(kp, ks), kp, ks)
    solve_problem(name(kp, ks), kp, ks)
