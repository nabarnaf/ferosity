#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 13:49:33 2018

@author: barnafi
"""

from ferosity.GLOBAL_VARIABLES import *
import ferosity.LinearBurtschell as LB
from ferosity.LinearBurtschell import Markers
import ferosity.Libs as Libs
from parameters.LinearSwellingBurtschell import physical_params, output_params
from ferosity.Parameters import PhysicalParameters, ExternalForces, OutputParameters
from ferosity.Solvers.LinearizedMonolithicSolver import LinearizedMonolithicSolver as MonolithicSolver
from ferosity.MeshCreation import generateSquare

name = "linear_swelling_burtschell_comparison"

##################
# Code beggining #
##################

def getSmoothPhi(mesh):
    phi  = Expression("x[1]<l/2", l=length, degree = 2, domain=mesh)
    # Apply a bit of smoothing to phi
    V = FunctionSpace(mesh, 'CG', 2)
    pu, pv = TrialFunction(V), TestFunction(V)
    sol = Function(V)
    dxm = dx(mesh)
    solve(pu*pv*dxm + Constant(5e-8)*dot(grad(pu), grad(pv))*dxm == phi*pv*dxm, sol)
    file = XDMFFile('images/{}/phi_smooth.xdmf'.format(name))
    file.parameters["functions_share_mesh"] = True
    file.parameters["flush_output"] = True
    file.write(sol, 0)
    file.close()
    return sol


# Simulation specifications
physicalParams = PhysicalParameters(**physical_params)
outputParams   = OutputParameters(**output_params)
N, length = 64, 1e-2

# Problem loads
f     = lambda t: Constant((0, 0))
theta = lambda t: Constant(0)
tf    = lambda t: Constant((0, 0))
ts    = lambda t: Constant((0, 0))
externalLoads = ExternalForces(ff=f, fs=f, theta=theta, tf=tf, ts=ts)

# Geometry
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)
Markers = Markers(markers, [LEFT, BOTTOM], [NONE], [TOP, BOTTOM, RIGHT], none=NONE)


# Part 1, constant porosity

def solveConstantPhi(uf_deg, ys_deg, Solver):
    physicalParams = PhysicalParameters(**physical_params)
    physicalParams.uf_degree = uf_deg
    physicalParams.ys_degree = ys_deg
    outputParams   = OutputParameters(**output_params)
    outputParams.name += "burtschell_case_{}{}".format(uf_deg, ys_deg)
    Solver.setup(mesh, physicalParams, externalLoads, Markers, outputParams, MonolithicSolver)
    Solver.setBC(bc)
    Solver.solve()

# Boundary conditions
uf_pr_max = 1e-2
uf_pr = Expression(("4/l/l*uf_pr_max*x[1]*(l-x[1])","0."), l=length, uf_pr_max=uf_pr_max, degree=2)
bc = lambda t: [DirichletBC(Solver.Vsolid().sub(0), 0, markers, LEFT),
                DirichletBC(Solver.Vsolid().sub(1), 0, markers, BOTTOM),
                DirichletBC(Solver.Vfluid(), uf_pr, markers, LEFT)]

Solver = LB.LinearBurtschell(name)
Solver.setup(mesh, physicalParams, externalLoads, Markers, outputParams, MonolithicSolver)
Solver.setBC(bc)

solveConstantPhi(1, 1, Solver)
solveConstantPhi(1, 2, Solver)
solveConstantPhi(2, 1, Solver)
solveConstantPhi(2, 2, Solver)


# Part 2, spatially dependent porosity

phi = getSmoothPhi(mesh)

physical_params["lmbda"] = 0.035
physical_params["mu_s"]  = 0.035
physical_params["D"]     = 1e4
uf_pr = Expression(("4/l/l*uf_pr_max*x[1]*(l-x[1])","0."), l=length, uf_pr_max=0.05, degree=2)
bc = lambda t: [DirichletBC(Solver.Vsolid().sub(0), 0, markers, LEFT),
                DirichletBC(Solver.Vsolid().sub(1), 0, markers, BOTTOM),
                DirichletBC(Solver.Vfluid(), uf_pr, markers, LEFT)]
def solveSpatialPhi(uf_deg, ys_deg, Solver):
    physicalParams = PhysicalParameters(**physical_params)
    physicalParams.phi = lambda t: phi
    physicalParams.uf_degree = uf_deg
    physicalParams.ys_degree = ys_deg
    outputParams   = OutputParameters(**output_params)
    outputParams.name += "spatial_phi_{}{}".format(uf_deg, ys_deg)
    Solver.setup(mesh, physicalParams, externalLoads, Markers, outputParams, MonolithicSolver)
    Solver.setBC(bc)
    Solver.solve()

solveSpatialPhi(2, 2, Solver)
solveSpatialPhi(2, 1, Solver)
solveSpatialPhi(1, 2, Solver)
solveSpatialPhi(1, 1, Solver)
