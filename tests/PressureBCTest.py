"""
Script for testing new abstract structure
"""

import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.CooksonMonolithic import CooksonMonolithic
from ferosity.MeshCreation import generateSquare
from ferosity.PressureBC import PressureBC

set_log_active(False)
parameters['form_compiler']['optimize'] = True
parameters['form_compiler']["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6


name = "abstract_cookson_pressure_bc"


k1 = 2e3
k2 = 33
kp = 1e3
ks = 1e5

n_comps = 3

beta = np.ones([n_comps, n_comps])-np.identity(n_comps)

params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 2e3,
          "rhos":2e3,
	  "muf": 4e-3,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
	  "compartments interaction": beta,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 2,
          "dt": 1e-2,
          "export solutions": True,
          "save every": 1,
          "pressure model": "mono",
          "pressure bc use": True,
          "pressure bc tol": 1e-6,
          "pressure bc maxiter": 1000,
          "convergence_flag": False,
          "pressure bc log": False}

# Problem definition
N = 10
length = 1e-2

mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)


def evalF(F): return skel_energy(F) + vol_skel(det(F))

ds = Measure('ds', domain=mesh, subdomain_data=markers)
dsNeumann=ds(2)+ds(3)
cookson = CooksonMonolithic(mesh, name, params, evalF, vol_energy, dsNeumann)


def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                    DirichletBC(cookson.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

def f_sur_solid(t): return Constant((0, 0))
def f_vol_mass(t): return [Constant(0*t)] * n_comps

def pressure_func(t): return Constant(1e3 *
                                      params["initial porosities"][0] * (1 - exp(-t**2 / 0.25)))


pressure_bc = [PressureBC(markers, LEFT, pressure_func, 0)]

cookson.set_bcs(bcs, pressure_bc)
cookson.setup_loads(None, f_sur_solid, f_vol_mass, None)
cookson.solve()
