"""
@author: Giulia Ferla, Micol Bassanini
"""

import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import generateSquare
import matplotlib.pyplot as plt
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry

name = "cookson_test_fixedpoint_forces"

set_log_active(True)
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6

k1 = 2e3
k2 = 33
kp = 1e3  # 2e4
ks = 1e3  # Constant for pressure
semi_implicit_flag = False
geometry = "heart"  # square|heart
method ="fixed point"  # monolithic|staggered|fixed point

save_every = 1

n_comps = 3
beta = 1e-3 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))

params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 1.0,
          "dt": 1e-2,
          "export solutions": True,
          "geometry": geometry,
          "save every": save_every,
          "method": method,
          "scheme absolute tol": 1e-10,
          "scheme relative tol": 1e-8,
          "maxiter": 1000,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": False,
          "compare iterations": False,
          "flag semi implicit": semi_implicit_flag}


# Problem definition
if geometry == "square":
    N = 8
    length = 1e-2

    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

    skel_energy = CiarletGeymonat(k1, k2)
    vol_skel = LinpLog(kp)
    vol_energy = LinpLog(ks)

    def evalF(F): return skel_energy(F) + vol_skel(det(F))

    def f_sur_solid(t): return Constant((0, 0))
    def f_vol_solid(t): return Constant((0, 0))
    def f_vol_mass(t):
        source = Expression(
            "(x[0]<0.5 * L)? 0.5 : 0", L=length, degree=6)
        return [source] + [Constant(0.0)] * (n_comps - 1)

    NeumannMarkers = [TOP, RIGHT]
    RobinMarkers = []
    cookson = Cookson(mesh, name, params, evalF, vol_energy,
                      markers, NeumannMarkers, RobinMarkers)

    # Define problem handlers according to formulation
    if method == "monolithic":
        def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                            DirichletBC(cookson.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]
        cookson.set_bcs(bcs)

    elif method in ("staggered", "fixed point"):
        def bcs_y(t): return [DirichletBC(cookson.Vs.sub(0), Constant(0), markers, LEFT),
                              DirichletBC(cookson.Vs.sub(
                                  1), Constant(0), markers, BOTTOM)]

        def bcs_m(t): return []
        cookson.set_bcs([bcs_y, bcs_m])

    cookson.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    cookson.solve()

else:
    from ferosity.Fibers import generateFibers
    geom = "prolate_h4_v2_ASCII"

    mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
    NeumannMarkers = [ENDOCARD, BASE]
    RobinMarkers = [EPICARD]
    #noSlipMarkers  = [ENDOCARD, EPICARD]

   # Usyk,. mc Culloch 2002
    Cg = .88e3   # [Pa]
    bf = 8       # [-]
    bs = 6       # [-]
    bn = 3       # [-]
    bfs = 12      # [-]
    bfn = 3       # [-]
    bsn = 3       # [-]
    k = 5e4
    f0_, s0_, n0_ = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
    def ufl_norm(_x): return sqrt(dot(_x, _x))
    def normalize(_x): return _x / ufl_norm(_x)
    f0 = normalize(f0_)
    s0 = normalize(s0_)
    n0 = normalize(n0_)

    def W_mechs(F):
        E = 0.5 * (F.T * F - Identity(3))
        J = det(F)

        # Fibers

        Eff, Efs, Efn = inner(E * f0, f0), inner(E * f0, s0), inner(E * f0, n0)
        Esf, Ess, Esn = inner(E * s0, f0), inner(E * s0, s0), inner(E * s0, n0)
        Enf, Ens, Enn = inner(E * n0, f0), inner(E * n0, s0), inner(E * n0, n0)

        Q = Constant(bf) * Eff**2 \
            + Constant(bs) * Ess**2 \
            + Constant(bn) * Enn**2 \
            + Constant(bfs) * 2.0 * Efs**2 \
            + Constant(bfn) * 2.0 * Efn**2 \
            + Constant(bsn) * 2.0 * Esn**2
        WP = 0.5 * Constant(Cg) * (exp(Q) - 1)
        WV = Constant(k) / 2 * (J - 1) * ln(J)
        return WP + WV
    vol_energy = LinpLog(ks)

    def f(t): return Constant((0, 0, 0))
    def tf(t):return  Constant((0, 0, 0))
    def ts(t): 
        source = Expression(
           "(x[0]<0.5 * L)? 0.5 : 0", L=1, degree=6)
        return [source] + [Constant(0.0)] * (n_comps - 1)

    cookson = Cookson(mesh, name, params, W_mechs, vol_energy,
                      markers, NeumannMarkers, RobinMarkers)
    cookson.setFibers(f0, s0, n0)

    # Define problem handlers according to formulation
    if method == "monolithic":
        def bcs(t): return []
        cookson.set_bcs(bcs)

    elif method in ("staggered", "fixed point"):
        def bcs_y(t): return []
        def bcs_m(t): return []
        cookson.set_bcs([bcs_y, bcs_m])
    cookson.setup_loads(f, tf, ts, None)
    cookson.solve()
