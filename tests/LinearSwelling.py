#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 13:49:33 2018

@author: barnafi
"""

from ferosity.GLOBAL_VARIABLES import *  # Always required
import ferosity.LinearBurtschell as LB
from ferosity.Markers import Markers
import ferosity.Libs as Libs
from parameters.LinearSwelling import physical_params, output_params
from ferosity.Parameters import PhysicalParameters, ExternalForces, OutputParameters
from ferosity.Solvers.LinearizedMonolithicSolver import LinearizedMonolithicSolver as MonolithicSolver
from ferosity.MeshCreation import generateSquare

name = "linear_swelling"

##################
# Code beggining #
##################


# Simulation specifications

physicalParams = PhysicalParameters(**physical_params)
outputParams   = OutputParameters(**output_params)

# Problem loads
f     = lambda t: Constant((0, 0))
theta = lambda t: Constant(0)
tf    = lambda t: Constant(-1e3*(1 - exp(-t**2/0.25)))*FacetNormal(mesh)
ts    = lambda t: Constant(-1e3*(1 - exp(-t**2/0.25)))*FacetNormal(mesh)

externalLoads = ExternalForces(ff=f, fs=f, theta=theta, tf=tf, ts=ts)

# Mesh definition
length = 1e-2  # Domain side-length
N = 12

mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

Markers = Markers(markers, [LEFT], [LEFT], [TOP, BOTTOM], none=NONE)


Solver = LB.LinearBurtschell(name)
Solver.setup(mesh, physicalParams, externalLoads, Markers, outputParams, MonolithicSolver)


bc = lambda t: [DirichletBC(Solver.Vsolid().sub(0), 0, markers, LEFT),
    DirichletBC(Solver.Vsolid().sub(1), 0, markers, BOTTOM)]
Solver.setBC(bc)

Solver.solve()
