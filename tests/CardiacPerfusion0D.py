from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import Guccione
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog, LinxLog
from ferosity.Fibers import generateFibers
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import prolateGeometry
from ferosity.CSVInterpolation import CSVInterpolation
import numpy as np

name = "cardiac-0D"

set_log_active(False)
set_log_level(20)
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 3

kp = 1e4  # Pressure bulk
ks = 5e4  # Solid bulk
method ="fixed point"  # monolithic|staggered|fixed point
pressure_csv = "aortic_pressure_profile.csv"

n_comps = 1
beta = 1e-5 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))

params = {"permeabilities": [1e-8] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.1] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 3e-3,
          "dt": 7e-4,
          "save every": 10,
          "export solutions": False,
          "geometry": "heart",
          "method": method,
          "scheme absolute tol": 1e-11,
          "scheme relative tol": 1e-7,
          "maxiter": 100,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": False,
          "compare iterations": True,
          "flag semi implicit":False }

assert params["geometry"] == "heart", "Geometry must be heart."


# Load mesh
geom = "prolate_h4_v2_ASCII"

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
NeumannMarkers = []
RobinMarkers = [BASE, EPICARD, ENDOCARD]

# Generate potentials
f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
guccione = Guccione(f0, s0, n0)

vol_sol = LinxLog(ks)
vol_energy = LinpLog(kp)


def energy_solid(F):
    return guccione(F) + vol_sol(det(F))


def f(t): return Constant((0, 0, 0))
def tf(t): return Constant((0, 0, 0))


# Build source term according to CSV file.
period = 0.8
csv = CSVInterpolation(pressure_csv, period)

volume = assemble(1 * dx(mesh))


class Source:
    def __init__(self, cookson):
        self.cookson = cookson

    def __call__(self, t):

        out = n_comps * [0.0]

        p_correction = 6000  # Used because library doesn't support yet reference pressures
        p_in = 133.32 * csv(np.fmod(t, period)) - p_correction # mmHg to Pa
        p_out = self.cookson.getAveragePressure(
            0)  # Get average from first compartment
        delta_p = max(p_in - p_out, 0)  # Filter out backflow

        # Taken from zygote coronaries summing inlets and outlets independently
        Sout = 5.68788e-6
        Sin = 11.3918e-6
        factor = 2.0 / params["rhof"] / (1.0 / Sout / Sout - 1.0 / Sin / Sin)
        out[-1] = sqrt(factor * delta_p) / volume
        return Constant(out)


cookson = Cookson(mesh, name, params, energy_solid, vol_energy,
                  markers, NeumannMarkers, RobinMarkers)
cookson.setFibers(f0, s0, n0)

# Define problem handlers according to formulation
if method == "monolithic":
    def bcs(t): return []
    cookson.set_bcs(bcs)
else:
    def bcs_y(t): return []
    def bcs_m(t): return []
    cookson.set_bcs([bcs_y, bcs_m])

ts = Source(cookson)
cookson.setup_loads(f, tf, ts, None)

# Initial conditions
sol0y = Expression(["0", "0", "0"], degree = 1)
sol0m = Expression(["10"], degree = 1)
cookson.initialize_solutions(sol0y, sol0m)
cookson.solve()

if method =="monolithic":
    iter_cookson = cookson.newton_iter
    print(iter_cookson)
else:
    iter_cookson = cookson.iter
    print(iter_cookson)
    iter_cookson = cookson.newton_y
    print(iter_cookson)
    iter_cookson = cookson.newton_mass
    print(iter_cookson)
    
