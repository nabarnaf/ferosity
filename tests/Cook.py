from dolfin import *
from mpi4py import MPI
import numpy as np
from petsc4py import PETSc
import sys
from time import perf_counter as time
time_init = MPI.Wtime()
assert len(sys.argv) == 5, "Must give argument for number of refinements, FE degree,  method (Newton|BFGS) and load force"

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 4
#parameters["mesh_partitioner"] = "ParMETIS"
method = sys.argv[3]  # Newton|BFGS
load_in = float(sys.argv[4])
# Solver params
atol, rtol, maxit, alpha, verbose = 1e-8, 1e-6, 100, 1.0, True
# Set options for KSP
if method == "Newton":
    snes_type = "newtonls"
    ksp_type = "minres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-6
elif method == "Newton-inexact":
    snes_type = "newtonls"
    ksp_type = "minres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-2
    PETSc.Options().setValue("-snes_ksp_ew", None)
    PETSc.Options().setValue("-snes_ksp_ew_rtol0", 1e-1)
    PETSc.Options().setValue("-snes_ksp_ew_rtolmax", 0.1)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS-inexact":
    snes_type = "qn"
    ksp_type = "minres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-2
    PETSc.Options().setValue("-ksp_converged_reason", None)
    PETSc.Options().setValue("-snes_lag_jacobian", 1000)
    PETSc.Options().setValue("-snes_lag_preconditioner", 1000)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS":
    snes_type = "qn"
    ksp_type = "preonly"
    ksp_atol = 0.0
    ksp_rtol = 0.0
    PETSc.Options().setValue("-snes_lag_jacobian", 1000)
    PETSc.Options().setValue("-snes_lag_preconditioner", 1000)
else:
    assert False, "method must be one of Newton|Newton-inexact|BFGS-inexact|BFGS"

# Set options for KSP
PETSc.Log().begin() # Used for log_view
PETSc.Options().setValue("-ksp_type", ksp_type)
PETSc.Options().setValue("-ksp_atol", ksp_atol)
PETSc.Options().setValue("-ksp_rtol", ksp_rtol)
PETSc.Options().setValue("-pc_type", "hypre")
# PETSc.Options().setValue("-ksp_norm_type", "unpreconditioned")
PETSc.Options().setValue("-ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-ksp_gmres_restart", 1000)
PETSc.Options().setValue("-snes_converged_reason", None)
PETSc.Options().setValue("-snes_monitor", None)
#PETSc.Options().setValue("-log_view", None)
#PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-snes_atol", 1e-10)
PETSc.Options().setValue("-snes_rtol", 1e-6)
PETSc.Options().setValue("-snes_stol", 0.0)
PETSc.Options().setValue("-snes_type", snes_type)
#PETSc.Options().setValue("-snes_view", None)
PETSc.Options().setValue("-snes_qn_type", "lbfgs")
PETSc.Options().setValue("-snes_qn_m", 20)
PETSc.Options().setValue("-snes_qn_scale_type", "jacobian")
PETSc.Options().setValue("-snes_qn_restart_type", "none")
PETSc.Options().setValue("-snes_linesearch_type", "basic")


anderson_depth = 0  # Used with BFGS

# Model params
N = int(sys.argv[1])
u_deg = int(sys.argv[2])
nx = N * 6
ny = N * 6
nz = N * 1
lx = 48e-3  # mm
ly = 44e-3  # mm
lz = 10e-3  # mm
w = 16e-3  # mm
tau = load_in
mesh = BoxMesh(Point(0, 0, 0), Point(lx, ly, lz), nx, ny, nz)

# Convert box into Cook membrane
for i, coord in enumerate(mesh.coordinates()):
    x, y, z = coord
    mesh.coordinates()[i, 1] = (1 - (ly - w)/(ly*lx)*x)*y + ly * x / lx

V = VectorFunctionSpace(mesh, "CG", u_deg)
if MPI.COMM_WORLD.rank == 0:
    print("Problem dofs = {}, dofs/core = {}".format(V.dim(), V.dim() / MPI.COMM_WORLD.size), flush=True)
#    assert V.dim() / MPI.COMM_WORLD.size <= 500000, "We allow up to 500k dofs per core, sorry!"

# Mark boundary subdomians
c = Constant((0.0, 0.0, 0.0))

boundary_markers = MeshFunction("size_t", mesh, 2)
boundary_markers.set_all(0)


class Right(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], lx)


class Left(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)


bx0 = Right()
bx1 = Left()
RIGHT = 1
LEFT = 2
bx0.mark(boundary_markers, RIGHT)
bx1.mark(boundary_markers, LEFT)
bcl = DirichletBC(V, c, boundary_markers, LEFT)
bcs = [bcl]
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# FILE = File('images/cook.pvd')
# Problem constants
ap = 126  # 126kPa
bp = 252  # 252kPa
lp = 81512  # 81512kPa
# Neo-hookean params
#C1 = 0.4
#D1 = 2.5e-4

mu = Constant(80.194e6)
nu = Constant(0.3)
lmbd = 2*mu*nu / (1 - 2 * nu)
kk = lmbd + 0.666666 * mu
C1 = mu / 2

# Find coefficients of parabolic shear stress
tA = np.array([[ly**2, ly, 1], [(ly+w)**2, ly+w, 1], [(ly + 0.5 * w)**2, ly + 0.5 * w, 1]])
tb = np.array([0, 0, tau])
coeffs = np.linalg.solve(tA, tb)

# Define functions
du = TrialFunction(V)            # Incremental displacement
v = TestFunction(V)             # Test function
u = Function(V)                 # Displacement
u.rename("d", "d")
T = Expression(("0.0",  "tt", "0.0"), degree=1, tt=tau)  # Traction force on the boundary


# Kinematics
I = Identity(3)    # Identity tensor
F = I + grad(u)             # Deformation gradient
C, J = F.T*F, det(F)
H = J * inv(F).T

# Problem definition
psi = C1 * (J**(-2/3) * tr(C) - 3) + kk * (J**2 - 1 - 2 * ln(J))
Pi = psi*dx - dot(H * T, u)*ds(RIGHT)
Res = derivative(Pi, u, v)
Jac = derivative(Res, u, du)


class SNESProblem():
    def __init__(self, FF, uu, bbcs):
        V = uu.function_space()
        ddu = TrialFunction(V)
        self.L = FF
        self.a = derivative(FF, uu, ddu)
        self.bcs = bbcs
        self.bcs0 = [DirichletBC(_bc) for _bc in bbcs]
        for bc in self.bcs0:
            bc.homogenize()
        self.u = uu

    def F(self, snes, xx, FF):
        xx = PETScVector(xx)
        FF = PETScVector(FF)
        xx.vec().copy(self.u.vector().vec())
        self.u.vector().apply("")
        assemble(self.L, tensor=FF)
        for bc in self.bcs0:
            bc.apply(FF)

    def J(self, snes, xx, JJ, PP):
        JJ = PETScMatrix(JJ)
        JJ.mat().setBlockSize(3)
        xx.copy(self.u.vector().vec())
        self.u.vector().apply("")
        assemble(self.a, tensor=JJ)
        for bc in self.bcs0:
            bc.apply(JJ)


for bc in bcs:
    bc.apply(u.vector())
problem = SNESProblem(Res, u, bcs)

b = PETScVector()  # same as b = PETSc.Vec()
J_mat = PETScMatrix()
assemble(Res, tensor=b)
assemble(Jac, tensor=J_mat)
for bc in bcs:
    bc.apply(J_mat, b)

snes = PETSc.SNES().create(MPI.COMM_WORLD)
snes.setFunction(problem.F, b.vec())
snes.setJacobian(problem.J, J_mat.mat())
snes.setFromOptions()
snes.ksp.setFromOptions()
snes.solve(None, problem.u.vector().vec())
time_end = MPI.Wtime() - time_init

# Output
l_its = snes.getLinearSolveIterations()
nl_its = snes.getIterationNumber()
converged = snes.getConvergedReason() > 0
if MPI.COMM_WORLD.rank == 0:
    print("Dofs={}".format(V.dim()), flush=True)
    print("Converged={}".format(converged), flush=True)
    print("Nonlinear iterations={}".format(nl_its), flush=True)
    print("Total linear iterations={}".format(l_its), flush=True)
    print("Average linear iterations={:.2f}".format(l_its/nl_its), flush=True)
    print("Wall time={:.2f}".format(time_end), flush=True)
