E     = 1e4
nu    = 0.2

physical_params = { "sim_time": 1e-2,
                      "dim": 2,
                      "dt": 1e-6,
                      "t0": 0,
                      "ys_degree": 2,
                      "uf_degree": 2,
                      "us_degree": 1,
                      "p_degree": 1,
                      "active_stress": False,
                      "rhos": 1,
                      "rhof": 1,
                      "phi": lambda t: 0.1,
                      "mu_f": 10,
                      "ks": 1,
                      "lmbda": 10, # E*nu/((1.+nu)*(1.-2.*nu)),
                      "mu_s": 10, # E/(2.*(1.+nu)),
                      "gamma": 1, # 20,
                      "D": 1,
                      "approach": "Default",
                      "AS": 1e6, "beta": 10} # AS is active stress constant

output_params = {"store_solutions": True,
                  "export_solutions": False,
                  "save_every": 10,
                  "name": "result_convergence"}