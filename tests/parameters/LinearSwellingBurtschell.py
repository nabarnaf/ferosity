k = k1 = 2e3
k2 = 33

physical_params = { "sim_time": 1,
                      "dim": 2,
                      "dt": 1e-2,
                      "t0": 0,
                      "ys_degree": 2,
                      "uf_degree": 2,
                      "us_degree": 1,
                      "p_degree": 1,
                      "active_stress": False,
                      "rhos": 1e3,
                      "rhof": 1e3,
                      "phi":  lambda t: 0.1,
                      "mu_f": 0.035,
                      "ks": 2e8,
                      "lmbda": abs(k - 4*(k1+k2)/3),
                      "mu_s": 2*(k1+k2),
                      "gamma": 2e5,
                      "D": 1e7,
                      "AS": 1e6, "beta": 10} # AS is active stress constant

output_params = {"store_solutions": False,
                  "export_solutions": True,
                  "save_every": 1,
                  "name": "result_swelling"}
