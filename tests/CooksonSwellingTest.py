"""
@author: Giulia Ferla, Micol Bassanini
"""
import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import generateSquare
import matplotlib.pyplot as plt

name = "cookson-swelling"

set_log_active(False)
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6

k1 = 2e3
k2 = 33
kp = 1e3  # 2e4
ks = 1e3  # Constant for pressure
semi_implicit_flag = False
scheme = "fixed point"  # monolithic|staggered|fixed point

save_every = 1

n_comps = 2
beta = 1e-3 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))

params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 1.0,
          "dt": 1e-2,
          "export solutions": True,
          "save every": save_every,
          "method": scheme,
          "geometry": "square",
          "scheme absolute tol": 1e-10,
          "scheme relative tol": 1e-8,
          "maxiter": 1000,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": False,
          "compare iterations": False,
          "flag semi implicit": semi_implicit_flag}


# Problem definition
N = 8
length = 1e-2

mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)


def evalF(F): return skel_energy(F) + vol_skel(det(F))

NeumannMarkers = [TOP, RIGHT]
RobinMarkers = []

def f_sur_solid(t): return Constant((0, 0))
def f_vol_solid(t): return Constant((0, 0))

def f_vol_mass(t):
  source = Expression(
      "(x[0]<0.5 * L)? 0.5 : 0", L=length, degree=6)
  return [source] + [Constant(0.0)] * (n_comps - 1)

cookson = Cookson(mesh, name, params, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)

# Define problem handlers according to formulation
if scheme == "monolithic":
  def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                      DirichletBC(cookson.V.sub(0).sub(
                          1), Constant(0), markers, BOTTOM)]
  cookson.set_bcs(bcs)

elif scheme in ("staggered", "fixed point"):
  def bcs_y(t): return [DirichletBC(cookson.Vs.sub(0), Constant(0), markers, LEFT),
                        DirichletBC(cookson.Vs.sub(
                            1), Constant(0), markers, BOTTOM)]
  def bcs_m(t): return []
  cookson.set_bcs([bcs_y, bcs_m])

cookson.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
cookson.solve()
