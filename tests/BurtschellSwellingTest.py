#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 13:49:33 2018

@author: barnafi
"""

import lib.BurtschellSolver as BS
import lib.Libs as Libs
from dolfin import *

# Dolfin parameters
set_log_level(40)
#parameters["form_compiler"]["quadrature_degree"] = 10
parameters["form_compiler"]["cpp_optimize"] = True
name = "swelling"

##################
# Code beggining #
##################


# Simulation specifications

sim_time   = 1.5
dt         = Constant(1e-2)
t0         = 0.
Niter      = int(sim_time/dt(0))
save_every = 1

# Problem parameters
f = lambda t: Constant((0, 0))
theta = lambda t: 0.



# Mesh definition
length = 1e-2  # Domain side-length
N = 6
mesh = UnitSquareMesh(N, N)
# Rescale for Chapelle-Moireau comparison
mesh.coordinates()[:] *= length
h = Constant(mesh.hmax())

# Subdomains: Solid
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0) and on_boundary
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], length) and on_boundary
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], length) and on_boundary
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0) and on_boundary
left, right, top, bottom = Left(), Right(), Top(), Bottom()
LEFT, RIGHT, TOP, BOTTOM = 1, 2, 3, 4  # Set numbering
NONE = 99

markers = MeshFunction("size_t", mesh, 1)
markers.set_all(0)


boundaries = (left, right, top, bottom)
def_names = (LEFT, RIGHT, TOP, BOTTOM)
for side, num in zip(boundaries, def_names):
    side.mark(markers, num)


# Measures definition
dx = Measure('dx', domain=mesh)
ds = Measure('ds', domain=mesh, subdomain_data=markers)

Solver = BS.BurtschellSolver(mesh, f, theta, markers, name)
Solver.setParameters(dt=dt, ks=2e3, kp = 2e3)  # Use default values

# Set Neumann measures
Solver.setMeasures(dx, ds, dsN=ds(LEFT), dsNnos=ds(TOP)+ds(BOTTOM), dsNnof=ds(NONE))

# Set Neumann conditions
tNeumann = lambda t: Constant(-1e3*(1 - exp(-t**2/0.25)))*FacetNormal(mesh)
tNoSlid  = tNoFlux = lambda t: 0.*FacetNormal(mesh)
Solver.setNeumannBoundaryConditions(tNeumann, tNoSlid, tNoFlux)
Solver.setWeakMarkers([TOP, BOTTOM], [], [])

# Set Dirichlet BC

bc1 = DirichletBC(Solver.Vstep2b.sub(0), 0., markers, LEFT)
bc2 = DirichletBC(Solver.Vstep2b.sub(1), 0., markers, BOTTOM)
bcS = lambda t: [bc1, bc2]
Solver.setSolidBC(bcS)

bcF = lambda t: []
Solver.setFluidBC(bcF)  # No fluid BC on swelling test

#setInitialConditions(us0, uf0) # Zero by default, phi0=0.1

Solver.solve(t0, Niter, save_every)