"""
@author: Giulia Ferla, Micol Bassanini
"""

from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import generateSquare
import numpy as np
import matplotlib.pyplot as plt

set_log_active(False)

plt.rc('font', family='serif')
plt.rc('text', usetex=True)
plt.rcParams.update(
    {'font.size': 18, 'axes.labelweight': 'bold', 'lines.linewidth': 4})

name = "convergence"
name2 = "convergence-solexact"

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6

convergence_type = "time"  # "time"|"space"
degree_solid = 2
degree_mass = 1

k1 = 2e3
k2 = 33
kp = 2e4
ks = 2e2                   # Constant for pressure
method = "monolithic"     # "monolithic"|"fixed point"|"staggered"
semi_implicit_flag = True

save_every = 1

n_comps = 3
beta = 1e-3 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))


skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)

if convergence_type == "space":
    N = [8, 16, 32]
    times = [1e-5]
else:
    N = [128]
    times = np.array([1e-3, 0.5e-3, 1e-4])

length = 1
h = np.zeros(len(N))


def evalF(F):
    return skel_energy(F) + vol_skel(det(F))

# Define exact solutions:


def yex_func(t, x, y):
    return as_vector((Constant(5 * t**2) * sin(4 * pi * y) * sin(4 * pi * x),
                      Constant(-5 * t**2) * sin(4 * pi * y) * sin(4 * pi * x)))


def yex_dd_func(t, x, y):
    return as_vector((Constant(10) * sin(4 * pi * y) * sin(4 * pi * x),
                      Constant(-10) * sin(4 * pi * y) * sin(4 * pi * x)))


def mex_func(t, x, y): return as_vector(
    (Constant(2e4 * t**2) * sin(2 * pi * x) * (x - length) * sin(2 * pi * y) * (y - length),
        100 * Constant(t**2) * sin(2 * pi * x) * (x - length) *
     sin(2 * pi * y) * (y - length),
        Constant(100 * t**2) * sin(x) * (x - length) * sin(y) * (y - length)))


def mex_d_func(t, x, y):
    return as_vector((Constant(4e4 * t) * sin(2 * pi * x) * (x - length) * sin(2 * pi * y) * (y - length),
                      Constant(200 * t) * sin(2 * pi * x) *
                      (x - length) * sin(2 * pi * y) * (y - length),
                      Constant(200 * t) * sin(2 * pi * x) * (x - length) * sin(2 * pi * y) * (y - length)))



# mesh_fine, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(
# 2**7, length)
# el_s_fine = VectorElement("CG", mesh_fine.ufl_cell(), degree_solid)
# el_m_fine = VectorElement("CG", mesh_fine.ufl_cell(), degree_mass, n_comps)
# Vs_fine = FunctionSpace(mesh_fine, el_s_fine)
# Vm_fine = FunctionSpace(mesh_fine, el_m_fine)


if convergence_type == "space":
    err_inf_y_L2 = np.zeros(len(N))
    err_inf_y_H1 = np.zeros(len(N))
    err_inf_m_L2 = np.zeros((n_comps, len(N)))
    err_inf_m_H1 = np.zeros((n_comps, len(N)))
    err_inf_mtot_L2 = np.zeros(len(N))
    err_inf_mtot_H1 = np.zeros(len(N))
else:
    err_inf_y_L2 = np.zeros(len(times))
    err_inf_y_H1 = np.zeros(len(times))
    err_inf_mtot_L2 = np.zeros(len(times))
    err_inf_mtot_H1 = np.zeros(len(times))

ip = 0

while ip < len(N):

    print("Solving problem from N={}".format(N[ip]))

    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(
        N[ip], length)
    h[ip] = mesh.hmax()
    NeumannMarkers = [TOP, RIGHT]
    RobinMarkers = []

    ii = 0

    while ii < len(times):
        params = {"permeabilities": [1e-7] * n_comps,
                  "rhof": 1e3,
                  "rhos": 1e3,
                  "muf": 0.035,
                  "initial porosities": [0.035] * n_comps,
                  "number compartments": n_comps,
                  "compartments interaction": beta,
                  "gamma": 0,
                  "venous return": 0,
                  "degree solid": degree_solid,
                  "degree mass": degree_mass,
                  "t0": 0,
                  "tf": 1e-2,
                  "dt": times[ii],
                  "export solutions": False,
                  "geometry": "square",
                  "save every": save_every,
                  "method": method,
                  "scheme absolute tol": 1e-14,
                  "scheme relative tol": 1e-10,
                  "maxiter": 1000,
                  "pressure model": "mono",
                  "pressure bc use": False,
                  "convergence_flag": True,
                  "compare iterations": False,
                  "flag semi implicit": semi_implicit_flag}

        cookson = Cookson(mesh, name, params, evalF, vol_energy,
                          markers, NeumannMarkers, RobinMarkers)

        if method == "monolithic":
            def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                                DirichletBC(cookson.V.sub(0).sub(
                                    1), Constant(0), markers, LEFT),
                                DirichletBC(cookson.V.sub(0).sub(
                                    0), Constant(0), markers, BOTTOM),
                                DirichletBC(cookson.V.sub(0).sub(
                                    1), Constant(0), markers, BOTTOM),
                                DirichletBC(cookson.V.sub(1), Constant(n_comps * [0]), "on_boundary")]
            cookson.set_bcs(bcs)

        elif method in ("staggered", "fixed point"):
            def bcs_y(t): return [DirichletBC(cookson.Vs.sub(0), Constant(0), markers, LEFT),
                                  DirichletBC(cookson.Vs.sub(
                                      1), Constant(0), markers, LEFT),
                                  DirichletBC(cookson.Vs.sub(
                                      0), Constant(0), markers, BOTTOM),
                                  DirichletBC(cookson.Vs.sub(1), Constant(0), markers, BOTTOM)]

            def bcs_m(t): return [DirichletBC(
                cookson.Vm, Constant(n_comps * [0]), "on_boundary")]
            cookson.set_bcs([bcs_y, bcs_m])

        x, y = SpatialCoordinate(cookson.mesh)

        def load_solid_exact(t):
            yex = yex_func(t, x, y)
            mex = mex_func(t, x, y)
            F = grad(yex) + Identity(2)
            n = FacetNormal(cookson.mesh)
            return cookson.energy.getPiola(F, mex) * n

        def load_mass_exact(t):
            yex = yex_func(t, x, y)
            mex = mex_func(t, x, y)
            mex_d = mex_d_func(t, x, y)
            F = grad(yex) + Identity(2)
            J = det(F)
            p = cookson.energy.getPressure(F, mex)
            qq = [None] * cookson.N_compartments
            for comp in range(cookson.N_compartments):
                Ki = J * inv(F) * \
                    Constant(cookson.kf_vec[comp]) * inv(F.T)
                qw = mex_d[comp] + \
                    div(-Constant(cookson.rhof) * Ki * grad(p[comp]))
                for comp2 in range(cookson.N_compartments):
                    qw += J * (cookson.beta[comp][comp2]
                               * (p[comp] - p[comp2]))
                qq[comp] = qw / cookson.rhof
            return qq

        def load_sol_vol_exact(t):
            yex = yex_func(t, x, y)
            mex = mex_func(t, x, y)
            yex_dd = yex_dd_func(t, x, y)
            F = grad(yex) + Identity(2)
            return Constant(cookson.rhos * (cookson.phis)) * yex_dd - div(cookson.energy.getPiola(F, mex))

        cookson.setup_loads(load_sol_vol_exact,
                            load_solid_exact, load_mass_exact, None)
        cookson.solve()

        n_sols = len(cookson.y_sol)
        err_y_L2 = np.zeros(n_sols)
        err_y_H1 = np.zeros(n_sols)
        err_m_L2 = np.zeros((cookson.N_compartments, n_sols))
        err_m_H1 = np.zeros((cookson.N_compartments, n_sols))
        err_mtot_L2 = np.zeros(n_sols)
        err_mtot_H1 = np.zeros(n_sols)

        time = params["t0"] + params["dt"]
        it = 0

        def L2_error(x_, y_):
            return sqrt(assemble(dot(x_ - y_, x_ - y_) * dx(mesh)))

        def H1_error(x_, y_):
            return sqrt(assemble(dot(x_ - y_, x_ - y_)
                                 * dx(mesh) + inner(grad(x_ - y_), grad(x_ - y_)) * dx(mesh)))
        while time <= params["tf"]:

            if convergence_type == "space":
                # x_fine, y_fine = SpatialCoordinate(mesh_fine)
                x_fine, y_fine = SpatialCoordinate(mesh)

                mex = mex_func(time, x_fine, y_fine)
                # ex_m = project(mex, Vm_fine)
                yex = yex_func(time, x_fine, y_fine)
                # ex_y = project(yex, Vs_fine)

                # Error in added mass
                sol_m = cookson.m_sol[it]
                # err_mtot_L2[it] = errornorm(ex_m, sol_m, "L2")
                err_mtot_L2[it] = L2_error(mex, sol_m)
                # err_mtot_H1[it] = errornorm(ex_m, sol_m, "H1")
                err_mtot_H1[it] = H1_error(mex, sol_m)

                # Error in displacement
                sol_y = cookson.y_sol[it]
                # err_y_L2[it] = errornorm(ex_y, sol_y, "L2")
                err_y_L2[it] = L2_error(yex, sol_y)
                # err_y_H1[it] = errornorm(ex_y, sol_y, "H1")
                err_y_H1[it] = H1_error(yex, sol_y)

            else:
                x, y = SpatialCoordinate(cookson.mesh)
                mex = mex_func(time, x, y)
                # ex_m = project(mex, cookson.Vm)
                yex = yex_func(time, x, y)
                # ex_y = project(yex, cookson.Vs)

                # Error in added mass
                sol_m = cookson.m_sol[it]
                # err_mtot_L2[it] = errornorm(ex_m, sol_m, "L2")
                err_mtot_L2[it] = L2_error(mex, sol_m)
                # err_mtot_H1[it] = errornorm(ex_m, sol_m, "H1")
                err_mtot_H1[it] = H1_error(mex, sol_m)

                # Error in displacement
                sol_y = cookson.y_sol[it]
                # err_y_L2[it] = errornorm(ex_y, sol_y, "L2")
                err_y_L2[it] = L2_error(yex, sol_y)
                # err_y_H1[it] = errornorm(ex_y, sol_y, "H1")
                err_y_H1[it] = H1_error(yex, sol_y)

            time += params["dt"]
            it += 1

        if convergence_type == "time":
            err_inf_y_L2[ii] = err_y_L2.max()
            err_inf_y_H1[ii] = err_y_H1.max()
            err_inf_mtot_L2[ii] = err_mtot_L2.max()
            err_inf_mtot_H1[ii] = err_mtot_H1.max()

        ii += 1

    if convergence_type == "space":
        err_inf_y_L2[ip] = err_y_L2.max()
        err_inf_y_H1[ip] = err_y_H1.max()
        err_inf_mtot_L2[ip] = err_mtot_L2.max()
        err_inf_mtot_H1[ip] = err_mtot_H1.max()

    ip += 1

print("\nError displacement in norm L2", err_inf_y_L2)
print("\nError displacement in norm H1", err_inf_y_H1)
print("\nError added mass in norm L2", err_inf_mtot_L2)
print("\nError added mass in norm H1", err_inf_mtot_H1)

if convergence_type == "space":

    cmap = plt.get_cmap("viridis")

    plt.figure(1)
    plt.loglog(h, err_inf_y_H1, label=r'$L^\infty(H^1)$ error')
    plt.loglog(h, err_inf_y_H1[0] * h / h[0], label=r'$h$', linestyle='--')
    plt.loglog(h, err_inf_y_H1[0] * h**2 / h[0]**2, label=r'$h^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|y_s - y_{s, exact}\|_{L^\infty(H^1)}$')
    plt.xlabel(r'Mesh size $h$')
    plt.savefig('convergence_y_H1.png', bbox_inches='tight')
    plt.clf()

    plt.figure(2)
    plt.loglog(h, err_inf_y_L2, label=r'$L^\infty(L^2)$ error')
    plt.loglog(h, err_inf_y_L2[0] * h / h[0], label=r'$h$', linestyle='--')
    plt.loglog(h, err_inf_y_L2[0] * h**2 / h[0]**2, label=r'$h^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|y_s - y_{s, exact}\|_{L^\infty(L^2)}$')
    plt.xlabel(r'Mesh size $h$')
    plt.savefig('convergence_y_L2.png', bbox_inches = 'tight')
    plt.clf()

    plt.figure(3)
    plt.loglog(h, err_inf_mtot_H1, label=r'$L^\infty(H^1)$ error')
    plt.loglog(h, err_inf_mtot_H1[0] * h / h[0], label=r'$h$', linestyle='--')
    plt.loglog(h, err_inf_mtot_H1[0] * h**2 / h[0]**2, label=r'$h^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|m - m_{exact}\|_{L^\infty(H^1)}$')
    plt.xlabel(r'Mesh size $h$')
    plt.savefig('convergence_mtot_H1.png', bbox_inches='tight')
    plt.clf()

    plt.figure(4)
    plt.loglog(h, err_inf_mtot_L2, label=r'$L^\infty(L^2)$ error')
    plt.loglog(h, err_inf_mtot_L2[0] * h / h[0], label=r'$h$', linestyle='--')
    plt.loglog(h, err_inf_mtot_L2[0] * h**2 / h[0]**2, label=r'$h^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|m - m_{exact}\|_{L^\infty(L^2)}$')
    plt.xlabel(r'Mesh size $h$')
    plt.savefig('convergence_mtot_L2.png', bbox_inches='tight')
    plt.clf()


if convergence_type == "time":

    cmap = plt.get_cmap("viridis")

    plt.figure(1)
    plt.loglog(times, err_inf_y_H1, label=r'$L^\infty(H^1)$ error')
    plt.loglog(times, err_inf_y_H1[0] * times / times[0], label=r'$\Delta t$', linestyle='--')
    plt.loglog(times, err_inf_y_H1[0] * times**2 / times[0]**2, label=r'$\Delta t^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|y_s - y_{s, exact}\|_{L^\infty(H^1)}$')
    plt.xlabel(r'Time step $\Delta t$')
    plt.savefig('convergence_y_H1.png', bbox_inches='tight')
    plt.clf()

    plt.figure(2)
    plt.loglog(times, err_inf_y_L2, label=r'$L^\infty(L^2)$ error')
    plt.loglog(times, err_inf_y_L2[0] * times / times[0], label=r'$\Delta t$', linestyle='--')
    plt.loglog(times, err_inf_y_L2[0] * times**2 / times[0]**2, label=r'$\Delta t^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|y_s - y_{s, exact}\|_{L^\infty(L^2)}$')
    plt.xlabel(r'Time step $\Delta t$')
    plt.savefig('convergence_y_L2.png', bbox_inches = 'tight')
    plt.clf()

    plt.figure(3)
    plt.loglog(times, err_inf_mtot_H1, label=r'$L^\infty(H^1)$ error')
    plt.loglog(times, err_inf_mtot_H1[0] * times / times[0], label=r'$\Delta t$', linestyle='--')
    plt.loglog(times, err_inf_mtot_H1[0] * times**2 / times[0]**2, label=r'$\Delta t^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|m - m_{exact}\|_{L^\infty(H^1)}$')
    plt.xlabel(r'Time step $\Delta t$')
    plt.savefig('convergence_mtot_H1.png', bbox_inches='tight')
    plt.clf()

    plt.figure(4)
    plt.loglog(times, err_inf_mtot_L2, label=r'$L^\infty(L^2)$ error')
    plt.loglog(times, err_inf_mtot_L2[0] * times / times[0], label=r'$\Delta t$', linestyle='--')
    plt.loglog(times, err_inf_mtot_L2[0] * times**2 / times[0]**2, label=r'$\Delta t^2$', linestyle='--')
    plt.legend(loc='best')
    plt.ylabel(r'$\|m - m_{exact}\|_{L^\infty(L^2)}$')
    plt.xlabel(r'Time step $\Delta t$')
    plt.savefig('convergence_mtot_L2.png', bbox_inches='tight')
    plt.clf()
