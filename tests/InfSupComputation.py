#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 17:35:55 2019

@author: barnafi
"""

from dolfin import *
import numpy as np
from scipy.linalg import eigvals, inv

name = "infsup_computation"

N       = 20
mesh    = UnitSquareMesh(N, N)
phi_min = 1e-8
phi_max = 1 - phi_min
Nfirst  = 100  # 100 gves good results
Nphis   = 100  # 100 gives good results
phis_first_part = np.linspace(phi_min, 0.1, Nfirst)
phis_part2 = np.linspace(0.1, phi_max, Nphis)
phis = np.unique(np.hstack((phis_first_part, phis_part2)))
#phis = np.linspace(phi_min, phi_max, Nphis)

mu_f  = Constant(0.035)
E     = 1e4
nu    = 0.2
lmbda_ref = E*nu/((1+nu)*(1-2*nu))
mu_s_ref  = E/(2*(1+nu))


solid_energies = ["l", "h", "hp"]
fluid_energies = ["l", "v", "vp"]


save_plot = True
def getInfsup(mesh, phi, u_deg, y_deg, p_deg, fluid_energy, solid_energy):
    """
    Get inf-sup constant corresponding to a given mesh, phi, velocity degree and
    pressure degree
    """
    phi = Constant(phi)  # To avoid using JIT on each iteration

    # Define function spaces
    u_el = VectorElement("CG", triangle, u_deg)
    y_el = VectorElement("CG", triangle, y_deg)
    p_el = FiniteElement("CG", triangle, p_deg)
    V    = FunctionSpace(mesh, MixedElement(u_el, y_el, p_el))

    # No-slip boundary condition for velocity
    # x1 = 0, x1 = 1 and around the dolphin
    zero = Constant((0, 0))
    bcs = [DirichletBC(V.sub(0), zero, "on_boundary && near(x[1], 0)"),
           DirichletBC(V.sub(1), zero, "on_boundary && near(x[0], 0)")]


    # Define variational problem
    (u, y, p) = TrialFunctions(V)
    (v, w, q) = TestFunctions(V)
    f = Constant((0, 0))

    mu_f = 0.035 if fluid_energy=="v" else 1
    mu_f = Constant(mu_f)
    lmbda = lmbda_ref if solid_energy=="h" else 1
    lmbda = Constant(lmbda)
    mu_s  = mu_s_ref if solid_energy=="h" else 1
    mu_s  = Constant(mu_s)


    def energy(_u, _v, energy_type):
        eps     = lambda v: sym(grad(v))
        sig_vis = lambda v: 2*mu_f*eps(v)
        hooke   = lambda t: lmbda*tr(t)*Identity(2) + 2*mu_s*t
        sig_sol = lambda v: hooke(eps(v))

        if energy_type == "l":
            return inner(grad(_u), grad(_v))
        elif energy_type in ("h", "hp"):
            return inner(sig_sol(_u), eps(_v))
        elif energy_type in ("v", "vp"):
            return inner(phi*sig_vis(_u), eps(_v))

    a = (energy(u, v, fluid_energy) + energy(y, w, solid_energy)
        - div(Constant(phi)*v + Constant(1-phi)*w)*p
        + q*div(Constant(phi)*u + Constant(1-phi)*y) )*dx

    dummy = v[0]*dx
    A = PETScMatrix()
    assemble_system(a, dummy, bcs, A_tensor=A)

    b = p*q*dx
    B = PETScMatrix()
    assemble_system(b, dummy, bcs, A_tensor=B)


    # Now use scipy
    from ferosity.PETScHandler import asScipySparse

    Anp = asScipySparse(A)
    Bnp = asScipySparse(B)

    from scipy.sparse.linalg import eigsh

    # In practice, a working shift (sigma) has been observed to be phi_min/10
    out = eigsh(Anp, k=1, M=Bnp, sigma=phi_min/3,
                which='LM', return_eigenvectors=False, maxiter=1e14, tol=1e-12)
    return sqrt(abs(out.min()))

import pylab as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20,'axes.labelweight':'bold' , 'lines.linewidth':2})
plt.rcParams['text.latex.preamble'] = [r'\boldmath']  # Sets bold fonts for latex on axes
import numpy as np

energies = [(_i, _j) for _i,_j in zip(fluid_energies, solid_energies)]
ufys = [ (i,j) for i in range(1,3) for j in range(1,3)]
ufys.remove((1,1))

for fluid_energy, solid_energy in energies:
    results = dict()
    print(" Computing energies {} and {}".format(fluid_energy, solid_energy))
    for u_deg, y_deg in ufys:
        print("Generating fluid P{} / solid P{}".format(u_deg, y_deg))
        infsups = []
        for phi in phis:
            print("-------- Calculating phi={}".format(phi))
            infsups.append(getInfsup(mesh, phi, u_deg, y_deg, 1, fluid_energy, solid_energy))
        results[(u_deg, y_deg)] = infsups

    if save_plot:
    #    plt.style.use('dark_background') # Activate for use in dark slides
        plt.clf()
        plt.semilogy(phis, results[(2,1)], label=r'$P_2$-$P_1$')
        plt.semilogy(phis, results[(1,2)], label=r'$P_1$-$P_2$')
        plt.semilogy(phis, results[(2,2)], label=r'$P_2$-$P_2$')
        plt.legend()
        plt.xlabel(r'$\phi$')
        plt.ylabel(r'$\beta$')
        plt.tight_layout()
        plt.savefig('images/{}/output_semilog{}{}.png'.format(name, fluid_energy, solid_energy), dpi=300, transparent=True)
        plt.clf()
        plt.plot(phis, results[(2,1)], label=r'$P_2$-$P_1$')
        plt.plot(phis, results[(1,2)], label=r'$P_1$-$P_2$')
        plt.plot(phis, results[(2,2)], label=r'$P_2$-$P_2$')
        plt.xlabel(r'{\bf$\phi$}')
        plt.ylabel(r'${\bf\beta}$')
        plt.legend()
        plt.tight_layout()
        plt.savefig('images/{}/output_{}{}.png'.format(name, fluid_energy, solid_energy), dpi=300, transparent=True)