from petsc4py import PETSc
from dolfin import *
from mpi4py import MPI
from time import perf_counter as time
import sys
assert len(sys.argv) == 5, "Must give argument for number of refinements, FE degree, method (Newton|BFGS) and rotation denominator (pi/DENOM)"

time_init = time()
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 3
parameters["mesh_partitioner"] = "ParMETIS"
method = sys.argv[3]  # Newton|BFGS
denom = float(sys.argv[4])
# Set options for KSP
if method == "Newton":
    snes_type = "newtonls"
    ksp_type = "gmres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-6
elif method == "Newton-inexact":
    snes_type = "newtonls"
    ksp_type = "gmres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-2
    PETSc.Options().setValue("-snes_ksp_ew", None)
    PETSc.Options().setValue("-snes_ksp_ew_rtol0", 1e-1)
    PETSc.Options().setValue("-snes_ksp_ew_rtolmax", 0.1)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS-inexact":
    snes_type = "qn"
    ksp_type = "gmres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-1
    PETSc.Options().setValue("-ksp_converged_reason", None)
    PETSc.Options().setValue("-snes_max_it", 1000)
    PETSc.Options().setValue("-snes_lag_jacobian", 1000)
    PETSc.Options().setValue("-snes_lag_preconditioner", 1000)
elif method == "BFGS":
    snes_type = "qn"
    ksp_type = "preonly"
    ksp_atol = 0.0
    ksp_rtol = 0.01
    PETSc.Options().setValue("-snes_max_it", 1000)
    PETSc.Options().setValue("-snes_lag_jacobian", 1000)
    PETSc.Options().setValue("-snes_lag_preconditioner", 1000)
else:
    assert False, "method must be one of Newton|Newton-inexact|BFGS-inexact|BFGS"

# Set options for KSP
PETSc.Options().setValue("-ksp_type", ksp_type)
PETSc.Options().setValue("-ksp_atol", ksp_atol)
PETSc.Options().setValue("-ksp_rtol", ksp_rtol)
PETSc.Options().setValue("-snes_atol", 1e-10)
PETSc.Options().setValue("-snes_rtol", 1e-6)
PETSc.Options().setValue("-snes_stol", 0.0)
PETSc.Options().setValue("-pc_type", "fieldsplit")
PETSc.Options().setValue("-ksp_norm_type", "unpreconditioned")
PETSc.Options().setValue("-ksp_gmres_modifiedgramschmidt", None)
#PETSc.Options().setValue("-ksp_initial_guess_nonzero", None) # Gives problem with BFGS...
PETSc.Options().setValue("-ksp_gmres_restart", 1000)
PETSc.Options().setValue("-ksp_max_it", 2000)
PETSc.Options().setValue("-snes_type", snes_type)
PETSc.Options().setValue("-snes_monitor", None)
#PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-snes_linesearch_type", "basic")
PETSc.Options().setValue("-snes_qn_type", "lbfgs")
PETSc.Options().setValue("-snes_qn_m", 20)
PETSc.Options().setValue("-snes_qn_restart_type", "none")
PETSc.Options().setValue("-snes_qn_scale_type", "jacobian")
# PETSc.Options().setValue("-snes_converged_reason", None)

# Fieldsplit specs
PETSc.Options().setValue("-pc_fieldsplit_type", "schur")
PETSc.Options().setValue("-pc_fieldsplit_schur_fact_type", "lower")
PETSc.Options().setValue("-pc_fieldsplit_schur_precondition", "selfp")
#PETSc.Options().setValue("-pc_fieldsplit_schur_scale", 1.0)

# KSP 0
PETSc.Options().setValue("-fieldsplit_0_ksp_type", "preonly")  # preonly | gmres
#PETSc.Options().setValue("-fieldsplit_0_ksp_initial_guess_nonzero", None)
#PETSc.Options().setValue("-fieldsplit_0_ksp_monitor", None)  # preonly | gmres
#PETSc.Options().setValue("-fieldsplit_0_ksp_norm_type", "none")  # preonly | gmres
#PETSc.Options().setValue("-fieldsplit_0_ksp_max_it", 5)  # preonly | gmres
PETSc.Options().setValue("-fieldsplit_0_pc_type", "hypre")

# KSP 1
PETSc.Options().setValue("-fieldsplit_1_ksp_type", "preonly")  # preonly | gmres
#PETSc.Options().setValue("-fieldsplit_1_ksp_initial_guess_nonzero", None)
#PETSc.Options().setValue("-fieldsplit_1_ksp_monitor", None)  # preonly | gmres
#PETSc.Options().setValue("-fieldsplit_1_ksp_norm_type", "none")  # preonly | gmres
#PETSc.Options().setValue("-fieldsplit_1_ksp_max_it", 5)  # preonly | gmres
PETSc.Options().setValue("-fieldsplit_1_pc_type", "hypre")
#PETSc.Options().setValue("-fieldsplit_1_pc_telescope_reduction_factor", min(MPI.COMM_WORLD.size, 4))
#PETSc.Options().setValue("-fieldsplit_1_telescope_pc_type", "jacobi")


# Solver params
atol, rtol, maxit, alpha, verbose = 1e-8, 1e-6, 500, 1.0, True
rtol_inexact_bfgs = 1e-1
if method == "Newton":
    maxit = 50
anderson_depth = 0  # Used with BFGS

# Model params
N = int(sys.argv[1])
u_deg = int(sys.argv[2])
nx = N*8
ny = N*2
nz = N*2
lx = 8  # m
ly = 1  # m
lz = 1  # m
theta = pi/denom
mesh = BoxMesh(Point(0, 0, 0), Point(lx, ly, lz), nx, ny, nz)
V = FunctionSpace(mesh, MixedElement(
    [VectorElement("CG", mesh.ufl_cell(), u_deg), FiniteElement('DG', mesh.ufl_cell(), 0)]))
if MPI.COMM_WORLD.rank == 0:
    print("Problem dofs = {} ({} + {}), dofs/core = {}".format(V.dim(),
                                                               V.sub(0).dim(), V.sub(1).dim(), V.dim() / MPI.COMM_WORLD.size), flush=True)
#    assert V.dim() / MPI.COMM_WORLD.size <= 250000, "We allow up to 250k dofs per core, sorry!"

# Mark boundary subdomians
left = CompiledSubDomain("near(x[0], side) && on_boundary", side=0.0)
right = CompiledSubDomain("near(x[0], side) && on_boundary", side=lx)

# Define Dirichlet boundary (x = 0 or x = lx)
c = Constant(("0.0", "0.0", "0.0"))
r = Expression(("scale*0.0",
                "scale*(y0 + (x[1] - y0)*cos(theta) - (x[2] - z0)*sin(theta) - x[1])",
                "scale*(z0 + (x[1] - y0)*sin(theta) + (x[2] - z0)*cos(theta) - x[2])"),
               scale=0.5, y0=0.5, z0=0.5, theta=theta, degree=4)

r1 = Expression("scale*(y0 + (x[1] - y0)*cos(theta) - (x[2] - z0)*sin(theta) - x[1])",
                scale=0.5, y0=0.5, z0=0.5, theta=theta, degree=4)
r2 = Expression("scale*(z0 + (x[1] - y0)*sin(theta) + (x[2] - z0)*cos(theta) - x[2])",
                scale=0.5, y0=0.5, z0=0.5, theta=theta, degree=4)

bcl = DirichletBC(V.sub(0), c, left)
bcr = DirichletBC(V.sub(0), r, right)
bcs = [bcl, bcr]
#bcr1 = DirichletBC(V.sub(0).sub(1), r1, right)
#bcr2 = DirichletBC(V.sub(0).sub(2), r2, right)
#bcs = [bcl, bcr1, bcr2]


#FILE = File('images/twist.pvd')
# Problem constants
ap = 9000  # Pa
bp = 9000  # Pa

# Define functions
du = TrialFunction(V)            # Incremental displacement
v = TestFunction(V)             # Test function
sol = Function(V)                 # Displacement
u, p = split(sol)

# Kinematics
I = Identity(3)    # Identity tensor
F = I + grad(u)    # Deformation gradient
J = det(F)
H = J * inv(F).T

# Problem definition
psi = Constant(ap) * (inner(F, F) - 3) + Constant(bp) * \
    (inner(H, H)) - Constant(4 * bp + 2 * ap) * ln(J)
Pi = psi*dx - p * (J - 1) * dx
Res = derivative(Pi, sol, v)
Jac = derivative(Res, sol, du)


class SNESProblem():
    def __init__(self, FF, uu, bbcs):
        V = uu.function_space()
        ddu = TrialFunction(V)
        self.L = FF
        self.a = derivative(FF, uu, ddu)
        self.bcs = bbcs
        self.bcs0 = [DirichletBC(_bc) for _bc in bbcs]
        for bc in self.bcs0:
            bc.homogenize()
        self.u = uu

    def F(self, snes, xx, FF):
        FF = PETScVector(FF)
        xx.copy(self.u.vector().vec())
        self.u.vector().apply("")
        assemble(self.L, tensor=FF)
        for bc in self.bcs0:
            bc.apply(FF)

    def J(self, snes, xx, JJ, PP):
        JJ = PETScMatrix(JJ)
        xx.copy(self.u.vector().vec())
        self.u.vector().apply("")
        assemble(self.a, tensor=JJ)
        for bc in self.bcs0:
            bc.apply(JJ)


for bc in bcs:
    bc.apply(sol.vector())
problem = SNESProblem(Res, sol, bcs)

b = PETScVector()  # same as b = PETSc.Vec()
J_mat = PETScMatrix()
assemble(Res, tensor=b)
assemble(Jac, tensor=J_mat)
for bc in bcs:
    BC = DirichletBC(bc)
    BC.homogenize()
    BC.apply(J_mat, b)

dofmap_s = V.sub(0).dofmap().dofs()
dofmap_p = V.sub(1).dofmap().dofs()
is_s = PETSc.IS().createGeneral(dofmap_s)
is_p = PETSc.IS().createGeneral(dofmap_p)

snes = PETSc.SNES().create(MPI.COMM_WORLD)
snes.setFunction(problem.F, b.vec())
snes.setJacobian(problem.J, J_mat.mat())

# Set up fieldsplit
ksp = snes.getKSP()
pc = ksp.getPC()
pc.setType("fieldsplit")
pc.setFieldSplitIS((None, is_s), (None, is_p))


# Setup and solve
snes.setFromOptions()
snes.ksp.setFromOptions()
snes.solve(None, problem.u.vector().vec())

# Output
l_its = snes.getLinearSolveIterations()
nl_its = snes.getIterationNumber()
converged = snes.getConvergedReason() > 0
if MPI.COMM_WORLD.rank == 0:
    print("Dofs={}".format(V.dim()), flush=True)
    print("Converged={}".format(converged), flush=True)
    print("Nonlinear iterations={}".format(nl_its), flush=True)
    print("Total linear iterations={}".format(l_its), flush=True)
    print("Average linear iterations={:.2f}".format(l_its/nl_its), flush=True)
    print("Wall time={:.2f}".format(time() - time_init), flush=True)
