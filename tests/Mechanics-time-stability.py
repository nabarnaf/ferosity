from time import time
from ferosity.Fibers import generateFibers
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry
from ferosity.MechanicsSolver import MechanicsSolver
from ferosity.Markers import MarkersSolid
from ferosity.Parameters import PhysicalParametersSolid, OutputParameters
from petsc4py import PETSc
from sys import argv
assert len(argv) == 4, "Must give argument for FE degree, method (Newton|BFGS) and activation peak"


degree = int(argv[1])
method = argv[2]
act_peak = float(argv[3])
assert method in ("Newton", "Newton-inexact", "BFGS",
                  "BFGS-inexact"), "given method must be one of Newton|Newton-inexact|BFGS|BFGS-inexact"

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

if method == "Newton":
    snes_type = "newtonls"
    ksp_type = "minres"
    ksp_atol = 1e-10
    ksp_rtol = 1e-6
elif method == "Newton-inexact":
    snes_type = "newtonls"
    ksp_type = "minres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-2
    PETSc.Options().setValue("-snes_ksp_ew", None)
    PETSc.Options().setValue("-snes_ksp_ew_rtol0", 1e-1)
    PETSc.Options().setValue("-snes_ksp_ew_rtolmax", 0.1)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS-inexact":
    snes_type = "qn"
    ksp_type = "minres"
    ksp_atol = 1e-14
    ksp_rtol = 1e-1
    # PETSc.Options().setValue("-ksp_converged_reason", None)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS":
    snes_type = "qn"
    ksp_type = "preonly"
    ksp_atol = 0.0
    ksp_rtol = 0.01
else:
    assert False, "method must be one of Newton|Newton-inexact|BFGS-inexact|BFGS"

# Set options for KSP
PETSc.Options().setValue("-ksp_type", ksp_type)
PETSc.Options().setValue("-ksp_atol", ksp_atol)
PETSc.Options().setValue("-ksp_rtol", ksp_rtol)
PETSc.Options().setValue("-pc_type", "hypre")
# PETSc.Options().setValue("-ksp_norm_type", "unpreconditioned")
PETSc.Options().setValue("-ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-ksp_gmres_restart", 1000)
# PETSc.Options().setValue("-snes_converged_reason", None)
# PETSc.Options().setValue("-snes_monitor", None)
#PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-snes_type", snes_type)
PETSc.Options().setValue("-snes_qn_type", "lbfgs")
PETSc.Options().setValue("-snes_qn_m", 50)
PETSc.Options().setValue("-snes_qn_scale_type", "jacobian")
PETSc.Options().setValue("-snes_linesearch_type", "basic")

name = "test_mechanics"
geom = "prolate_h4_v2_ASCII"
# assert geom in ("prolate_h4_v2_ASCII", "prolate_h4_v2_ASCII_ref", "prolate_4mm", "prolate_4mm_ref")

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
neumannMarkers = []
robinMarkers = [ENDOCARD, BASE, EPICARD]

# Problem setting
t0 = 0.0
dt = 1e-2
tf = 10 * dt
Markers = MarkersSolid(markers, neumannMarkers, robinMarkers)
PhysicalParams = PhysicalParametersSolid(
    dt=dt, t0=t0, sim_time=tf, ys_degree=degree, AS=act_peak, ks=5e4)
OutputParams = OutputParameters(name="result", verbose=False, export_solutions=False)
ts = Constant((0, 0, 0))


MS = MechanicsSolver(name)
MS.setup(mesh, PhysicalParams, Markers, OutputParams)
# Fibers
f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
MS.setFibers(f0, s0, n0)
if MPI.rank(MPI.comm_world) == 0:
    print("Dofs =", MS.V.dim())


# def find_biggest_dt(t):
#     dt_new = dt
#     while t + dt_new <= tf:
#         t_new = t + dt_new
#         converged = MS.solveTimeStep(
#             t_new, method=method, time_update=False)
#         if converged:
#             dt_new += dt
#             PhysicalParams.dt = dt_new
#         else:
#             break
#     PhysicalParams.dt = dt
#     return dt_new


dts = []
for i in range(1, PhysicalParams.Niter):
    t = PhysicalParams.t0 + i*PhysicalParams.dt
    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solving time t={}".format(t), flush=True)
    # dt_biggest = find_biggest_dt(t)
    converged = MS.solveTimeStep(t_new, method=method, time_update=False)

    dts.append((t, dt_biggest))
    # if MPI.rank(MPI.comm_world) == 0:
    #     print("------ dt found:", dt_biggest)
    current_t = time()
    MS.solveTimeStep(t, method=method)

    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solver in {} s".format(time() - current_t), flush=True)
if MPI.rank(MPI.comm_world) == 0:
    print("Found timesteps for method {}:".format(method))
    print(*dts, sep=',')
