from time import time
from ferosity.Fibers import generateFibers
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry
from ferosity.MechanicsSolverMixedQI import MechanicsSolverMixedQI
from ferosity.Markers import MarkersSolid
from ferosity.Parameters import PhysicalParametersSolid, OutputParameters
from petsc4py import PETSc
from sys import argv

if len(argv) > 1:
    assert len(argv) == 2, "Arguments must be: STRING (Newton|Newton-inexact|BFGS|BFGS-inexact)"
    method = argv[1]
    options = ("Newton", "Newton-inexact", "BFGS", "BFGS-inexact")
    assert method in options, "given method must be one of Newton|Newton-inexact|BFGS|BFGS-inexact"
else:
    method = "Newton"
y_degree = 2  # CG
p_degree = 0  # DG

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 4
# TEMP
PETSc.Options().setValue("-ws_0_ksp_type", "cg")
# PETSc.Options().setValue("-ws_0_ksp_pc_side", "right")
PETSc.Options().setValue("-ws_0_ksp_atol", 1e-12)
PETSc.Options().setValue("-ws_0_ksp_rtol", 1e-1)
PETSc.Options().setValue("-ws_0_pc_type", "jacobi")
PETSc.Options().setValue("-ws_1_ksp_type", "cg")
PETSc.Options().setValue("-ws_1_ksp_atol", 1e-12)
PETSc.Options().setValue("-ws_1_ksp_rtol", 1e-1)
# PETSc.Options().setValue("-ws_1_ksp_pc_side", "right")
PETSc.Options().setValue("-ws_1_pc_type", "jacobi")
# PETSc.Options().setValue("-ws_0_ksp_monitor", None)
# PETSc.Options().setValue("-ws_1_ksp_monitor", None)
PETSc.Options().setValue("-ws_schur_ksp_type", "gmres")
PETSc.Options().setValue("-ws_schur_pc_type", "gamg")
PETSc.Options().setValue("-ws_schur_ksp_atol", 1e-12)
PETSc.Options().setValue("-ws_schur_ksp_rtol", 1e-1)


PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-ksp_type", "fgmres")  # Setting this overrides the internal defaults
PETSc.Options().setValue("-pc_type", "lu")  # Setting this overrides the internal defaults
#PETSc.Options().setValue("-pc_type", "fieldsplit")
PETSc.Options().setValue("-ksp_norm_type", "unpreconditioned")
PETSc.Options().setValue("-ksp_pc_side", "right")
PETSc.Options().setValue("-ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-ksp_gmres_restart", 1000)
# PETSc.Options().setValue("-ksp_rtol", 1e-6)  # Same here
# PETSc.Options().setValue("-ksp_atol", 1e-10)  # And here
PETSc.Options().setValue("-ksp_max_it", 200)

# Fieldsplit options
# schur|additive|multiplicative  (add:jacobi, mult:gauss-seidel)
PETSc.Options().setValue("-pc_fieldsplit_type", "schur")
PETSc.Options().setValue("-pc_fieldsplit_schur_fact_type", "upper")  # diag|lower|upper|full
# PETSc.Options().setValue("-pc_fieldsplit_schur_precondition", "selfp")  # SIMPLE

# KSP 0
PETSc.Options().setValue("-fieldsplit_0_ksp_type", "preonly")  # preonly | gmres
PETSc.Options().setValue("-fieldsplit_0_ksp_monitor", None)
PETSc.Options().setValue("-fieldsplit_0_ksp_gmres_restart", 100)
PETSc.Options().setValue("-fieldsplit_0_ksp_rtol", 1e-1)
PETSc.Options().setValue("-fieldsplit_0_ksp_atol", 1e-8)
PETSc.Options().setValue("-fieldsplit_0_ksp_max_it", 200)
PETSc.Options().setValue("-fieldsplit_0_ksp_norm_type", "unpreconditioned")
# PETSc.Options().setValue("-fieldsplit_0_ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-fieldsplit_0_pc_type", "lu")
PETSc.Options().setValue("-fieldsplit_0_pc_asm_local_type", "multiplicative")
PETSc.Options().setValue("-fieldsplit_0_sub_ksp_type", "preonly")
PETSc.Options().setValue("-fieldsplit_0_sub_pc_type", "lu")
PETSc.Options().setValue("-fieldsplit_0_sub_pc_factor_mat_solver_type", "mumps")
PETSc.Options().setValue("-fieldsplit_0_pc_hypre_type", "boomeramg")  # Only of boomeramg

# KSP 1
PETSc.Options().setValue("-fieldsplit_1_ksp_type", "preonly")  # preonly | cg
PETSc.Options().setValue("-fieldsplit_1_ksp_monitor", None)
PETSc.Options().setValue("-fieldsplit_1_ksp_rtol", 1e-1)
PETSc.Options().setValue("-fieldsplit_1_ksp_atol", 1e-8)
PETSc.Options().setValue("-fieldsplit_1_ksp_max_it", 200)
# PETSc.Options().setValue("-fieldsplit_1_ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-fieldsplit_1_ksp_norm_type", "unpreconditioned")
# PETSc.Options().setValue("-fieldsplit_1_pc_type", "telescope")
# PETSc.Options().setValue("-fieldsplit_1_pc_telescope_reduction_factor", 2)
# PETSc.Options().setValue("-fieldsplit_1_telescope_pc_type", "jacobi")

# BAK
# PETSc.Options().setValue("-fieldsplit_1_pc_type", "jacobi")
# PETSc.Options().setValue("-fieldsplit_1_pc_asm_local_type", "additive")
# PETSc.Options().setValue("-fieldsplit_1_sub_ksp_type", "preonly")
# PETSc.Options().setValue("-fieldsplit_1_sub_pc_type", "lu")
# PETSc.Options().setValue("-fieldsplit_1_sub_pc_factor_mat_solver_type", "umfpack")
# PETSc.Options().setValue("-fieldsplit_1_pc_hypre_type", "boomeramg")

name = "test_mechanics"
#geom = "prolate_h4_v2_ASCII"
geom = "prolate_h4_v2_ASCII_ref"
# geom = "prolate_4mm"
# geom = "prolate_4mm_ref"
export = False
LBFGS_order = 30

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
neumannMarkers = []
robinMarkers = [ENDOCARD, BASE, EPICARD]

# Problem setting
Markers = MarkersSolid(markers, neumannMarkers, robinMarkers)
PhysicalParams = PhysicalParametersSolid(
    dt=1e-2, t0=0.0, sim_time=0.015, ys_degree=y_degree, p_degree=p_degree, AS=5e3, ks=5e4)
OutputParams = OutputParameters(name="result", verbose=True, export_solutions=export)
ts = Constant((0, 0, 0))


MS = MechanicsSolverMixedQI(name)
MS.setup(mesh, PhysicalParams, Markers, OutputParams)
# Fibers
f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
MS.setFibers(f0, s0, n0)
if MPI.rank(MPI.comm_world) == 0:
    print("Dofs =", MS.V.dim())
for i in range(1, PhysicalParams.Niter):
    t = PhysicalParams.t0 + i*PhysicalParams.dt
    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solving time t={}".format(t), flush=True)
    current_t = time()

    MS.solveTimeStep(t, method=method, anderson_depth=0, LBFGS_order=LBFGS_order)

    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solver in {} s".format(time() - current_t), flush=True)

    if export:
        MS.exportSolution(t)
