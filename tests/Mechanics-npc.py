from time import perf_counter as time
from ferosity.Fibers import generateFibers
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry
from ferosity.MechanicsSolver import MechanicsSolver
from ferosity.Markers import MarkersSolid
from ferosity.Parameters import PhysicalParametersSolid, OutputParameters
from petsc4py import PETSc
from sys import argv
assert len(argv) == 8, "Must give argument for mesh name, FE degree, activation peak, bulk modulusi (quasi-incompress), time step, final time and iterations outname"
init_time = time()

degree = int(argv[2])
activation_peak = float(argv[3])
ks = float(argv[4])
dt = float(argv[5])
tf = float(argv[6])
iters_outname = argv[7]

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

# Set options for KSP
PETSc.Log().begin() # Used for log_view
PETSc.Options().setValue("-snes_type", "anderson")
PETSc.Options().setValue("-snes_atol", 1e-10)
PETSc.Options().setValue("-snes_rtol", 1e-6)
PETSc.Options().setValue("-snes_stol", 0.0)
PETSc.Options().setValue("-snes_monitor", None)
#PETSc.Options().setValue("-npc_snes_monitor", None)
#PETSc.Options().setValue("-npc_ksp_converged_reason", None)
PETSc.Options().setValue("-snes_error_if_not_converged", True)
#PETSc.Options().setValue("-log_view", None)
#PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-npc_snes_type", "qn")
PETSc.Options().setValue("-npc_snes_max_it", 2)
PETSc.Options().setValue("-npc_snes_qn_type", "lbfgs")
PETSc.Options().setValue("-npc_snes_qn_m", 2)
PETSc.Options().setValue("-npc_snes_lag_jacobian", -2)
PETSc.Options().setValue("-npc_snes_lag_preconditioner", -2)
PETSc.Options().setValue("-npc_snes_jacobian_persistent", True)
PETSc.Options().setValue("-npc_snes_qn_scale_type", "jacobian")
PETSc.Options().setValue("-npc_snes_qn_restart_type", "none")
PETSc.Options().setValue("-npc_ksp_type", "preonly")
PETSc.Options().setValue("-npc_ksp_atol", 0.0)
PETSc.Options().setValue("-npc_ksp_rtol", 1e-6)
PETSc.Options().setValue("-npc_pc_type", "hypre")
#PETSc.Options().setValue("-snes_linesearch_type", "basic")

name = "test_mechanics"
geom = argv[1]
assert geom in ("prolate_h4_v2_ASCII", "prolate_h4_v2_ASCII_ref", "prolate_4mm", "prolate_4mm_ref")
export = False

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
neumannMarkers = []
robinMarkers = [ENDOCARD, BASE, EPICARD]

# Problem setting
Markers = MarkersSolid(markers, neumannMarkers, robinMarkers)
PhysicalParams = PhysicalParametersSolid(
    dt=dt, t0=0.0, sim_time=tf, ys_degree=degree, AS=activation_peak, ks=ks)
OutputParams = OutputParameters(name="result", export_solutions=export)
ts = Constant((0, 0, 0))


MS = MechanicsSolver(name)
MS.setup(mesh, PhysicalParams, Markers, OutputParams)
# Fibers
f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
MS.setFibers(f0, s0, n0)
if MPI.rank(MPI.comm_world) == 0:
    print("Dofs =", MS.V.dim())
for i in range(1, PhysicalParams.Niter):
    t = PhysicalParams.t0 + i*PhysicalParams.dt
    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solving time t={}".format(t), flush=True)
    current_t = time()

    MS.solveTimeStep(t, method="BFGS")

    if export:
        MS.exportSolution(t)
MS.saveIterations(iters_outname)
if MPI.rank(MPI.comm_world) == 0:
    print("Total CPU time:", time() - init_time)
