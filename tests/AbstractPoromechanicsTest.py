"""
Script for testing new abstract structure
"""

import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.CooksonMonolithic import CooksonMonolithic
from ferosity.MeshCreation import generateSquare

name = "abstract_cookson"


k1 = 2e3
k2 = 33
kp = 2e4  # 2e4
ks = 2e2  # Constant for pressure

n_comps = 3
beta=np.ones([n_comps, n_comps])-np.identity(n_comps)

params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 2e3,
          "rhos": 2e3,
          "muf": 4e-3,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 1,
          "dt": 1e-2,
          "export solutions": True,
          "save every": 1,
          "pressure model": "mono",
          "convergence_flag":True,
          "pressure bc use": False,
          "flag semi implicit":False}

save_every = 1

# Problem definition
N = 10
length = 1e-2


def ys_right(t): return Constant(
    (length / 10. * sin(2 * pi * t), 0)) if t <= 1 else Constant((0, 0))


mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)


def evalF(F): return skel_energy(F) + vol_skel(det(F))

ds = Measure('ds', domain=mesh, subdomain_data=markers)

dsNeumann = ds(2) + ds(3)

cookson = CooksonMonolithic(mesh, name, params, evalF, vol_energy, dsNeumann)

# Do artificial swelling for fixed added mass on boundary




def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                    DirichletBC(cookson.V.sub(0).sub(
                        1), Constant(0), markers, BOTTOM)]#,
                    # DirichletBC(cookson.V.sub(1), [Constant(0)]*n_comps, markers, LEFT)]

def f_sur_solid(t): return Constant((t, 0))
def f_vol_solid(t): return Constant((0, 0))
def f_vol_mass(t): return [Constant(1)] * n_comps if t<0.5 else [Constant(0.)] * n_comps

cookson.set_bcs(bcs)
cookson.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
cookson.solve()
