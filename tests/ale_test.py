#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 17:08:49 2018

@author: barnafi
"""

from dolfin import *
from ferosity.Libs import *

# Elastic constants
mu = Constant(1)
eta = Constant(10)
gamma = Constant(100) # weak Robin parameter
dt = Constant(5e-3)
Niter = 50
k1 = Constant(2e3)
k2 = Constant(33)

kp = Constant(2e3)
ks = Constant(2e3)

# Problem definition
N = 10
mesh = UnitSquareMesh(N, N)
el_solid = VectorElement('CG', triangle, 1)
el_fluid = VectorElement('CG', triangle, 1)
V = FunctionSpace(mesh, MixedElement(el_solid, el_fluid) )
Sol = Function(V)
us, uf = split(Sol) # Same as as_vector([Sol[0], Sol[1]]), as_vector([Sol[2], Sol[3]])
vs, vf = TestFunctions(V)

us_n, uf_n = Function(V).split(True)
us_nn = us_n.copy(True)
us_n.rename("disp", "")
us_nn.rename("disp", "")
uf_n.rename("vel", "")

class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0) and on_boundary
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0) and on_boundary
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0) and on_boundary
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0) and on_boundary
left, right, top, bottom = Left(), Right(), Top(), Bottom()
LEFT, RIGHT, TOP, BOTTOM = 1, 2, 3, 4  # Set numbering

markers = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
markers.set_all(0)


boundaries = (left, right, top, bottom)
def_names = (LEFT, RIGHT, TOP, BOTTOM)
for side, num in zip(boundaries, def_names):
    side.mark(markers, num)


# Measures definition
ds = Measure('ds', domain=mesh, subdomain_data=markers)
dx = Measure('dx', domain=mesh)

bcs = DirichletBC(V.sub(0), Constant((0, 0)), markers, LEFT)
bcfinit = DirichletBC(V.sub(1), Constant((10, 0)), markers, LEFT)
bcf = DirichletBC(V.sub(1), Constant((1, 0)), markers, LEFT)

# Helmholtz free energy: Neohookean
ALEform = ALENonLinearForm(mesh, V)

F = grad(us)+Identity(2)


F = variable(F)
C = F.T*F
J = det(F)
I1, I2, I3 = tr(C), 0.5*(tr(C)**2 - tr(C*C)), det(C)
#psi = 0.5*mu*(inner(F, F) - 2) + 0.5*eta*(J-1)**2
psi = k1*(I1*I3**(-1/2) - 2)**2 + k2*(I2*I3**(-1) - 2)**2 \
        + kp*( J - 1 - ln(J))

P = diff(psi, F)

aSolid = dot(us - 2*us_n + us_nn, vs)/dt/dt*dx + inner(P, grad(vs))*dx \
        - dot(Constant((10, 0)), vs)*ds(RIGHT) -dot(Constant((0, -9.8)), vs)*dx
u_zero = Function(V.sub(0).collapse())
ALEform.addDisplacementAndForm(u_zero, aSolid)

aFluid = dot((uf - uf_n)/dt, vf)*dx + dot(div(outer(uf, uf_n - (us - us_n)/dt)), vf)*dx \
        + inner(grad(uf), grad(vf))*dx \
        + gamma*dot(uf - (us - us_n)/dt, vf)*(ds(TOP) + ds(BOTTOM))
ALEform.addDisplacementAndForm(us_n, aFluid)
ALEform.addBC(bcs)
ALEform.addBC(bcf)



bcfinit.apply(Sol.vector())
#fs = File("images/ALE_test_solid.pvd")
#ff = File("images/ALE_test_fluid.pvd")
for i in range(Niter):
    print("Iteration ", i)
#    fs << us_n
#    ff << uf_n
    ALEform.solve(Sol)
    assign(us_nn, us_n)
    assign(us_n, Sol.sub(0))
    assign(uf_n, Sol.sub(1))
    
