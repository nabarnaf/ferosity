from time import time
from ferosity.Fibers import generateFibers
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry
from ferosity.Markers import MarkersSolid
from ferosity.Parameters import PhysicalParametersSolid, OutputParameters
from petsc4py import PETSc
from sys import argv

class PreconditionerWoodburySchur(object):

    def __init__(self, M, is_0, is_1, type = "full"):
        self.M = M
        self.is_0 = is_0
        self.is_1 = is_1
        self.type = type

        assert type in ["lower", "diag", "upper",
                        "full"], "Woodbury Schur type must be one of diag|lower|upper|full"

    def apply_lower_inv(self, x0, x1, y0, y1):
        self.ksp_0.solve(x0, self.temp0)
        self.M10.mult(self.temp0, y1)
        # Out
        self.y1.aypx(-1, x1)
        self.x0.copy(y0)

    def apply_upper_inv(self, x0, x1, y0, y1):
        self.M01.mult(x1, self.temp0)
        self.ksp_0.solve(self.temp0, y0)
        # Out
        self.x1.copy(y1)
        self.y0.aypx(-1, x0)

    def apply_diag_inv(self, x0, x1, y0, y1):
        # Solve first
        self.ksp_0.solve(x0, y0)

        # Solve second using Woodbury
        # we do inv(S)*x1 = inv(M11)*(x1 - M10*inv(-M00+M01*inv(M11)*M10)*M01*inv(M11)*x1)
        
        # This results in temp0
        self.ksp_1.solve(x1, self.temp1)
        self.M01.mult(self.temp1, self.temp0)
        
        # This results in temp1
        self.ksp_schur(self.temp0, self.temp00) 
        # left of parenthesis
        self.M10.mult(self.temp00, self.temp1)
        #ksp_1.solve(temp1, temp11)
        x1.axpy(-1, self.temp1)
        
        # Final 
        self.ksp_1.solve(x1, y1)

    def setUp(self, pc):

        is_0 = self.is_0
        is_1 = self.is_1
        M = self.M
        # Allocate matrices
        self.M00 = M.createSubMatrix(is_0, is_0)
        # M00.setBlockSize(3)
        self.M01 = M.createSubMatrix(is_0, is_1)
        self.M10 = M.createSubMatrix(is_1, is_0)
        self.M11 = M.createSubMatrix(is_1, is_1)

        Mdiag = self.M11.copy()
        Mdiag.zeroEntries()
        Mdiag.setDiagonal(self.M11.getDiagonal())
        self.Mschur = -self.M00 + self.M01 * Mdiag * self.M10

        # Create solvers
        ksp_0 = PETSc.KSP().create()
        ksp_0.setOptionsPrefix("ws_0_")
        ksp_0.setOperators(self.M00, self.M00)
        ksp_0.setFromOptions()

        ksp_1 = PETSc.KSP().create()
        ksp_1.setOptionsPrefix("ws_1_")
        ksp_1.setOperators(self.M11, self.M11)
        ksp_1.setFromOptions()

        ksp_schur = PETSc.KSP().create()
        ksp_schur.setOptionsPrefix("ws_schur_")
        ksp_schur.setOperators(self.Mschur, self.Mschur)
        ksp_schur.setFromOptions()

        self.ksp_0 = ksp_0
        self.ksp_1 = ksp_1
        self.ksp_schur = ksp_schur

        # Create vectors
        self.x0 = PETSc.Vec().create()
        self.x1 = PETSc.Vec().create()
        self.y0 = PETSc.Vec().create()
        self.y1 = PETSc.Vec().create()
        self.temp0 = PETSc.Vec().create()
        self.temp00 = PETSc.Vec().create()
        self.temp000 = PETSc.Vec().create()
        self.temp0000 = PETSc.Vec().create()
        self.temp1 = PETSc.Vec().create()
        self.temp11 = PETSc.Vec().create()
        self.temp111 = PETSc.Vec().create()
        self.temp1111 = PETSc.Vec().create()
        self.vecs_allocated = False

    def apply(self, pc, x, y):
        # Result is y = A^{-1}x, i.e. solves Ay=x
        y.getSubVector(self.is_0, self.y0)
        y.getSubVector(self.is_1, self.y1)
        x.getSubVector(self.is_0, self.x0)
        x.getSubVector(self.is_1, self.x1)
        x0 = self.x0
        y0 = self.y0
        x1 = self.x1
        y1 = self.y1

        if not self.vecs_allocated:
            self.temp0 = x0.copy()
            self.temp00 = x0.copy()
            self.temp000 = x0.copy()
            self.temp0000 = x0.copy()
            self.temp1 = x1.copy()
            self.temp11 = x1.copy()
            self.temp111 = x1.copy()
            self.temp1111 = x1.copy()
            self.vecs_allocated = True

        if type == "diag":
            self.apply_diag_inv(x0, x1, y0, y1)
        elif type == "lower":
            self.apply_lower_inv(x0, x1, self.temp000, self.temp111)
            self.apply_diag_inv(self.temp000, self.temp111, y0, y1)
        elif type == "upper":
            self.apply_diag_inv(x0, x1, self.temp000, self.temp111)
            self.apply_upper_inv(self.temp000, self.temp111, y0, y1)
        else:  # type == "full"
            self.apply_lower_inv(x0, x1, self.temp000, self.temp111)
            self.apply_diag_inv(self.temp000, self.temp111, self.temp0000, self.temp1111)
            self.apply_upper_inv(self.temp0000, self.temp1111, y0, y1)

        x.restoreSubVector(self.is_0, x0)
        x.restoreSubVector(self.is_1, x1)
        y.restoreSubVector(self.is_0, y0)
        y.restoreSubVector(self.is_1, y1)
        # Solve second using Woodbury
        # we do inv(S)*x1 = inv(M11)*(x1 - M10(-M00+M01*inv(M11)*M10)*M01*inv(M11)*x1)

        #### THIS IS THE SCHUR ONLY IMPLEMENTATION ###
        # if not vecs_allocated:
        #     temp1 = x.copy()
        # temp0 = ksp_0.getOperators()[0].getVecRight()
        # ksp_1.solve(x, temp1)
        # M01.mult(temp1, temp0)

        # # This results in temp1
        # # first one
        # #temp00 = temp0.copy()
        # #M00.mult(temp0, temp00)
        # # second one
        # #M10.mult(temp0, temp1)
        # #temp11 = temp1.copy()
        # #ksp_1.solve(temp1, temp11)
        # #M01.mult(temp11, temp0)
        # #temp0.axpy(-1, temp00)
        # if not vecs_allocated:
        #     temp00 = temp0.copy()
        # ksp_schur(temp0, temp00)
        # # left of parenthesis
        # M10.mult(temp00, temp1)
        # if not vecs_allocated:
        #     temp11 = temp1.copy()
        #     vecs_allocated = True
        # ksp_1.solve(temp1, temp11)

        # # Final inv(M11)x1 - t11
        # ksp_1.solve(x, y)
        # y.axpy(-1, temp11)
        # y.scale(-1) # TEST PETSc uses Schut with -1 scale for positivity
        
class Newton:

    def __init__(self, jac0, atol, rtol, maxit, alpha, verbose=False, inexact=False):
        self.atol = atol
        self.rtol = rtol
        self.maxit = maxit
        self.alpha = alpha
        self.verbose = verbose
        self.inexact = inexact
        ksp = PETSc.KSP().create()
        ksp.setType('gmres')
        ksp.setOperators(jac0)
        pc = ksp.getPC()
        pc.setType('hypre')
        pc.setFromOptions()
        if inexact:
            self.rtol_linear = 1e-2
            ksp.setTolerances(1e-2, 0.0, 1e20, 1000)
        else:
            ksp.setTolerances(1e-8, 1e-10, 1e20, 1000)
        ksp.setFromOptions()
        self.ksp = ksp
        self.do_fieldsplit = False
        self.sub_allocated = False

    def apply_bcs(self, obj, bcs):
        if bcs:
            for bb in bcs:
                bb.apply(obj)

    def set_mechanics_fieldsplit_pc(self, V0, V1):
        self.V0 = V0
        self.V1 = V1
        self.do_fieldsplit = True

    def set_mechanics_fieldsplit_pc_setup(self, jac):
        dofmap_s = self.V0.dofmap().dofs()
        dofmap_p = self.V1.dofmap().dofs()
        is_s = PETSc.IS().createGeneral(dofmap_s)
        is_p = PETSc.IS().createGeneral(dofmap_p)
        self.is_s = is_s
        self.is_p = is_p

        self.ksp = PETSc.KSP().create()
        self.ksp.setOperators(jac.mat())
        self.ksp.setType("fgmres")
        pc = self.ksp.getPC()
        pc.setType('python')
        ctx = PreconditionerWoodburySchur(jac.mat(), is_s, is_p)
        pc.setPythonContext(ctx)
        pc.setOperators(jac.mat())
        pc.setUp()
        self.ksp.setPC(pc)
        self.ksp.setFromOptions()
        pc.setFromOptions()
        # pc.setFieldSplitIS((None, is_s), (None, is_p))
        # pc.setUp()  # Must be called after set from options
        # ksps = pc.getFieldSplitSubKSP()

        # Create Woodbury Schur action
        # ksps[1].setType("preonly")
        # PC = PETSc.PC().create()
        # PC.setType('python')
        # ctx = PreconditionerWoodburySchur(jac.mat(), is_s, is_p)
        # PC.setPythonContext(ctx)
        # PC.setOperators(jac.mat().createSubMatrix(is_p, is_p))
        # PC.setUp()
        # ksps[1].setPC(PC)

        # Then setup everything
        # for ksp in ksps:
        #     ksp.setFromOptions()
        #     pc_inner = ksp.getPC()
        #     pc_inner.setFromOptions()
        #     pc_inner.setUp()

    def solve(self, sol, res_fun, jac_fun, bcs=None, bs=None):

        if bcs:
            for bb in bcs:
                bb.apply(sol)
                bb.homogenize()
        t0 = time()
        err_abs = 1.0
        err_rel = 1.0
        it = 0
        du = sol.copy()
        du.zero()
        res = PETScVector()
        jac = PETScMatrix()

        res_fun(res)
        self.apply_bcs(res, bcs)
        jac_fun(jac)
        self.apply_bcs(jac, bcs)

        res0 = res.norm('l2')
        total_krylov = 0
        if inexact:
            rtol = self.rtol_linear
            self.res_old = res0
        while err_abs > self.atol and err_rel > self.rtol and it < maxit:
            it += 1

            self.ksp.setOperators(jac.mat())
            if self.do_fieldsplit:
                self.set_mechanics_fieldsplit_pc_setup(jac)
            if self.inexact:
                PHI = (1 + np.sqrt(5))/2
                if rtol**PHI > 0.1:
                    rtol = max(rtol, rtol**PHI)
                if rtol >= 1:
                    rtol = 1e-1
                self.ksp.setTolerances(rtol, 0.0, 1e14, 1000)

            self.ksp.setFromOptions()
            self.ksp.setUp()
            self.ksp.solve(res.vec(), du.vec())
            krylov_its = self.ksp.getIterationNumber()
            # krylov_its = df.solve(jac, du, res, 'gmres', 'hypre_amg')
            # solve(A, du, b)
            du.apply("")
            sol.vec().axpy(-1, du.vec())  # we solve jac du = - res
            # sol.apply("")
            res_fun(res)
            self.apply_bcs(res, bcs)
            jac_fun(jac)
            if bs:
                jac.mat().setBlockSize(bs)
            self.apply_bcs(jac, bcs)
            err_abs = res.norm('l2')
            err_rel = err_abs/res0

            # Update tolerance for inexact
            if self.inexact:
                rtol = 0.1 * abs(ksp.getResidualNorm() - err_abs) / \
                    res_old  # Einstat-Walker Choice 1 with a factor
                rtol = min(rtol, 0.1)  # Never go over 0.1
                res_old = err_abs
            if self.verbose and MPI.COMM_WORLD.rank == 0:
                print('it {}, err abs = {:.3e}  err rel = {:.3e} in {} GMRES iterations'.format(
                    it, err_abs, err_rel, krylov_its), flush=True)
            total_krylov += krylov_its
            if min(err_abs, err_rel) > 1e14 or np.isnan(err_abs) or np.isnan(err_rel):
                if MPI.COMM_WORLD.rank == 0:
                    print("\t Newton diverged")
                return False
        if(MPI.COMM_WORLD.rank == 0 and it < self.maxit):
            print("\t Newton converged in {} nonlinear its, {} total krylov its, {} avg krylov its, {:.3f}s".format(
                it, total_krylov, total_krylov/it, time() - t0), flush=True)
        if it == self.maxit:
            return False
        else:
            return True


class BFGS:
    def __init__(self, jac, atol, rtol, maxit, alpha, verbose=False, anderson_depth=0, LM=False, LM_order=10, inexact=False):
        atol = atol
        rtol = rtol
        maxit = maxit
        alpha = alpha
        verbose = verbose
        anderson_depth = anderson_depth
        LM = LM  # Low memory
        LM_order = LM_order  # Number of iterations to be used with LM
        ksp = PETSc.KSP().create()
        if inexact:
            ksp.setType('gmres')
            ksp.setTolerances(1e-2, 0.0, 1e20, 100)
        else:
            ksp.setType('preonly')
        ksp.setOperators(jac)
        pc = ksp.getPC()
        ksp.setFromOptions()
        pc.setFromOptions()
        self.ksp = ksp
        b = None
        sks = []
        yks = []
        rhoks = []

        def H0(self, vec, k):
            assert k == 0, "Preconditioner in BFGS must me called at last level in recursion always"
            # return 0.1*vec
            if not b:
                b = vec.copy()
            vec.copy(b)
            ksp.solve(b, vec)
            # return vec
        Hs = [H0]
        k = 0

    def set_mechanics_fieldsplit_pc(self, V0, V1):
        dofmap_s = V0.dofmap().dofs()
        dofmap_p = V1.dofmap().dofs()
        is_s = PETSc.IS().createGeneral(dofmap_s)
        is_p = PETSc.IS().createGeneral(dofmap_p)

        pc = ksp.getPC()
        pc.setType('fieldsplit')
        pc.setFromOptions()
        pc.setFieldSplitIS((None, is_s))
        pc.setFieldSplitIS((None, is_p))
        pc.setUp()  # Must be called after set from options
        ksps = pc.getFieldSplitSubKSP()
        for ksp in ksps:
            ksp.setFromOptions()

    def update(self, sk, yk):
        sks.append(sk)
        yks.append(yk)
        rhoks.append(1 / (yk.dot(sk)))

        if LM:
            # Truncate iterations
            if len(sks) > LM_order:
                sks.pop(0)
                yks.pop(0)
                rhoks.pop(0)
            assert len(sks) <= LM_order, "More previous iterations than allowed."
        else:
            # Add recursive functions
            def Hk(vec, k):
                rhok = rhoks[k-1]
                skdvec = sks[k-1].dot(vec)
                vec.axpy(-rhok*skdvec, yks[k-1])
                Hs[k-1](vec, k-1)
                vec.axpy(-rhok * yks[k-1].dot(vec), sks[k-1])
                vec.axpy(rhok * skdvec, sks[k-1])
            Hs.append(Hk)

    def apply(self, vec):
        if LM:
            iters = len(sks)  # Number of iterations
            alphas = np.zeros(iters)
            betas = np.zeros(iters)

            # Use two-loop recursion
            for ii in range(iters):
                jj = iters - ii - 1  # invert direction
                alphas[jj] = rhoks[jj] * vec.dot(sks[jj])
                vec.axpy(-alphas[jj], yks[jj])
            Hs[0](vec, 0)
            for ii in range(iters):
                betas[ii] = rhoks[ii] * yks[ii].dot(vec)
                vec.axpy(alphas[ii] - betas[ii], sks[ii])
        else:
            return Hs[-1](vec, len(Hs)-1)

    def apply_bcs(self, obj, bcs):
        if bcs:
            for bb in bcs:
                bb.apply(obj)

    def apply_bc_petsc(self, vec, bcs):
        if bcs:
            for bb in bcs:
                vals = bb.get_boundary_values()
                for dof in vals:
                    vec.setValue(dof, vals[dof])

    def solve(self, sol, res_fun, jac_fun=None, bcs=None):
        t0 = time()
        # If there are BCs, homogenize them after initializing solution
        if bcs:
            for bb in bcs:
                bb.apply(sol)
                bb.homogenize()
        soln = sol.vec().copy()  # resn is PETSc vector

        res = PETScVector()
        res_fun(res)  # Initial residual
        apply_bcs(res, bcs)
        resn = res.vec().copy()
        anderson = AndersonAcceleration(anderson_depth)
        error0 = res.norm('l2')
        error_rel = error_abs = 1
        it = 0
        total_krylov = 0
        while error_abs > atol and error_rel > rtol and it < maxit:
            apply(res.vec())
            krylov_its = ksp.getIterationNumber()

            do_LS = False  # TODO: Line search
            if do_LS:
                pass
            sol.vec().axpy(-alpha, res.vec())
            anderson.get_next_vector(sol.vec())
            sol.apply("")
            res_fun(res)
            apply_bcs(res, bcs)

            # They must be allocated as the iteration count is unkown
            update(sol.vec() - soln, res.vec() - resn)  # sk, yk

            sol.vec().copy(soln), res.vec().copy(resn)
            error_abs = res.norm('l2')
            error_rel = error_abs / error0
            if(MPI.COMM_WORLD.rank == 0 and verbose):
                print("it {}, err_abs={:.3e}, err_rel={:.3e} in {} GMRES iterations".format(
                    it, error_abs, error_rel, krylov_its), flush=True)
            it += 1
            total_krylov += krylov_its
            if min(error_abs, error_rel) > 1e14 or np.isnan(error_abs) or np.isnan(error_rel):
                if MPI.COMM_WORLD.rank == 0:
                    print("\t BFGS diverged")
                return False
        if(MPI.COMM_WORLD.rank == 0 and it < maxit):
            print("\t BFGS converged in {} nonlinear its, {} total krylov its, {} avg krylov its, {:.3f}s".format(
                it, total_krylov, total_krylov/it, time() - t0), flush=True)
        if it == maxit:
            return False
        else:
            return True


######## PROGRAM START ########
if len(argv) > 1:
    assert len(argv) == 2, "Arguments must be: STRING (Newton|Newton-inexact|BFGS|BFGS-inexact)"
    method = argv[1]
    options = ("Newton", "Newton-inexact", "BFGS", "BFGS-inexact")
    assert method in options, "given method must be one of Newton|Newton-inexact|BFGS|BFGS-inexact"
else:
    method = "Newton"
y_degree = 1  # CG
p_degree = 0  # DG

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 4
# TEMP
PETSc.Options().setValue("-ws_0_ksp_type", "cg")
# PETSc.Options().setValue("-ws_0_ksp_pc_side", "right")
PETSc.Options().setValue("-ws_0_ksp_atol", 1e-12)
PETSc.Options().setValue("-ws_0_ksp_rtol", 1e-4)
PETSc.Options().setValue("-ws_0_pc_type", "gamg")
PETSc.Options().setValue("-ws_1_ksp_type", "cg")
PETSc.Options().setValue("-ws_1_ksp_atol", 1e-12)
PETSc.Options().setValue("-ws_1_ksp_rtol", 1e-4)
# PETSc.Options().setValue("-ws_1_ksp_pc_side", "right")
PETSc.Options().setValue("-ws_1_pc_type", "jacobi")
# PETSc.Options().setValue("-ws_0_ksp_monitor", None)
# PETSc.Options().setValue("-ws_1_ksp_monitor", None)
PETSc.Options().setValue("-ws_schur_ksp_type", "gmres")
PETSc.Options().setValue("-ws_schur_pc_type", "gamg")
PETSc.Options().setValue("-ws_schur_ksp_atol", 1e-12)
PETSc.Options().setValue("-ws_schur_ksp_rtol", 1e-4)


PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-ksp_type", "fgmres")  # Setting this overrides the internal defaults
#PETSc.Options().setValue("-pc_type", "lu")  # Setting this overrides the internal defaults
#PETSc.Options().setValue("-pc_type", "fieldsplit")
PETSc.Options().setValue("-ksp_norm_type", "unpreconditioned")
PETSc.Options().setValue("-ksp_pc_side", "right")
PETSc.Options().setValue("-ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-ksp_gmres_restart", 1000)
# PETSc.Options().setValue("-ksp_rtol", 1e-6)  # Same here
# PETSc.Options().setValue("-ksp_atol", 1e-10)  # And here
PETSc.Options().setValue("-ksp_max_it", 200)

# Fieldsplit options
# schur|additive|multiplicative  (add:jacobi, mult:gauss-seidel)
PETSc.Options().setValue("-pc_fieldsplit_type", "schur")
PETSc.Options().setValue("-pc_fieldsplit_schur_fact_type", "upper")  # diag|lower|upper|full
# PETSc.Options().setValue("-pc_fieldsplit_schur_precondition", "selfp")  # SIMPLE

# KSP 0
PETSc.Options().setValue("-fieldsplit_0_ksp_type", "preonly")  # preonly | gmres
PETSc.Options().setValue("-fieldsplit_0_ksp_monitor", None)
PETSc.Options().setValue("-fieldsplit_0_ksp_gmres_restart", 100)
PETSc.Options().setValue("-fieldsplit_0_ksp_rtol", 1e-1)
PETSc.Options().setValue("-fieldsplit_0_ksp_atol", 1e-8)
PETSc.Options().setValue("-fieldsplit_0_ksp_max_it", 200)
PETSc.Options().setValue("-fieldsplit_0_ksp_norm_type", "unpreconditioned")
# PETSc.Options().setValue("-fieldsplit_0_ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-fieldsplit_0_pc_type", "lu")
PETSc.Options().setValue("-fieldsplit_0_pc_asm_local_type", "multiplicative")
PETSc.Options().setValue("-fieldsplit_0_sub_ksp_type", "preonly")
PETSc.Options().setValue("-fieldsplit_0_sub_pc_type", "lu")
PETSc.Options().setValue("-fieldsplit_0_sub_pc_factor_mat_solver_type", "mumps")
PETSc.Options().setValue("-fieldsplit_0_pc_hypre_type", "boomeramg")  # Only of boomeramg

# KSP 1
PETSc.Options().setValue("-fieldsplit_1_ksp_type", "preonly")  # preonly | cg
PETSc.Options().setValue("-fieldsplit_1_ksp_monitor", None)
PETSc.Options().setValue("-fieldsplit_1_ksp_rtol", 1e-1)
PETSc.Options().setValue("-fieldsplit_1_ksp_atol", 1e-8)
PETSc.Options().setValue("-fieldsplit_1_ksp_max_it", 200)
# PETSc.Options().setValue("-fieldsplit_1_ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-fieldsplit_1_ksp_norm_type", "unpreconditioned")
# PETSc.Options().setValue("-fieldsplit_1_pc_type", "telescope")
# PETSc.Options().setValue("-fieldsplit_1_pc_telescope_reduction_factor", 2)
# PETSc.Options().setValue("-fieldsplit_1_telescope_pc_type", "jacobi")

# BAK
# PETSc.Options().setValue("-fieldsplit_1_pc_type", "jacobi")
# PETSc.Options().setValue("-fieldsplit_1_pc_asm_local_type", "additive")
# PETSc.Options().setValue("-fieldsplit_1_sub_ksp_type", "preonly")
# PETSc.Options().setValue("-fieldsplit_1_sub_pc_type", "lu")
# PETSc.Options().setValue("-fieldsplit_1_sub_pc_factor_mat_solver_type", "umfpack")
# PETSc.Options().setValue("-fieldsplit_1_pc_hypre_type", "boomeramg")

name = "test_mechanics"
geom = "prolate_h4_v2_ASCII"
# geom = "prolate_4mm_ref"
# geom = "prolate_4mm_ref"
export = False
LBFGS_order = 30

###### GEOMETRY #######
mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
neumannMarkers = []
robinMarkers = [ENDOCARD, BASE, EPICARD]
####################### GEOMETRY

###### Problem setting ######
Markers = MarkersSolid(markers, neumannMarkers, robinMarkers)
physicalParameters = PhysicalParametersSolid(
    dt=1e-2, t0=0.0, sim_time=0.015, ys_degree=y_degree, p_degree=p_degree, AS=5e3, ks=5e4)
outputParameters = OutputParameters(name="result", verbose=True, export_solutions=export)
ts = Constant((0, 0, 0))
############################# Problem setting


###### Functional spaces and weak form ######
y_el = VectorElement("CG", mesh.ufl_cell(), physicalParameters.ys_degree)
p_el = FiniteElement("DG", mesh.ufl_cell(), physicalParameters.p_degree)
V = FunctionSpace(mesh, MixedElement([y_el, p_el]))
Sol = Function(V)

        # Initialize solutions
ys_n = Function(V.sub(0).collapse(), name="displacement")
ys_nn = ys_n.copy(True)

dx = Measure('dx', domain=mesh, metadata={'optimize': True})
dx = dx(degree=5)
ds = Measure('ds', domain=mesh, subdomain_data=Markers.markers,
                          metadata={'optimize': True})
dsEmpty = ds(Markers.NONE)
dSN = sum([ds(i) for i in Markers.neumannMarkers], dsEmpty)
dSRob = sum([ds(i) for i in Markers.robinMarkers], dsEmpty)

ys, p = split(Sol)
ws, q = TestFunctions(V)
dt = Constant(physicalParameters.dt)

# Auxiliary variables
idt = 1/dt
us = idt * (ys - ys_n)
us_n = idt * (ys_n - ys_nn)

F = Identity(3) + grad(ys)
J = det(F)
J = variable(J)
F = variable(F)
Cbar = det(F)**(-2/3) * F.T * F

# Usyk,. mc Culloch 2002
Cg = .88e3   # [Pa]
bf = 8       # [-]
bs = 6       # [-]
bn = 3       # [-]
bfs = 12      # [-]
bfn = 3       # [-]
bsn = 3       # [-]
k = physicalParameters.ks

f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
E = 0.5*(Cbar - Identity(3))
Eff, Efs, Efn = inner(E*f0, f0), inner(E*f0, s0), inner(E*f0, n0)
Esf, Ess, Esn = inner(E*s0, f0), inner(E*s0, s0), inner(E*s0, n0)
Enf, Ens, Enn = inner(E*n0, f0), inner(E*n0, s0), inner(E*n0, n0)

Q = Constant(bf) * Eff**2 \
            + Constant(bs) * Ess**2 \
            + Constant(bn) * Enn**2 \
            + Constant(bfs) * 2.0 * Efs**2 \
            + Constant(bfn) * 2.0 * Efn**2 \
            + Constant(bsn) * 2.0 * Esn**2
WP = 0.5*Constant(Cg)*(exp(Q)-1)
WV = Constant(k)/2*(J-1)*ln(J)

W = WP + WV

P = diff(WP, F) - p * J * inv(F).T
# We later impose weakly that p = - diff(WV, J)

# Setup forms

# Pfaller et al.
k_perp = Constant(2e5)  # [Pa/m]
c_perp = Constant(5e3)  # [Pa*s/m]

n = FacetNormal(mesh)
ts = Constant((0, 0, 0))
rhos = Constant(physicalParameters.rhos)
ts_robin = -outer(n, n)*(k_perp*ys + c_perp*us) - (Identity(3) - outer(n, n))*k_perp/10*ys
Fs = (inner(P, grad(ws)) + rhos/dt*dot(us - us_n, ws))*dx \
            - dot(ts, ws)*dSN \
            - dot(ts_robin, ws)*dSRob
Fp = -Constant(1/k) * (p + diff(WV, J)) * q * dx

C = F.T*F
I4f = dot(f0, C*f0)
T_wave = 0.8
time_stim = Expression("t<0.4?sin(2*pi*t/0.8):0", t=0, degree=0)
Ta = Constant(physicalParameters.AS)*time_stim
Pa = Ta*outer(F*f0, f0)/sqrt(I4f)

FF =  Fs + Fp + inner(Pa, grad(ws))*dx
Jac = derivative(FF, Sol)
#############################################


if MPI.rank(MPI.comm_world) == 0:
    print("Dofs =", V.dim())

jac = PETScMatrix()
assemble(Jac, tensor=jac)
inexact = True if "inexact" in method else False
status = False

for i in range(1, physicalParameters.Niter):
    t = physicalParameters.t0 + i*physicalParameters.dt
    time_stim.t = t
    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solving time t={}".format(t), flush=True)
    current_t = time()
    if method in ("Newton", "Newton-inexact"):
        def res_fun(res):
            assemble(FF, tensor=res)

        def jac_fun(jac):
            assemble(Jac, tensor=jac)

        atol = 1e-8
        rtol = 1e-6
        maxit = 100
        alpha = 1.0
        solver = Newton(jac.mat(), atol, rtol, maxit, alpha, verbose=outputParameters.verbose, inexact=inexact)

    elif method in ("BFGS", "BFGS-inexact"):
        def res_fun(res):
            assemble(F, tensor=res)
        atol = 1e-8
        rtol = 1e-6
        maxit = 100
        alpha = 1.0
        solver = BFGS(jac.mat(), atol, rtol, maxit, alpha, verbose=outputParameters.verbose, anderson_depth=anderson_depth, LM=True, LM_order=LBFGS_order, inexact=inexact)

    solver.set_mechanics_fieldsplit_pc(V.sub(0), V.sub(1))

    status = solver.solve(Sol.vector(), res_fun, jac_fun)
    ys_nn.assign(ys_n)
    assign(ys_n, Sol.sub(0))


    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solver in {} s".format(time() - current_t), flush=True)

