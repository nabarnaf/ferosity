"""
Script for testing new abstract structure
"""

from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.CooksonMonolithic import CooksonMonolithic
from ferosity.MeshCreation import generateSquare

name = "cookson_vol_model_study"


k1 = 2e3
k2 = 33
kp = 2e4  # 2e4
ks = 2e4  # Constant for pressure

n_comps = 2
params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 2e3,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "degree solid": 1,
          "degree mass": 1,
          "t0": 0,
          "tf": 1,
          "dt": 1e-2,
          "export solutions": True,
          "save every": 1,
          "pressure model": "mono",
          "pressure bc use": False}

save_every = 1

# Problem definition
N = 10
length = 1e-2


def ys_right(t): return Constant(
    (length / 10. * sin(2 * pi * t), 0)) if t <= 1 else Constant((0, 0))


mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)


def evalF(F): return skel_energy(F) + vol_skel(det(F))

def solve_problem(model):
    params["pressure model"] = model
    cookson = CooksonMonolithic(mesh, name + model, params, evalF, vol_energy, None, None)

    # Do artificial swelling for fixed added mass on boundary


    def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                        DirichletBC(cookson.V.sub(0).sub(
                            1), Constant(0), markers, BOTTOM),
                        DirichletBC(cookson.V.sub(1).extract_sub_space([0]), Constant(2e3 * t), markers, LEFT)]


    cookson.set_bcs(bcs)
    cookson.solve()

solve_problem("mono")
solve_problem("multi")