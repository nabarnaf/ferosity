from time import perf_counter as time
from ferosity.Fibers import generateFibers
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry
from ferosity.MechanicsSolver import MechanicsSolver
from ferosity.Markers import MarkersSolid
from ferosity.Parameters import PhysicalParametersSolid, OutputParameters
from petsc4py import PETSc
from sys import argv
assert len(argv) == 9, "Must give argument for mesh name, FE degree, method (Newton|BFGS), activation peak, bulk modulusi (quasi-incompress), time step, final time and iterations outname"
init_time = time()

degree = int(argv[2])
method = argv[3]
activation_peak = float(argv[4])
ks = float(argv[5])
dt = float(argv[6])
tf = float(argv[7])
iters_outname = argv[8]
assert method in ("Newton", "Newton-inexact", "BFGS",
                  "BFGS-inexact"), "given method must be one of Newton|Newton-inexact|BFGS|BFGS-inexact"

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

if method == "Newton":
    snes_type = "newtonls"
    ksp_type = "minres"
    ksp_atol = 0.0
    ksp_rtol = 1e-6
elif method == "Newton-inexact":
    snes_type = "newtonls"
    ksp_type = "minres"
    ksp_atol = 0.0
    ksp_rtol = 1e-2
    PETSc.Options().setValue("-snes_ksp_ew", None)
    PETSc.Options().setValue("-snes_ksp_ew_rtol0", 1e-2) # 1e-2 good for cardiac
    PETSc.Options().setValue("-snes_ksp_ew_rtolmax", 0.1)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS-inexact":
    snes_type = "qn"
    ksp_type = "minres"
    ksp_atol = 0.0
    ksp_rtol = 1e-1
    # PETSc.Options().setValue("-ksp_converged_reason", None)
    PETSc.Options().setValue("-snes_lag_jacobian", 1000)
    PETSc.Options().setValue("-snes_lag_preconditioner", 1000)
    PETSc.Options().setValue("-snes_max_it", 1000)
elif method == "BFGS":
    snes_type = "qn"
    ksp_type = "preonly"
    ksp_atol = 0.0
    ksp_rtol = 0.0
    PETSc.Options().setValue("-snes_max_it", 1000)
    PETSc.Options().setValue("-snes_lag_jacobian", -2)
    PETSc.Options().setValue("-snes_lag_preconditioner", -2)
else:
    assert False, "method must be one of Newton|Newton-inexact|BFGS-inexact|BFGS"

# Set options for KSP
PETSc.Log().begin() # Used for log_view
PETSc.Options().setValue("-ksp_type", ksp_type)
PETSc.Options().setValue("-ksp_atol", ksp_atol)
PETSc.Options().setValue("-ksp_rtol", ksp_rtol)
PETSc.Options().setValue("-snes_atol", 1e-10)
PETSc.Options().setValue("-snes_rtol", 1e-6)
PETSc.Options().setValue("-snes_stol", 0.0)
PETSc.Options().setValue("-pc_type", "hypre")
PETSc.Options().setValue("-ksp_gmres_modifiedgramschmidt", None)
PETSc.Options().setValue("-ksp_gmres_restart", 1000)
# PETSc.Options().setValue("-snes_converged_reason", None)
PETSc.Options().setValue("-ksp_converged_reason", None)
PETSc.Options().setValue("-snes_monitor", None)
PETSc.Options().setValue("-snes_error_if_not_converged", True)
#PETSc.Options().setValue("-log_view", None)
#PETSc.Options().setValue("-ksp_monitor", None)
PETSc.Options().setValue("-snes_type", snes_type)
PETSc.Options().setValue("-snes_qn_type", "lbfgs")
PETSc.Options().setValue("-snes_qn_m", 20)
PETSc.Options().setValue("-snes_qn_scale_type", "jacobian")
PETSc.Options().setValue("-snes_qn_restart_type", "none")
PETSc.Options().setValue("-snes_linesearch_type", "basic")

name = "test_mechanics"
geom = argv[1]
assert geom in ("prolate_h4_v2_ASCII", "prolate_h4_v2_ASCII_ref", "prolate_4mm", "prolate_4mm_ref")
export = False

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
neumannMarkers = []
robinMarkers = [ENDOCARD, BASE, EPICARD]

# Problem setting
Markers = MarkersSolid(markers, neumannMarkers, robinMarkers)
PhysicalParams = PhysicalParametersSolid(
    dt=dt, t0=0.0, sim_time=tf, ys_degree=degree, AS=activation_peak, ks=ks)
OutputParams = OutputParameters(name="result", export_solutions=export)
ts = Constant((0, 0, 0))


MS = MechanicsSolver(name)
MS.setup(mesh, PhysicalParams, Markers, OutputParams)
# Fibers
f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
MS.setFibers(f0, s0, n0)
if MPI.rank(MPI.comm_world) == 0:
    print("Dofs =", MS.V.dim())
for i in range(1, PhysicalParams.Niter):
    t = PhysicalParams.t0 + i*PhysicalParams.dt
    if MPI.rank(MPI.comm_world) == 0:
        print("--- Solving time t={}".format(t), flush=True)
    current_t = time()

    MS.solveTimeStep(t, method=method)

    if export:
        MS.exportSolution(t)
MS.saveIterations(iters_outname)
if MPI.rank(MPI.comm_world) == 0:
    print("Total CPU time:", time() - init_time)
