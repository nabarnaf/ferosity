"""
Script for testing new abstract structure
"""

from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog, Quadratic
from ferosity.Physics.CooksonMonolithic import CooksonMonolithic
from ferosity.MeshCreation import generateSquare
from ferosity.PressureBC import PressureBC

set_log_active(False)
parameters['form_compiler']['optimize'] = True
parameters['form_compiler']["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 10


name = "cookson_differing_laws/cookson_differing_laws"


k1 = 2e3
k2 = 33
kp_values = {'l': 1e2, 'm': 1e3, 'h': 1e5}
ks_values = {'l': 1e2, 'm': 1e3, 'h': 1e5}

laws = {'log': LinpLog, 'quad': Quadratic}

n_comps = 1
params = {"permeabilities": [1e-4] * n_comps,
          "rhof": 2e3,
          "initial porosities": [0.1] * n_comps,
          "number compartments": n_comps,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 3,
          "dt": 1e-3,
          "export solutions": True,
          "save every": 100,
          "pressure model": "mono",
          "pressure bc use": True,
          "pressure bc tol": 1e-6,
          "pressure bc maxiter": 200,
          "pressure bc log": False}


# Problem definition
N = 10
length = 1e-2

mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N, length)

skel_energy = CiarletGeymonat(k1, k2)


def solve_problem(kp, ks, law_solid, law_vol, postfix):
     vol_skel = law_solid(kp)
     vol_energy = law_vol(ks)

     def evalF(F): return skel_energy(F) + vol_skel(det(F))

     cookson = CooksonMonolithic(
         mesh, name + postfix, params, evalF, vol_energy, None, None)

     def bcs(t): return [DirichletBC(cookson.V.sub(0).sub(0), Constant(0), markers, LEFT),
                         DirichletBC(cookson.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

     def pressure_func(t): return Constant(1e2 *
                                           params["initial porosities"][0] * (1 - exp(-t**2 / 0.25)))

     pressure_bc = [PressureBC(markers, LEFT, pressure_func, 0)]

     cookson.set_bcs(bcs, pressure_bc)
     cookson.solve()


for law_solid_key in laws:
     for law_vol_key in laws:
          for kp_key in kp_values:
               for ks_key in ks_values:
                    postfix = law_solid_key + law_vol_key + kp_key + ks_key
                    print("Solving" + postfix)
                    solve_problem(kp_values[kp_key], ks_values[ks_key], laws[law_solid_key], laws[law_vol_key], postfix)