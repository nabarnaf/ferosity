"""
@author: Giulia Ferla, Micol Bassanini
"""

import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import generateSquare
import matplotlib.pyplot as plt

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
#parameters["form_compiler"]["quadrature_degree"] = 6

name = "Accuracy_analysis"

set_log_active(False)

k1 = 2e3
k2 = 33
kp = 2e4
ks = 2e2  # Constant for pressure
semi_implicit_flag = True

n_comps = 3
beta = 1e-3 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))

params_monolithic = {"permeabilities": [1e-7] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 2e-3,
          "dt": 5e-5,
          "export solutions": False,
          "geometry": "square",
          "save every": 1,
          "method": "monolithic",
          "scheme absolute tol": 1e-11,
          "scheme relative tol": 1e-7,
          "maxiter": 1000,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": True,
          "compare iterations": False,
          "flag semi implicit": semi_implicit_flag}

params_fixedpoint = params_monolithic.copy()
params_fixedpoint["method"] = "fixed point"

params_staggered = params_monolithic.copy()
params_staggered["method"] = "staggered"

save_every = 1

# Problem definition
N = [8, 16, 32, 40]
space_flag = len(N) > 1
N_fine = 64
length = 1e-2

time_mono = np.zeros(len(N))
time_fixedpoint = np.zeros(len(N))
time_staggered = np.zeros(len(N))

skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)

def evalF(F): return skel_energy(F) + vol_skel(det(F))
def f_sur_solid(t):
        return Constant((0, 0))

def f_vol_solid(t):
        return Constant((0, 0))

def f_vol_mass(t):
        source = Expression(
            "(x[0]<0.5 * L)? 0.5 : 0", L=length, degree=6)
        return [source] + [Constant(0.0)] * (n_comps - 1)

mesh_fine, markers_fine, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N_fine, length)
NeumannMarkers_fine = [TOP, RIGHT]
RobinMarkers_fine = []

cookson_exact = Cookson(mesh_fine, name, params_monolithic, evalF, vol_energy, markers_fine, NeumannMarkers_fine, RobinMarkers_fine)
def bcs_exact(t):
    return [DirichletBC(cookson_exact.V.sub(0).sub(0), Constant(0), markers_fine, LEFT),
            DirichletBC(cookson_exact.V.sub(0).sub(
            1), Constant(0), markers_fine, BOTTOM)]
cookson_exact.set_bcs(bcs_exact)
cookson_exact.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
cookson_exact.solve()

err_y_L2_mono_N = np.zeros(len(N))
err_y_H1_mono_N = np.zeros(len(N))
err_mtot_L2_mono_N = np.zeros(len(N))
err_mtot_H1_mono_N = np.zeros(len(N))
err_y_L2_fixedpoint_N = np.zeros(len(N))
err_y_H1_fixedpoint_N = np.zeros(len(N))
err_mtot_L2_fixedpoint_N = np.zeros(len(N))
err_mtot_H1_fixedpoint_N = np.zeros(len(N))
err_y_L2_staggered_N = np.zeros(len(N))
err_y_H1_staggered_N = np.zeros(len(N))
err_mtot_L2_staggered_N = np.zeros(len(N))
err_mtot_H1_staggered_N = np.zeros(len(N))

nn = 0
while nn < len(N):

    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N[nn], length)

    NeumannMarkers = [TOP, RIGHT]
    RobinMarkers = []
    cookson_mono = Cookson(mesh, name, params_monolithic, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)
    cookson_fixedpoint = Cookson(mesh, name, params_fixedpoint, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)
    cookson_staggered = Cookson(mesh, name, params_staggered, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)  

    def bcs_mono(t):
        return [DirichletBC(cookson_mono.V.sub(0).sub(0), Constant(0), markers, LEFT),
                DirichletBC(cookson_mono.V.sub(0).sub(
                1), Constant(0), markers, BOTTOM)]

    def bcs_y_fixedpoint(t):
        return [DirichletBC(cookson_fixedpoint.Vs.sub(0), Constant(0), markers, LEFT),
                DirichletBC(cookson_fixedpoint.Vs.sub(
                1), Constant(0), markers, BOTTOM)]

    def bcs_m_fixedpoint(t):
        return []

    def bcs_y_staggered(t):
        return [DirichletBC(cookson_staggered.Vs.sub(0), Constant(0), markers, LEFT),
                DirichletBC(cookson_staggered.Vs.sub(
                1), Constant(0), markers, BOTTOM)]

    def bcs_m_staggered(t):
        return []

    from time import time
    cookson_mono.set_bcs(bcs_mono)
    cookson_mono.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_mono.solve()
    time_mono[nn] = time() - current_time
    cookson_fixedpoint.set_bcs([bcs_y_fixedpoint, bcs_m_fixedpoint])
    cookson_fixedpoint.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_fixedpoint.solve()
    time_fixedpoint[nn] = time() - current_time
    cookson_staggered.set_bcs([bcs_y_staggered, bcs_m_staggered])
    cookson_staggered.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_staggered.solve()
    time_staggered[nn] = time() - current_time


    n_sols = len(cookson_mono.y_sol)
    err_y_L2_mono = np.zeros(n_sols)
    err_y_H1_mono = np.zeros(n_sols)
    err_mtot_L2_mono = np.zeros(n_sols)
    err_mtot_H1_mono = np.zeros(n_sols)
    err_y_L2_fixedpoint = np.zeros(n_sols)
    err_y_H1_fixedpoint = np.zeros(n_sols)
    err_mtot_L2_fixedpoint = np.zeros(n_sols)
    err_mtot_H1_fixedpoint = np.zeros(n_sols)
    err_y_L2_staggered = np.zeros(n_sols)
    err_y_H1_staggered = np.zeros(n_sols)
    err_mtot_L2_staggered = np.zeros(n_sols)
    err_mtot_H1_staggered = np.zeros(n_sols)

    ti = params["t0"] + params["dt"]
    it = 0

    while ti <= params["tf"]:

        ti += params["dt"]

        deg_rise = 3

        # Error in added mass
        sol_m_mono = cookson_mono.m_sol[it]
        err_mtot_L2_mono[it] = errornorm(cookson_exact.m_sol[it], sol_m_mono, "L2", degree_rise=deg_rise)
        err_mtot_H1_mono[it] = errornorm(cookson_exact.m_sol[it], sol_m_mono, "H1", degree_rise=deg_rise)
        sol_m_fp = cookson_fixedpoint.m_sol[it]
        err_mtot_L2_fixedpoint[it] = errornorm(cookson_exact.m_sol[it], sol_m_fp, "L2", degree_rise=deg_rise)
        err_mtot_H1_fixedpoint[it] = errornorm(cookson_exact.m_sol[it], sol_m_fp, "H1", degree_rise=deg_rise)
        sol_m_stagg = cookson_staggered.m_sol[it]
        err_mtot_L2_staggered[it] = errornorm(cookson_exact.m_sol[it], sol_m_stagg, "L2", degree_rise=deg_rise)
        err_mtot_H1_staggered[it] = errornorm(cookson_exact.m_sol[it], sol_m_stagg, "H1", degree_rise=deg_rise)

        # Error in displacement
        sol_y_mono = cookson_mono.y_sol[it]
        err_y_L2_mono[it] = errornorm(cookson_exact.y_sol[it], sol_y_mono, "L2", degree_rise=deg_rise)
        err_y_H1_mono[it] = errornorm(cookson_exact.y_sol[it], sol_y_mono, "H1", degree_rise=deg_rise)
        sol_y_fp = cookson_fixedpoint.y_sol[it]
        err_y_L2_fixedpoint[it] = errornorm(cookson_exact.y_sol[it], sol_y_fp, "L2", degree_rise=deg_rise)
        err_y_H1_fixedpoint[it] = errornorm(cookson_exact.y_sol[it], sol_y_fp, "H1", degree_rise=deg_rise)
        sol_y_stagg = cookson_staggered.y_sol[it]
        err_y_L2_staggered[it] = errornorm(cookson_exact.y_sol[it], sol_y_stagg, "L2", degree_rise=deg_rise)
        err_y_H1_staggered[it] = errornorm(cookson_exact.y_sol[it], sol_y_stagg, "H1", degree_rise=deg_rise)

        it += 1

    if space_flag:
        err_y_L2_mono_N[nn] = err_y_L2_mono.max()
        err_y_H1_mono_N[nn] = err_y_H1_mono.max()
        err_mtot_L2_mono_N[nn] = err_mtot_L2_mono.max()
        err_mtot_H1_mono_N[nn] = err_mtot_H1_mono.max()
        err_y_L2_fixedpoint_N[nn] = err_y_L2_fixedpoint.max()
        err_y_H1_fixedpoint_N[nn] = err_y_H1_fixedpoint.max()
        err_mtot_L2_fixedpoint_N[nn] = err_mtot_L2_fixedpoint.max()
        err_mtot_H1_fixedpoint_N[nn] = err_mtot_H1_fixedpoint.max()
        err_y_L2_staggered_N[nn] = err_y_L2_staggered.max()
        err_y_H1_staggered_N[nn] = err_y_H1_staggered.max()
        err_mtot_L2_staggered_N[nn] = err_mtot_L2_staggered.max()
        err_mtot_H1_staggered_N[nn] = err_mtot_H1_staggered.max()
    nn += 1

    if not space_flag:
        times = np.linspace(params_monolithic["t0"] + params_monolithic["dt"],
                    params_monolithic["tf"], len(err_y_L2_mono))
        plt.figure(1)
        plt.plot(times, err_y_L2_mono, label=r'error L2 y mono')
        plt.plot(times, err_y_L2_fixedpoint, label=r'error L2 y fixedpoint')
        plt.plot(times, err_y_L2_staggered, label=r'error L2 y staggered')
        plt.legend(loc='best')
        plt.savefig('accuracy_y_L2.png')
        plt.clf()
        plt.figure(2)
        plt.plot(times, err_y_H1_mono, label=r'error H1 y mono')
        plt.plot(times, err_y_H1_fixedpoint, label=r'error H1 y fixedpoint')
        plt.plot(times, err_y_H1_staggered, label=r'error H1 y staggered')
        plt.legend(loc='best')
        plt.savefig('accuracy_y_H1.png')
        plt.clf()
        plt.figure(3)
        plt.plot(times, err_mtot_L2_mono, label=r'error L2 m mono')
        plt.plot(times, err_mtot_L2_fixedpoint, label=r'error L2 m fixedpoint')
        plt.plot(times, err_mtot_L2_staggered, label=r'error L2 m staggered')
        plt.legend(loc='best')
        plt.savefig('accuracy_m_L2.png')
        plt.clf()
        plt.figure(4)
        plt.plot(times, err_mtot_H1_mono, label=r'error H1 m mono')
        plt.plot(times, err_mtot_H1_fixedpoint, label=r'error H1 m fixedpoint')
        plt.plot(times, err_mtot_H1_staggered, label=r'error H1 m staggered')
        plt.legend(loc='best')
        plt.savefig('accuracy_m_H1.png')
        plt.clf()


if space_flag:
    print("\ntime solving monolithic formulation: ", time_mono)
    print("\ntime solving fixedpoint formulation: ", time_fixedpoint)
    print("\ntime solving staggered formulation: ", time_staggered)
    plt.figure(1)
    plt.plot(N, time_mono, label=r'mono')
    plt.plot(N, time_fixedpoint, label=r'fixedpoint')
    plt.plot(N, time_staggered, label=r'staggered')
    plt.legend(loc='best')
    plt.savefig('time_compare.png')
    plt.clf()
    print("\nN=", N)
    print("\nError L2 of monolithic solver in displacement: ", err_y_L2_mono_N)
    print("\nError L2 of fixed point solver in displacement: ", err_y_L2_fixedpoint_N)
    print("\nError L2 of staggered solver in displacement: ", err_y_L2_staggered_N)
    print("\nError H1 of monolithic solver in displacement: ", err_y_H1_mono_N)
    print("\nError H1 of fixed point solver in displacement: ", err_y_H1_fixedpoint_N)
    print("\nError H1 of staggered solver in displacement: ", err_y_H1_staggered_N)
    print("\nError L2 of monolithic solver in added mass: ", err_mtot_L2_mono_N)
    print("\nError L2 of fixed point solver in added mass: ", err_mtot_L2_fixedpoint_N)
    print("\nError L2 of staggered solver in added mass: ", err_mtot_L2_staggered_N)
    print("\nError H1 of monolithic solver in added mass: ", err_mtot_H1_mono_N)
    print("\nError H1 of fixed point solver in added mass: ", err_mtot_H1_fixedpoint_N)
    print("\nError H1 of staggered solver in added mass: ", err_mtot_H1_staggered_N)
