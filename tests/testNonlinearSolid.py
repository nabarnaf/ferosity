from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry

CRITICAL  = 50 # errors that may lead to data corruption and suchlike
ERROR     = 40 # things that go boom
WARNING   = 30 # things that may go boom later
INFO      = 20 # information of general interest
PROGRESS  = 16 # what's happening (broadly)
TRACE     = 13 # what's happening (in detail)
DBG       = 10 # sundry

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "nonlinear_prolate"
#geom = "prolate_h4_v2_ASCII"
geom = "prolate_4mm"

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
neumannSolidMarkers = [ENDOCARD, BASE]
neumannRobinMarkers = [EPICARD]
neumannFluidMarkers = [BASE]
noSlipMarkers  = [ENDOCARD, EPICARD]


# Output file
xdmf = XDMFFile("../images/{}/{}.xdmf".format(name, "result"))
xdmf.parameters["functions_share_mesh"] = True
xdmf.parameters["flush_output"] = True


# Parameters

sim_time = 1.0
dim = mesh.topology().dim()
dt  = Constant(1e-2)
t0  = 0
Niter = int((sim_time - t0)/dt(0))
ys_degree = 1
uf_degree = 2
us_degree = 1
p_degree  = 1
rhos  = Constant(1e3)  # [kg/m^3]
rhof  = Constant(1e3)  # [kg/m^3]
phi   = Constant(0.05)
mu_f  = Constant(3e-2)
ks    = Constant(2e8)  # From Chapelle
lmbda = Constant(710)
mu_s  = Constant(4066)
gamma = Constant(2e5)
D = Constant(1e7)
AS = Constant(3e4)  # Active stress

# Pfaller et al.
k_perp = Constant(2e5)  # [Pa/m]  
c_perp = Constant(5e3)  # [Pa*s/m]

# Functional setting

cell = mesh.ufl_cell()
ys_el = VectorElement('CG', cell, ys_degree)
uf_el = VectorElement('CG', cell, uf_degree)
us_el = VectorElement('CG', cell, us_degree)
p_el  = FiniteElement('CG', cell, p_degree)

mix_el = MixedElement(ys_el, us_el, uf_el, p_el)

V = FunctionSpace(mesh, mix_el)


# Fibers
from ferosity.Fibers import generateFibers

f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
ufl_norm = lambda _x: sqrt(dot(_x, _x))
normalize = lambda _x: _x/ufl_norm(_x)
f0 = normalize(f0)
s0 = normalize(s0)
n0 = normalize(n0)


# Boundary conditions

dx = Measure('dx', domain=mesh, metadata={'optimize':True})
dx = dx(degree=5)
ds = Measure('ds', domain=mesh, subdomain_data=markers, metadata={'optimize':True})
dsEmpty = ds(NONE)

dSNs  = sum([ds(i) for i in neumannSolidMarkers], dsEmpty)
dSNf  = sum([ds(i) for i in neumannFluidMarkers], dsEmpty)
dSnos = sum([ds(i) for i in noSlipMarkers], dsEmpty)
dSRob = sum([ds(i) for i in neumannRobinMarkers], dsEmpty)


# quotes = mesh.coordinates()[:,2]
# min_quote = MPI.min(MPI.comm_world, quotes.min())

# Here we fix a point on the BASE, choosing it so that is has minimal x
# z = min(mesh.coordinates()[:, 2])
# z = MPI.min(MPI.comm_world, z)  # Get BASE coordinate

# coords = mesh.coordinates()
# base_coords = coords[coords[:, 2]==z]

# comm = MPI.comm_world

# PP = None
# rank = comm.Get_rank()
# print(rank, "1", flush=True)

# size = comm.Get_size()
# rank = comm.Get_rank()

# if len(base_coords)>0:
#     ind_x = base_coords[:, 0].argmin()
#     bdry_fixed = base_coords[ind_x]
# else:
#     bdry_fixed = 9999,9999,9999

# # Collect idx, point on root

# points = comm.gather(bdry_fixed, root = 0)

# # Process everything on 0
# if rank == 0:
#     import numpy as np
#      # Get minimum x coordinate
#     arg = np.argmin([x for x, y, z in points])
#     PP  = points[arg]  # Get rank of minimum 

# PP = comm.bcast(PP, root = 0)  # This gets the BASE point with smallest x



# def point(p):
#     # Hard coded from arbitrary point from geometry h4_v2_ASCII, it has smallest x at BASE
#     x, y, z = PP
#     result = near(p[0], x) #and near(p[1], y) and near(p[2], z)
#     return result


# bcs = [ DirichletBC(V.sub(0).sub(2), Constant(0), markers, BASE)]

### External Loads

f     = Constant((0, 0, 0))
theta = Constant(0)
tf    = Constant((0, 0, 0))
ts    = Constant((0, 0, 0))


### Problem definition

def C( ten ):
    return 2*mu_s*ten + lmbda*tr(ten)*Identity(dim)

def eps(vec):
    return sym(grad(vec))

Sol = Function(V)
ys, us, uf, p = split(Sol)
Test = TestFunction(V)
ws, vs, vf, q = split(Test)

# Current iterates
Sol_n = Function(V)
ys_n, us_n, uf_n, p_n = split(Sol_n)

F = Identity(dim) + grad(ys)
F = variable(F)
J = det(F)
#W = lmbda*exp(tr(F.T*F) - 3) + ks*log(J)*(1 - log(J))**2
#W = (mu_s/2)*exp(tr(F.T*F) - 3) - mu_s*ln(J) + (lmbda/2)*(ln(J))**2  # Fenics law

# Usyk,. mc Culloch 2002
Cg  = .88e3   # [Pa]
bf  = 8       # [-]
bs  = 6       # [-]
bn  = 3       # [-]
bfs = 12      # [-]
bfn = 3       # [-]
bsn = 3       # [-]
k   = 5e4

E = 0.5*(F.T*F - Identity(3))
Eff, Efs, Efn = inner(E*f0, f0), inner(E*f0, s0), inner(E*f0, n0)
Esf, Ess, Esn = inner(E*s0, f0), inner(E*s0, s0), inner(E*s0, n0)
Enf, Ens, Enn = inner(E*n0, f0), inner(E*n0, s0), inner(E*n0, n0)

#        Q  = bf*Eff**2 + bt*(Ess**2+Enn**2+Esn**2+Ens**2) + bfs*(Efs**2+Esf**2+Efn**2+Enf**2)
Q  = Constant(bf )       * Eff**2 \
   + Constant(bs )       * Ess**2 \
   + Constant(bn )       * Enn**2 \
   + Constant(bfs) * 2.0 * Efs**2 \
   + Constant(bfn) * 2.0 * Efn**2 \
   + Constant(bsn) * 2.0 * Esn**2
WP = 0.5*Constant(Cg)*(exp(Q)-1)
WV = Constant(k)/2*(J-1)*ln(J)

W = WP + WV

P = diff(W, F)
sigma_vis  = 2*mu_f*eps(uf)

# Setup forms

n = FacetNormal(mesh)
h = Constant(mesh.hmax())
Ff  = (phi*inner(sigma_vis, eps(vf)) - p*div(phi*vf)
        + phi**2*D*dot(uf - (ys - ys_n)/dt, vf)
        + rhof*phi/dt*dot(uf - uf_n, vf)
        - theta*dot(uf, vf))*dx \
        + (gamma/h*dot(uf - us, vf) - dot((sigma_vis - p*Identity(dim))*n, vf))*dSnos \
            - (rhof*phi*dot(f, vf))*dx \
            - phi*dot(tf, vf)*dSNf

Fm  =  ((1-phi)**2/(ks*dt)*(p - p_n)*q
         + 1/dt*q*div((1-phi)*(ys - ys_n)) + q*div(phi*uf))*dx \
             - 1/rhof*theta*q*dx

phis = 1-phi
ts_robin = -outer(n, n)*(k_perp*ys + c_perp*us) - (Identity(3) - outer(n, n))*k_perp/10*ys
Fs  = (inner(P, grad(ws)) - p*div(phis*ws)
        + phi**2*D*dot((ys - ys_n)/dt - uf , ws)
        + rhos*phis/dt*dot(us - us_n, ws))*dx \
            - rhos*phis*dot(f, ws)*dx \
            - dot(ts, ws)*dSNs \
            - dot(ts_robin, ws)*dSRob
def getActive(t):
    F = Identity(dim) + grad(ys)
    C = F.T*F
    I4f = dot(f0, C*f0)
    T_wave = 0.6
    # time_stim = lambda _t: sin(2*DOLFIN_PI*t/T_wave) if t<= 0.3 else 0  
    time_stim = lambda _t: sin(2*DOLFIN_PI*t)**2
    Ta = Constant(AS*time_stim(t))  # Heartbeat in 1 second
    Pa = Ta*outer(F*f0, f0)/sqrt(I4f)
    return inner(Pa, grad(ws))*dx

def exportSolution(t, U):
    getCoord = lambda i: interpolate(U.sub(i), V.sub(i).collapse())
    names = ["displacement", "solid_vel", "fluid_vel", "pressure"]
    for i in range(4):
        func = getCoord(i)
        func.rename(names[i], names[i])
        xdmf.write(func, t)

Fv = (phis*dot(us, vs) - 1/dt*phis*dot(ys - ys_n, vs))*dx

F = Ff + Fm + Fs + Fv



#
## Problem solution
exportSolution(0, Sol_n)
from time import time
for i in range(Niter):
    t = t0 + i*dt(0)
    if MPI.rank(MPI.comm_world) == 0: print ("--- Solving time t={}".format(t), flush=True)
    current_t = time()

    Ftot = F+getActive(t)
    Jac = derivative(Ftot, Sol)
    # problem = NonlinearVariationalProblem(Ftot, Sol, bcs, J)
    problem = NonlinearVariationalProblem(Ftot, Sol, J=Jac)
    solver  = NonlinearVariationalSolver(problem)
    prm = solver.parameters
    prm['newton_solver']['absolute_tolerance'] = 1E-8
    prm['newton_solver']['relative_tolerance'] = 1E-7
    prm['newton_solver']['maximum_iterations'] = 25
    # prm['newton_solver']['linear_solver'] = 'lu'
    prm['newton_solver']['relaxation_parameter'] = 1.0
    # prm['preconditioner'] = 'ilu'
    # prm['krylov_solver']['absolute_tolerance'] = 1E-9
    # prm['krylov_solver']['relative_tolerance'] = 1E-7
    # prm['krylov_solver']['maximum_iterations'] = 1000
    # prm['krylov_solver']['gmres']['restart'] = 40
    # prm['krylov_solver']['preconditioner']['ilu']['fill_level'] = 0
    set_log_level(20)
    solver.solve()

    # solve(totF == 0, Sol, bcs=bcs, J=derivative(totF, Sol))
    if MPI.rank(MPI.comm_world) == 0: print("--- Solver in {} s".format(time() - current_t), flush=True)

    # Update solution
    Sol_n.assign(Sol)
    exportSolution(t, Sol_n)
xdmf.close()
print("Done")
