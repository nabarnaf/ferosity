#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 19:12:54 2019

@author: barnafi
"""


import ferosity.LinearBurtschell as LB
from ferosity.MeshCreation import generateSquare
from ferosity.Markers import Markers
from parameters.LinearConvergence import physical_params, output_params
from ferosity.Parameters import PhysicalParameters, ExternalForces, OutputParameters
from ferosity.Solvers.LinearizedMonolithicSolver import LinearizedMonolithicSolver as MonolithicSolver
import ferosity.Libs as Libs
from dolfin import *

# Dolfin parameters
set_log_level(40)
#parameters["form_compiler"]["quadrature_degree"] = 10
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
name = "linear_convergence_dt_hopt_d1"

def h2dt(h, delta):
    """ 
    Returns a time step so as to satisfy a CFL condition
    """ 
    uf_deg = physical_params['uf_degree']
    desired_convergence = delta
    exponent1 = (4 * uf_deg - 2 * desired_convergence ) / 3
    exponent2 = uf_deg - desired_convergence
    return  min(h ** exponent1, h ** exponent2)

##################
# Code beggining #
##################

# Convergence iterations
CONST = 5
init  = 1 # Take CONST*init elements per side
steps = 5 # Arrive to CONST*(init+steps)
names_methods = {"linear_convergence_dt_1em2": lambda h: 1e-2, 
                "linear_convergence_dt_h": lambda h: h, 
                "linear_convergence_dt_hh": lambda h: h**2, 
                "linear_convergence_dt_hopt_d1": lambda h: h2dt(h, 1),
                "linear_convergence_dt_hopt_d2": lambda h: h2dt(h, 2)}


### Create analytic solutions with sympy ###

import sympy as sp
from sympy.abc import x, y, t
from sympy.functions import Max, Min
from ferosity.sympy2fenics import grad, epsilon, hookeTensor, div, sympy2exp, infer_polynomial_degree

mu_s  = physical_params['mu_s']
lmbda = physical_params['lmbda']
mu_f  = physical_params['mu_f']
phi0  = physical_params['phi'](0)
rhof  = physical_params['rhof']
rhos0 = physical_params['rhos']
ks    = physical_params['ks']
D     = physical_params['D']
phit = t**2
#yys  = phit*l*sp.Matrix([sp.sin(x), 0])
yys  = phit*sp.Matrix([0.5*x**2*sp.cos(y), -x*sp.sin(y)])
vvs  = yys.diff(t)
#vvf  = sp.Matrix([phit*sp.sin(sp.pi*y/l), 0])
vvf  = phit*sp.Matrix([sp.sin(x-y)**2, sp.sin(x-y)**2])
pp   = phit*(1 - sp.sin(sp.pi*x/2))
# Now create stress tensors 
Grad = lambda u: grad(u, 2)
sigma_skel = hookeTensor(epsilon(yys, 2), mu_s, lmbda, 2)
sigma_vis  = 2*mu_f*epsilon(vvf, 2)

# Then create load terms
# First the source theta
phis = 1-phi0
theta = rhof*((1-phi0)**2/ks*pp.diff(t) + div(phi0*vvf + phis*vvs))

# Then the fluid and solid loads
spff = 1/(rhof*phi0)*( rhof*phi0*vvf.diff(t)- div(phi0*sigma_vis) + phi0*Grad(pp).transpose()
        + phi0**2*D*(vvf - vvs) - theta*vvf )

spfs = 1/(rhos0*phis)*( rhos0*phis*vvs.diff(t) - div(sigma_skel) + phis*Grad(pp).transpose()
        - phi0**2*D*(vvf - vvs)  )

# Finally, obtain expressions for the corresponding terms
genExpression = lambda _p: lambda _t: Expression(sympy2exp(_p), degree = 4, cell=triangle, t = _t)
Sigma_s  = genExpression(sigma_skel - phis*pp*sp.eye(2))
Sigma_f  = genExpression(sigma_vis - pp*sp.eye(2))
Ff       = genExpression(spff)
Fs       = genExpression(spfs)
Theta    = genExpression(theta)

ys_ex = genExpression(yys)
us_ex = genExpression(vvs)
uf_ex = genExpression(vvf)
p_ex  = genExpression(pp)


# Generate parameter objects
physicalParams = PhysicalParameters(**physical_params)
outputParams   = OutputParameters(**output_params)



def calculateError(ys, us, uf, p, dt):
    """
    Returns the Linfty(0,T;V) error for all terms
    """
    import numpy as np
    Ntimesteps = len(ys)
    ys_error = []
    us_error = []
    uf_error = []
    p_error  = []
    
    for i in range(Ntimesteps):
        if i%10==0:
            print("Calculating error it {}/{}, t={}".format(i+1, Ntimesteps, i*dt))
        ys_error.append(errornorm(ys_ex(i*dt), ys[i], "H1"))
        us_error.append(errornorm(us_ex(i*dt), us[i], "L2"))
        uf_error.append(errornorm(uf_ex(i*dt), uf[i], "H1"))
        p_error.append( errornorm( p_ex(i*dt),  p[i], "L2"))
        
    return np.max(ys_error), np.max(us_error), np.max(uf_error), np.max(p_error)
        
    

def setSolver(NN, h2dt_function):
    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(Nelements = NN, length = 1)
    Markers_obj = Markers(markers,
                          neumannSolidMarkers=[TOP, RIGHT, BOTTOM],
                          neumannFluidMarkers=[RIGHT],
                          noSlipMarkers=[NONE], none=NONE)
    bc = lambda t: [DirichletBC(Solver.Vsolid(), ys_ex(t), markers, LEFT),
                    DirichletBC(Solver.Vfluid(), uf_ex(t), markers, TOP),
                    DirichletBC(Solver.Vfluid(), uf_ex(t), markers, BOTTOM),
                    DirichletBC(Solver.Vfluid(), uf_ex(t) , markers, LEFT)]

    n  = FacetNormal(mesh)
    dt = h2dt_function(mesh.hmax())
    ts = lambda t: Sigma_s(t)*n
    tf = lambda t: Sigma_f(t)*n
    externalForces = ExternalForces(**{'ff': Ff, 'fs': Fs, 'ts':ts, 'tf':tf, 'theta': Theta})
    # Setup solver
    Solver = LB.LinearBurtschell(name)
    
    physicalParams.dt = dt
    Solver.setup(mesh, physicalParams, externalForces, Markers_obj, outputParams, MonolithicSolver)
    Solver.setBC(bc)

    Solver.solve()
    
    return Solver.dim(), mesh.hmax(), calculateError(Solver.ysList, Solver.usList, Solver.ufList, Solver.pList, dt)

for name in names_methods:

    print("---------------Test convergence rates for " + name)


    eys = []
    rys = []
    eus = []
    rus = []
    euf = []
    ruf = []
    ep  = []
    rp  = []
    hh  = []
    dofs = []

    for N in range(init, init+steps+1):
        print("---------------Solve problem with N={}*{} elements per side".format(CONST, N))
        dof, h, (yserr, userr, uferr, perr) = setSolver(2**N, names_methods[name])
        dofs.append(dof)
        hh.append(h)
        eys.append(yserr)
        eus.append(userr)
        euf.append(uferr)
        ep.append(perr)
        
        if N==init:
            continue
        else:
            i = N-init
            lnhh = ln(hh[i]/hh[i-1])
            rys.append(ln(eys[i]/eys[i-1])/lnhh)
            rus.append(ln(eus[i]/eus[i-1])/lnhh)
            ruf.append(ln(euf[i]/euf[i-1])/lnhh)
            rp.append( ln(ep[i]/ep[i-1])/lnhh)

    out_lines = []
    names = " dofs &  hh  &  eys  &  rys  &  eus  &  rus  &  euf  &  ruf  &  ep  &  rp " 
    print(names)
    out_lines.append(names)
    for i in range(steps):
        if i ==0:
            out = " {} & {:.3e} &  {:.3e}  & --  &  {:.3e}  &  --  &  {:.3e}  &  --  &  {:.3e}  &  -- \n".format(dofs[i], hh[i], eys[i], eus[i], euf[i], ep[i])
            print(out)
            out_lines.append(out)
        else:
            out = " {} & {:.3e} &  {:.3e}  &  {:.3g}  &  {:.3e}  &  {:.3g}  &  {:.3e}  &  {:.3g}  &  {:.3e}  &  {:.3g} \n".format(dofs[i], hh[i], eys[i], rys[i], eus[i], rus[i], euf[i], ruf[i], ep[i], rp[i])
            print(out)
            out_lines.append(out)

    with open(name, 'w') as f:
        f.writelines(out_lines)
