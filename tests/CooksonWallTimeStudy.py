"""
@author: Giulia Ferla, Micol Bassanini
"""

import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import generateSquare
import matplotlib.pyplot as plt

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 3

name = "wall-time"

set_log_active(False)
# Set pretty pylab
plt.rc('font', family='serif')
plt.rc('text', usetex=True)
plt.rcParams.update(
    {'font.size': 18, 'lines.linewidth': 4})

k1 = 2e3
k2 = 33
kp = 2e4
ks = 2e4  # Constant for pressure
semi_implicit_flag = False

n_comps = 2
beta = 5e-6 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))

params = {"permeabilities": [1e-7] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 1e-3,
          "dt": 1e-4,
          "export solutions": False,
          "geometry": "square",
          "save every": 1,
          "scheme absolute tol": 1e-10,
          "scheme relative tol": 1e-7,
          "maxiter": 1000,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": True,
          "compare iterations": False,
          "flag semi implicit": semi_implicit_flag}

params_monolithic = params.copy()
params_monolithic["method"] = "monolithic"

params_fixedpoint = params.copy()
params_fixedpoint["method"] = "fixed point"

# params_staggered = params.copy()
# params_staggered["method"] = "staggered"

save_every = 1

# Problem definition
N = [16, 32, 64, 128, 256]
space_flag = len(N) > 1
length = 1e-2

time_mono = np.zeros(len(N))
time_fixedpoint = np.zeros(len(N))
# time_staggered = np.zeros(len(N))

skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)

def evalF(F): return skel_energy(F) + vol_skel(det(F))


def f_sur_solid(t):
    return Constant((0, 0))


def f_vol_solid(t):
    return Constant((0, 0))


def f_vol_mass(t):
    source = Expression(
        "(x[0]<0.5 * L)? 0.5 : 0", L=length, degree=6)
    return [source] + [Constant(0.0)] * (n_comps - 1)


nn = 0
while nn < len(N):

    print("Solving for {} elements per side".format(N[nn]))
    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(
        N[nn], length)

    NeumannMarkers = [TOP, RIGHT]
    RobinMarkers = []
    cookson_mono = Cookson(mesh, name, params_monolithic, evalF,
                           vol_energy, markers, NeumannMarkers, RobinMarkers)
    cookson_fixedpoint = Cookson(
        mesh, name, params_fixedpoint, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)
    # cookson_staggered = Cookson(mesh, name, params_staggered,
    #                             evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)

    def bcs_mono(t):
        return [DirichletBC(cookson_mono.V.sub(0).sub(0), Constant(0), markers, LEFT),
                DirichletBC(cookson_mono.V.sub(0).sub(
                    1), Constant(0), markers, BOTTOM)]

    def bcs_y_fixedpoint(t):
        return [DirichletBC(cookson_fixedpoint.Vs.sub(0), Constant(0), markers, LEFT),
                DirichletBC(cookson_fixedpoint.Vs.sub(
                    1), Constant(0), markers, BOTTOM)]

    def bcs_m_fixedpoint(t):
        return []


    from time import time
    cookson_mono.set_bcs(bcs_mono)
    cookson_mono.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_mono.solve()
    time_mono[nn] = time() - current_time
    cookson_fixedpoint.set_bcs([bcs_y_fixedpoint, bcs_m_fixedpoint])
    cookson_fixedpoint.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_fixedpoint.solve()
    time_fixedpoint[nn] = time() - current_time
    # cookson_staggered.set_bcs([bcs_y_staggered, bcs_m_staggered])
    # cookson_staggered.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    # current_time = time()
    # cookson_staggered.solve()
    # time_staggered[nn] = time() - current_time

    ti = params["t0"] + params["dt"]
    it = 0

    nn += 1

times = np.linspace(params["t0"] + params["dt"],
                    params["tf"], len(N))

print("\ntime solving monolithic formulation: ", time_mono)
print("\ntime solving fixedpoint formulation: ", time_fixedpoint)
# print("\ntime solving staggered formulation: ", time_staggered)

cmap = plt.get_cmap("viridis")
# Get relevant colors
# indexes_colors = [int(i) for i in np.linspace(30, 200, N)]
# colors = [cmap.colors[i] for i in indexes_colors]

plt.figure(1)
plt.plot(N, time_mono, label=r'Monolithic', c = cmap.colors[30])
plt.plot(N, time_fixedpoint, label=r'Fixed point', c=cmap.colors[-30])
# plt.plot(N, time_staggered, label=r'staggered')
plt.legend(loc='best')
plt.xlabel('Elements per side $N$')
plt.ylabel('Wall time [$s$]')
plt.savefig('time_compare.png', bbox_inches='tight')
plt.clf()
