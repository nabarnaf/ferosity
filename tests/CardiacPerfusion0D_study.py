"""
@author: Giulia Ferla, Micol Bassanini
"""

from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import Guccione
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog, LinxLog
from ferosity.Fibers import generateFibers
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import prolateGeometry
from ferosity.CSVInterpolation import CSVInterpolation
import numpy as np

set_log_active(False)
set_log_level(20)
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 4

kp = 1e4  # Pressure bulk
ks = 5e4  # Solid bulk 5e4

pressure_csv = "aortic_pressure_profile.csv"

n_comps = 1
beta = 1e-5 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))
semi_implicit_flag=False
params_mono = {"permeabilities": [1e-8] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.1] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 2,
          "degree mass": 1,
          "t0": 0,
          "tf": 0.4,
          "dt": 5e-4,
          "save every": 5,
          "export solutions": False,
          "geometry": "heart",
          "method": "monolithic",
          "scheme absolute tol": 1e-11,
          "scheme relative tol": 1e-7,
          "maxiter": 100,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": True,
          "compare iterations": True,
          "flag semi implicit":semi_implicit_flag}

params_fixedpoint = params_mono.copy()
params_fixedpoint["method"] = "fixed point"

params_stag = params_mono.copy()
params_stag["method"] = "staggered"


# Load mesh
geom = "prolate_h4_v2_ASCII"

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(geom)
NeumannMarkers = []
RobinMarkers = [BASE, EPICARD, ENDOCARD]

# Generate potentials
f0, s0, n0 = generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE)
guccione = Guccione(f0, s0, n0)

vol_sol = LinxLog(ks)
vol_energy = LinpLog(kp)


def energy_solid(F):
    return guccione(F) + vol_sol(det(F))


def f(t): return Constant((0, 0, 0))
def tf(t): return Constant((0, 0, 0))


# Build source term according to CSV file.
period = 0.8
csv = CSVInterpolation(pressure_csv, period)

volume = assemble(1 * dx(mesh))

rhof=params_mono["rhof"]

class Source:
    def __init__(self, cookson):
        self.cookson = cookson

    def __call__(self, t):

        out = n_comps * [0.0]

        p_correction = 6000  # Used because library doesn't support yet reference pressures
        p_in = 133.32 * csv(np.fmod(t, period)) - p_correction # mmHg to Pa
        p_out = self.cookson.getAveragePressure(
            0)  # Get average from first compartment
        delta_p = max(p_in - p_out, 0)  # Filter out backflow

        # Taken from zygote coronaries summing inlets and outlets independently
        Sout = 5.68788e-6
        Sin = 11.3918e-6
        factor = 2.0 / rhof  / (1.0 / Sout / Sout - 1.0 / Sin / Sin)
        out[-1] = sqrt(factor * delta_p) / volume
        return Constant(out)


cookson_monolithic = Cookson(mesh, "Cardiac monolithic", params_mono, energy_solid, vol_energy,
                  markers, NeumannMarkers, RobinMarkers)
cookson_monolithic.setFibers(f0, s0, n0)
cookson_fixedpoint= Cookson(mesh, "Cardiac fixed point", params_fixedpoint, energy_solid, vol_energy,
                  markers, NeumannMarkers, RobinMarkers)
cookson_fixedpoint.setFibers(f0, s0, n0)
cookson_stag= Cookson(mesh, "Cardiac staggered", params_stag, energy_solid, vol_energy,
                  markers, NeumannMarkers, RobinMarkers)
cookson_stag.setFibers(f0, s0, n0)
# Define problem handlers according to formulation

def bcs_mono(t): return []
cookson_monolithic.set_bcs(bcs_mono)

def bcs_y(t): return []
def bcs_m(t): return []
cookson_fixedpoint.set_bcs([bcs_y, bcs_m])
cookson_stag.set_bcs([bcs_y, bcs_m])
ts_mono = Source(cookson_monolithic)
ts_fixedpoint = Source(cookson_fixedpoint)
ts_stag=Source(cookson_stag)
cookson_monolithic.setup_loads(f, tf, ts_mono, None)
cookson_fixedpoint.setup_loads(f, tf, ts_fixedpoint, None)
cookson_stag.setup_loads(f, tf, ts_stag, None)

# Initial conditions
sol0y = Expression(["0", "0", "0"], degree = 1)
sol0m = Expression(["10"], degree = 1)
cookson_monolithic.initialize_solutions(sol0y, sol0m)
cookson_fixedpoint.initialize_solutions(sol0y, sol0m)
cookson_stag.initialize_solutions(sol0y, sol0m)

from time import time
current_time=time()
cookson_monolithic.solve()
time_mono=time()-current_time
current_time=time()
cookson_fixedpoint.solve()
time_fixedpoint=time()-current_time
current_time=time()
cookson_stag.solve()
time_stag=time()-current_time

iter_mono = cookson_monolithic.newton_iter
iter_fixedpoint = cookson_fixedpoint.iter         # fixed point
iter_fn_y = cookson_fixedpoint.newton_y           # newton iterations fixedpoint
iter_fn_m = cookson_fixedpoint.newton_mass        # newton iterations fixedpoint
iter_stagn_y = cookson_stag.newton_y
iter_stagn_m = cookson_stag.newton_mass
print("\nNumber of iterations of monolithic formulation", iter_mono)
print("\nNumber of iterations of fixed point formulation", iter_fixedpoint)
print("\nNumber of Newton iterations of fixed point formulation for displacement", iter_fn_y)
print("\nNumber of Newton iterations of fixed point formulation for added mass", iter_fn_m) 
print("\nNumber of Newton iterations of staggered formulation for displacement", iter_stagn_y)
print("\nNumber of Newton iterations of staggered formulation for added mass", iter_stagn_m) 

print("\n\nTime solving monolithic formulation:", time_mono)
print("\n\nTime solving fixed point formulation:", time_fixedpoint)
print("\n\nTime solving staggered formulation:", time_stag)

error_fixedpoint_y_L2 = [errornorm(cookson_monolithic.y_sol[i], cookson_fixedpoint.y_sol[i], "L2") for i in range(0, L, 30)]
error_fixedpoint_y_L2_inf = max(error_fixedpoint_y_L2)
error_fixedpoint_y_H1 = [errornorm(cookson_monolithic.y_sol[i], cookson_fixedpoint.y_sol[i], "H1") for i in range(0, L, 30)]
error_fixedpoint_y_H1_inf = max(error_fixedpoint_y_H1)

error_fixedpoint_mtot_L2 = [errornorm(cookson_monolithic.m_sol[i], cookson_fixedpoint.m_sol[i], "L2") for i in range(0, L, 30)]
error_fixedpoint_mtot_L2_inf = max(error_fixedpoint_mtot_L2)
error_fixedpoint_mtot_H1 = [errornorm(cookson_monolithic.m_sol[i], cookson_fixedpoint.m_sol[i], "H1") for i in range(0, L, 30)]
error_fixedpoint_mtot_H1_inf = max(error_fixedpoint_mtot_H1)

error_staggered_y_L2 = [errornorm(cookson_monolithic.y_sol[i], cookson_stag.y_sol[i], "L2") for i in range(0, L, 30)]
error_staggered_y_L2_inf = max(error_staggered_y_L2)
error_staggered_y_H1 = [errornorm(cookson_monolithic.y_sol[i], cookson_stag.y_sol[i], "H1") for i in range(0, L, 30)]
error_staggered_y_H1_inf = max(error_staggered_y_H1)

error_staggered_mtot_L2 = [errornorm(cookson_monolithic.m_sol[i], cookson_stag.m_sol[i], "L2") for i in range(0, L, 30)]
error_staggered_mtot_L2_inf = max(error_staggered_mtot_L2)
error_staggered_mtot_H1 = [errornorm(cookson_monolithic.m_sol[i], cookson_stag.m_sol[i], "H1") for i in range(0, L, 30)]
error_staggered_mtot_H1_inf = max(error_staggered_mtot_H1)

print("\nError L2 of fixed point solver in displacement:", error_fixedpoint_y_L2_inf)
print("\nError H1 of fixed point solver in displacement:", error_fixedpoint_y_H1_inf)
print("\nError L2 of fixed point solver in added mass:", error_fixedpoint_mtot_L2_inf)
print("\nError H1 of fixed point solver in added mass:", error_fixedpoint_mtot_H1_inf)

print("\nError L2 of staggered solver in displacement:", error_staggered_y_L2_inf)
print("\nError H1 of staggered solver in displacement:", error_staggered_y_H1_inf)
print("\nError L2 of staggered solver in added mass:", error_staggered_mtot_L2_inf)
print("\nError H1 of staggered solver in added mass:", error_staggered_mtot_H1_inf)
