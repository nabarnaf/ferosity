"""
@author: Giulia Ferla, Micol Bassanini
"""

import numpy as np
from dolfin import *
from ferosity.ConstitutiveModeling.SkeletonEnergies import CiarletGeymonat, NeoHookean
from ferosity.ConstitutiveModeling.VolumetricEnergies import LinpLog
from ferosity.Physics.Cookson import Cookson
from ferosity.MeshCreation import generateSquare
import matplotlib.pyplot as plt
from ferosity.GLOBAL_VARIABLES import *
from ferosity.MeshCreation import prolateGeometry

name = "compare"

set_log_active(False)

k1 = 2e3
k2 = 33
kp = 1e3  # 2e4
ks = 1e3  # Constant for pressure
semi_implicit_flag = False

n_comps = 3
beta = 1e-3 * (np.ones([n_comps, n_comps]) - np.identity(n_comps))


geometry = "square"  # square|heart


save_every = 1

params_mono = {"permeabilities": [1e-7] * n_comps,
          "rhof": 1e3,
          "rhos": 1e3,
          "muf": 0.035,
          "initial porosities": [0.035] * n_comps,
          "number compartments": n_comps,
          "compartments interaction": beta,
          "gamma": 1e-4,
          "venous return": 0,
          "degree solid": 1,
          "degree mass": 1,
          "t0": 0,
          "tf": 0.8,
          "dt": 1e-2,
          "export solutions": False,
          "geometry": geometry,
          "save every": save_every,
          "method": "monolithic",
          "scheme absolute tol": 1e-11,
          "scheme relative tol": 1e-7,
          "maxiter": 1000,
          "pressure model": "mono",
          "pressure bc use": False,
          "convergence_flag": False,
          "compare iterations": True,
          "flag semi implicit": semi_implicit_flag}

params_fixedpoint = params_mono.copy()
params_fixedpoint["method"] = "fixed point"

params_stag = params_mono.copy()
params_fixedpoint["method"] = "staggered"

# Problem definition
N = [2**3, 12, 2**4]
length = 1e-2
skel_energy = CiarletGeymonat(k1, k2)
vol_skel = LinpLog(kp)
vol_energy = LinpLog(ks)


def evalF(F): return skel_energy(F) + vol_skel(det(F))

def f_sur_solid(t):
    return Constant((0, 0))


def f_vol_solid(t):
    return Constant((0, 0))


def f_vol_mass(t):
    source = Expression(
        "(x[0]<0.5 * L)? 0.5 : 0", L=length, degree=6)
    return [source] + [Constant(0.0)] * (n_comps - 1)

l=len(N)
iteravg_mono=np.zeros(l)
iteravg_fixedpoint=np.zeros(l)
iteravg_fn_y=np.zeros(l)
iteravg_fn_m=np.zeros(l)
iteravg_stagn_y=np.zeros(l)
iteravg_stagn_m=np.zeros(l)

nn=0

while nn < l:
    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generateSquare(N[nn], length)

    NeumannMarkers = [TOP, RIGHT]
    RobinMarkers = []

    cookson_mono = Cookson(mesh, name, params_mono, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)
    cookson_fixedpoint = Cookson(mesh, name, params_fixedpoint, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)
    cookson_stag = Cookson(mesh, name, params_stag, evalF, vol_energy, markers, NeumannMarkers, RobinMarkers)


    def bcs_monolithic(t): return [DirichletBC(cookson_mono.V.sub(0).sub(0), Constant(0), markers, LEFT),
                     DirichletBC(cookson_mono.V.sub(0).sub(
                         1), Constant(0), markers, BOTTOM)]



    cookson_mono.set_bcs(bcs_monolithic)
    cookson_mono.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)

    # Compare time:
    from time import time
    current_time = time()
    cookson_mono.solve()
    time_mono= time() - current_time
    iter_mono = cookson_mono.newton_iter

    print("\n\nSolving fixed point formulation:")
    def bcs_y_fixedpoint(t): return [DirichletBC(cookson_fixedpoint.Vs.sub(0), Constant(0), markers, LEFT),
                      DirichletBC(cookson_fixedpoint.Vs.sub(
                          1), Constant(0), markers, BOTTOM)]

    def bcs_m_fixedpoint(t): return []

    def bcs_y_stag(t): return [DirichletBC(cookson_stag.Vs.sub(0), Constant(0), markers, LEFT),
                      DirichletBC(cookson_stag.Vs.sub(
                          1), Constant(0), markers, BOTTOM)]

    def bcs_m_stag(t): return []

    cookson_fixedpoint.set_bcs([bcs_y_fixedpoint, bcs_m_fixedpoint])
    cookson_fixedpoint.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_fixedpoint.solve()
    time_fixedpoint = time() - current_time

    print("\n\nSolving staggered formulation:")
    cookson_stag.set_bcs([bcs_y_stag, bcs_m_stag])
    cookson_stag.setup_loads(f_vol_solid, f_sur_solid, f_vol_mass, None)
    current_time = time()
    cookson_stag.solve()
    time_stag = time() - current_time

    iter_mono = cookson_mono.newton_iter
    iter_fixedpoint = cookson_fixedpoint.iter         # fixed point
    iter_fn_y = cookson_fixedpoint.newton_y           # Newton iterations fixedpoint
    iter_fn_m = cookson_fixedpoint.newton_mass        # Newton iterations fixedpoint
    iter_stagn_y = cookson_stag.newton_y
    iter_stagn_m = cookson_stag.newton_mass

    if l == 1:
        print("\nNumber of iterations of monolithic formulation", iter_mono)
        print("\nNumber of iterations of fixed point formulation", iter_fixedpoint)

        print("\nNumber of Newton iterations of fixed point formulation for displacement", iter_fn_y) 
        print("\nNumber of Newton iterations of fixed point formulation for added mass", iter_fn_m) 
        print("\nNumber of Newton iterations of staggered formulation for displacement", iter_stagn_y)
        print("\nNumber of Newton iterations of staggered formulation for added mass", iter_stagn_m) 
        print(iter_stagn_m)
        t0 = params["t0"] + params["dt"]
        tf = params["tf"]
        temp1 = np.linspace(t0, tf, len(iter_mono))
        temp2 = np.linspace(t0, tf, len(iter_fixedpoint))

        plt.plot(temp1, iter_mono, label=r'Iterations of monolithic')
        plt.plot(temp2, iter_fixedpoint, label=r'Iterations of fixed point')
        plt.plot(N, iter_fn_y, label=r'Avg Newton iterations of fixed point for y')
        plt.plot(N, iter_fn_m, label=r'Avg Newton iterations of fixed point for m')
        plt.plot(N, iter_stagn_y, label=r'Avg Newton iterations of staggered for y')
        plt.plot(N, iter_stagn_m, label=r'Avg Newton iterations of staggered for m')
        plt.legend(loc='best')
        plt.savefig('Iterations.png')
        plt.clf()


        print("\n\nTime solving monolithic formulation:", time_mono)
        print("\n\nTime solving fixed point formulation:", time_fixedpoint)
        print("\n\nTime solving staggered formulation:", time_stag)
    
    else:
        import statistics as st
        iteravg_mono[nn]=st.mean(iter_mono)
        iteravg_fixedpoint[nn]=st.mean(iter_fixedpoint)
        iteravg_fn_y[nn]=st.mean(iter_fn_y)
        iteravg_fn_m[nn]=st.mean(iter_fn_m)
        iteravg_stagn_y[nn]=st.mean(iter_stagn_y)
        iteravg_stagn_m[nn]=st.mean(iter_stagn_m)

        print("N = ", N[nn])

        # Check:
        print("\nNumber of iterations of monolithic formulation", iter_mono)
        print(iter_mono)
        print("\nNumber of iterations of fixed point formulation", iter_fixedpoint)
        print(iter_fixedpoint)
        print("\nNumber of Newton iterations of fixed point formulation for displacement", iter_fn_y) 
        print(iter_fn_y)
        print("\nNumber of Newton iterations of fixed point formulation for added mass", iter_fn_m) 
        print(iter_fn_m)
        print("\nNumber of Newton iterations of staggered formulation for displacement", iter_stagn_y)
        print(iter_stagn_y)
        print("\nNumber of Newton iterations of staggered formulation for displacement", iter_stagn_m) 
        print(iter_stagn_m)

        print("\nAverage number of iterations of monolithic formulation", iteravg_mono) 
        print("\nAverage number of iterations of fixed point formulation", iteravg_fixedpoint) 
        print("\nAverage number of Newton iterations of fixed point formulation for displacement", iteravg_fn_y) 
        print("\nAverage number of Newton iterations of fixed point formulation for added mass", iteravg_fn_m) 
        print("\nAverage number of Newton iterations of staggered formulation for displacement", iteravg_stagn_y) 
        print("\nAverage number of Newton iterations of staggered formulation for displacement", iteravg_stagn_m) 

    nn+=1

plt.plot(N, iteravg_mono, label=r'Avg iterations of monolithic')
plt.plot(N, iteravg_fixedpoint, label=r'Avg iterations of fixed point')
plt.plot(N, iteravg_fn_y, label=r'Avg Newton iterations of fixed point for y')
plt.plot(N, iteravg_fn_m, label=r'Avg Newton iterations of fixed point for m')
plt.plot(N, iteravg_stagn_y, label=r'Avg Newton iterations of staggered for y')
plt.plot(N, iteravg_stagn_m, label=r'Avg Newton iterations of staggered for m')
plt.legend(loc='best')
plt.savefig('Average iterations.png')
plt.clf()

