from sys import argv
from numpy import genfromtxt, average

# Parse inputs
files_in = argv[2]  # List of relevant files separated by a space, should be all files HEART_ROB_METHOD_PX_%.csv, so all loads.
outname = argv[1]

# Generate file list to parse
files = files_in.split("\n")  # Turn them into a list
assert all([".csv" in file for file in files]), "Must give csv files with results"

# For each file, find dofs, nlits, tot krylov, avg krylov and time
outputs = []
for file in files:

    array = genfromtxt("output/{}".format(file), delimiter=",", skip_header=1)
    if len(array.shape) == 1: # This happens in DT where there is only one entry
        array = array.reshape([1, array.shape[0]])
    if array.shape[0] < 1:
        continue
    idx_time, idx_nl, idx_l, idx_time = 0, 1, 2, 3  # Indexes for CSV file
    file = file.replace(".csv", "").replace("\n", "") # Filter file format and possible a new line character
    if "KS" in file or "DT" in file: 
        _, _, _, degree_str, method, load_str = file.split('_')  # This is HEART STABILITY KS/DT P1 METHOD LOAD
    else:
        _, _, degree_str, method, load_str = file.split('_')  # This is HEART STABILITY P1 METHOD LOAD
    degree = int(degree_str[1])
    rob_param = float(load_str) # Name is the same as in the other scripts

    # Get fields
    converged = not any(array[:,idx_nl]<1)  # See divergence if at least one of the nl_its is 0
    nl_its = average(array[:, idx_nl])
    tot_krylov = average(array[:, idx_l])
    avg_krylov = tot_krylov / nl_its
    time = average(array[:, idx_time])

    # Note inclusion of robustness parameters
    outputs.append([rob_param, converged, nl_its, tot_krylov, avg_krylov, time])


# Sort by rob_param in case files weren't in order
outputs.sort(key=lambda x: float(x[0]))

# Save results in a nice CSV file
with open(outname, 'w') as f:
    f.write('rob-param,converged,nl-its,tot-krylov,avg-krylov,time\n')
    for out in outputs:
        rob_param, converged, nl_its, tot_krylov, avg_krylov, time = out
        f.write("{},{},{},{},{},{}\n".format(rob_param,
                                                converged, nl_its, tot_krylov, avg_krylov, time))
