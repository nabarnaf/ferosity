mkdir -p results

# Cook
for DEG in 1 2
do 
	for M in Newton Newton-inexact BFGS BFGS-inexact
	do 
		OUT="results/result-rob-cook-${M}-P${DEG}.csv"
		printf "" > $OUT
		python process-rob-cook-twist.py "COOK" "${M}" "$DEG"  "$(ls output | grep COOK_ROB | grep "${M}_" | grep "P${DEG}")" "${OUT}"  
	done 
done

# Twist
for M in Newton Newton-inexact BFGS BFGS-inexact
do 
	OUT="results/result-rob-twist-${M}.csv"
	printf "" > $OUT
	#python process-rob-cook-twist.py "TWIST" "${M}" "$DEG"  "$(ls output | grep TWIST_ROB | grep "${M}_")" "${OUT}"  
done 

# Heart AS robustness
for DEG in 1 2
do 
	for M in Newton Newton-inexact BFGS BFGS-inexact
	do 
		OUT="results/result-rob-heart-${M}-P${DEG}.csv"
		printf "" > $OUT
		python process-rob-heart.py "${OUT}" "$(ls output | grep HEART_STAB | grep "${M}_" | grep "P${DEG}" | grep ".csv" | grep -v "KS"| grep -v "DT")"  # Note: grep -v, negative match
	done 
done

# Heart KS robustness
for DEG in 1 2
do 
	for M in Newton Newton-inexact BFGS BFGS-inexact
	do 
		OUT="results/result-rob-heart-ks-${M}-P${DEG}.csv"
		printf "" > $OUT
		python process-rob-heart.py "${OUT}" "$(ls output | grep HEART_STAB | grep "${M}_" | grep "P${DEG}" | grep ".csv" | grep "KS")"
	done 
done

# Heart DT robustness
for DEG in 1 2
do 
	for M in Newton Newton-inexact BFGS BFGS-inexact
	do 
		OUT="results/result-rob-heart-dt-${M}-P${DEG}.csv"
		printf "" > $OUT
		python process-rob-heart.py "${OUT}" "$(ls output | grep HEART_STAB | grep "${M}_" | grep "P${DEG}" | grep ".csv" | grep "DT")"
	done 
done

