from sys import argv

# Parse inputs
problem = argv[1]
method = argv[2]
degree = argv[3]
files_in = argv[4]  # List of relevant files separated by a \n
outname = argv[5]

# Generate file list to parse
if problem == "COOK":
    base = "{}_PERF_P{}_{}".format(problem, degree, method)
else:  # method == TWIST
    base = "{}_PERF_{}".format(problem, method)
files = files_in.split("\n")  # Turn them into a list

# For each file, find dofs, nlits, tot krylov, avg krylov and time
outputs = []
for file in files:
    ff = None
    with open("output/{}".format(file)) as f:
        ff = f.readlines()

    # First get dofs line as it is where outputs start
    i_dofs = 0
    for i in range(len(ff)):
        if 'Dofs' in ff[i]:
            i_dofs = i

    def get_number(string):
        # Return the number after the equals sign
        return string.split("=")[1].replace("\n", "")
    # Get dofs
    dofs = get_number(ff[i_dofs])
    converged = get_number(ff[i_dofs+1])
    nl_its = get_number(ff[i_dofs+2])
    tot_krylov = get_number(ff[i_dofs+3])
    avg_krylov = get_number(ff[i_dofs+4])
    time = get_number(ff[i_dofs+5])

    if method == "BFGS-inexact":  # QN doesn't give krylov iterations
        its = []
        for i in range(len(ff)):
            if 'Linear solve converged' in ff[i]:
                its.append(int(ff[i].split('iterations ')[-1]))
        tot_krylov = sum(its)
        avg_krylov = tot_krylov/float(nl_its)

    outputs.append([dofs, converged, nl_its, tot_krylov, avg_krylov, time])

# Sort by dofs in case files weren't in order
outputs.sort(key=lambda x: int(x[0]))

# Save results in a nice CSV file
with open(outname, 'w') as f:
    f.write('dofs,converged,nl-its,tot-krylov,avg-krylov,time\n')
    for out in outputs:
        dofs, converged, nl_its, tot_krylov, avg_krylov, time = out
        f.write("{},{},{},{},{},{}\n".format(dofs, converged, nl_its, tot_krylov, avg_krylov, time))
