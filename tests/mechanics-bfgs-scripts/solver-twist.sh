# This script should be sourced from the automatic launch scripts.
# It can be replaced with another 'solve_problem' function for use in HPC.
function solve_problem {
	CORES=$1
	REFS=$2
	DEGREE=$3
	METHOD=$4
	OUTPUT_FILE=$5
	DENOM=$6

	tsp sh -c "mpirun -np $CORES python ../Twist.py $REFS $DEGREE $METHOD $DENOM | tee $OUTPUT_FILE"
}
