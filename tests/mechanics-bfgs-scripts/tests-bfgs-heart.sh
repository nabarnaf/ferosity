# Test Mechanics with all methods
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Init script
cd ${SCRIPT_DIR}
mkdir -p output

# Load execution function 'solve_problem'
RUN_FILE=${SCRIPT_DIR}/solver-heart.sh
RUN_FILE_STAB=${SCRIPT_DIR}/solver-heart-stability.sh
if [ "$#" -gt 0 ]; then  # Check if input file is given
  RUN_FILE=$1
fi
source ${RUN_FILE}

METHODS="Newton Newton-inexact BFGS BFGS-inexact"
MESHES="prolate_h4_v2_ASCII prolate_h4_v2_ASCII_ref prolate_4mm prolate_4mm_ref"
ASS="1e4 2e4 3e4 4e4 5e4 6e4 7e4 8e4 9e4 1e5"
KSS="5e4 1e5 1.5e5 2e5 2.5e5 3e5"
DTS="1e-2 2e-2 3e-2 4e-2 5e-2 6e-2 7e-2 8e-2 9e-2 1e-1"
AS_DEFAULT="1e4"
KS_DEFAULT="5e4"
DT_DEFAULT="1e-2"
TF_DEFAULT="0.8"
MESH_STAB="prolate_4mm"
DEGREES="1 2"
N_CORES="1 2 4 8 16 32"


for MESH in $MESHES
do
	for METHOD in $METHODS
	do
		for DEGREE in $DEGREES
		do
			# Default params for performance tests
			CORES=1
			AS=${AS_DEFAULT}
			KS=${KS_DEFAULT}
			DT=${DT_DEFAULT}
			TF=${TF_DEFAULT}
			OUTITERS="output/HEART_PERF_P${DEGREE}_${METHOD}_${MESH}.csv"
			OUTPUT="output/HEART_PERF_P${DEGREE}_${METHOD}_${MESH}.out"
			#solve_problem ${CORES} ${MESH} ${DEGREE} ${METHOD} ${AS} ${KS} ${DT} ${TF} ${OUTITERS} ${OUTPUT}
		done
	done
done

for METHOD in $METHODS
do
	for DEGREE in $DEGREES
	do
	    DT=${DT_DEFAULT}
	    TF="0.1"
	    for AS in $ASS
	    do

  		CORES=1
		KS=${KS_DEFAULT}
                OUTITERS="output/HEART_STABILITY_P${DEGREE}_${METHOD}_${AS}.csv"
  		OUTPUT="output/HEART_STABILITY_P${DEGREE}_${METHOD}_${AS}.out"
  		#solve_problem ${CORES} ${MESH_STAB} ${DEGREE} ${METHOD} ${AS} ${KS} ${DT} ${TF} ${OUTITERS} ${OUTPUT}
	    done
	    for KS in $KSS
	    do

  		CORES=1
		AS=${AS_DEFAULT}
                OUTITERS="output/HEART_STABILITY_KS_P${DEGREE}_${METHOD}_${KS}.csv"
  		OUTPUT="output/HEART_STABILITY_KS_P${DEGREE}_${METHOD}_${KS}.out"
  		#solve_problem ${CORES} ${MESH_STAB} ${DEGREE} ${METHOD} ${AS} ${KS} ${DT} ${TF} ${OUTITERS} ${OUTPUT}
	    done
	    for DTTEST in $DTS
	    do

  		CORES=1
		AS=${AS_DEFAULT}
                OUTITERS="output/HEART_STABILITY_DT_P${DEGREE}_${METHOD}_${DTTEST}.csv"
  		OUTPUT="output/HEART_STABILITY_DT_P${DEGREE}_${METHOD}_${DTTEST}.out"
		# Do only first time step, so TF=DTTEST
  		solve_problem ${CORES} ${MESH_STAB} ${DEGREE} ${METHOD} ${AS_DEFAULT} ${KS_DEFAULT} ${DTTEST} ${DTTEST} ${OUTITERS} ${OUTPUT}
	    done


	done
done


# SCALABILITY
echo "" > TEMP  # Used as temp iters file
for METHOD in ${METHODS}
do
	for DEGREE in ${DEGREES}
	do
		for CORES in ${N_CORES}
		do
			OUTPUT="output/HEART_SCALABILITY_P${DEGREE}_${METHOD}_${CORES}.out"
			OUTITERS="output/HEART_SCALABILITY_P${DEGREE}_${METHOD}_${CORES}.csv"
			AS=${AS_DEFAULT}
			KS=${KS_DEFAULT}
			DT=${DT_DEFAULT}
			TF=0.1
			#solve_problem $CORES "prolate_4mm_ref" $DEGREE $METHOD $AS $KS $DT $TF $OUTITERS $OUTPUT
		done
	done
done
