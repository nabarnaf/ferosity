from sys import argv

out = []
with open(argv[1], 'r') as f:
    lines = f.readlines()
    for l in lines[1:]:
        out.append(l.split(','))

out = out[:10] # First ten
nlits = [float(o[1]) for o in out]
tkits = [float(o[2])/float(o[1]) for o in out]
times = [float(o[3]) for o in out]
avg = lambda OO: sum(OO)/len(OO)
print("{}, {}, {}".format(avg(nlits), avg(tkits), avg(times)))
