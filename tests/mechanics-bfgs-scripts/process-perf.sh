mkdir -p results
for DEG in 1 2
do 
	for M in Newton Newton-inexact BFGS BFGS-inexact
	do 
		OUT="results/result-perf-cook-${M}-P${DEG}.csv"
		#python process-perf-cook-twist.py "COOK" "${M}" "$DEG"  "$(ls output | grep COOK_PERF | grep "${M}_" | grep "P${DEG}")" "${OUT}"  
	done 
done


for M in Newton Newton-inexact BFGS BFGS-inexact
do 
	OUT="results/result-perf-twist-${M}.csv"
	#python process-perf-cook-twist.py "TWIST" "${M}" "$DEG"  "$(ls output | grep TWIST_PERF | grep "${M}_")" "${OUT}"  
done 

for DEG in 1 2; do
	for M in Newton Newton-inexact BFGS-inexact BFGS; do
		OUTFILE="results/result-perf-heart-${M}-P${DEG}.csv"
		echo "mesh,nl-its,avg-krylov,time" > $OUTFILE
		for MESH in prolate_h4_v2_ASCII prolate_4mm prolate_h4_v2_ASCII_ref prolate_4mm_ref; do
			FILENAME="output/HEART_PERF_P${DEG}_${M}_${MESH}.csv"
			RES=$(python heart-10steps-avg.py $FILENAME)
			echo "${MESH},${RES}" >> $OUTFILE
		done
	done
done


