# This script should be sourced from the automatic launch scripts.
# It can be replaced with another 'solve_problem' function for use in HPC.
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function solve_problem {
	CORES=$1
	MESHTYPE=$2
	DEGREE=$3
	METHOD=$4
	AS=$5
	KS=$6
	DT=$7
	TF=$8
	OUTITERS=$9
	OUTPUT_FILE=${10}

	tsp sh -c "mpirun -np $CORES python ../Mechanics.py $MESHTYPE $DEGREE $METHOD $AS $KS $DT $TF $OUTITERS | tee ${SCRIPT_DIR}/${OUTPUT_FILE}"
}
