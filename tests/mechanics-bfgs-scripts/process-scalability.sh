mkdir -p results
for DEG in 1 2
do 
	for M in Newton Newton-inexact BFGS BFGS-inexact
	do 
		OUT="results/result-scalab-cook-${M}-P${DEG}.csv"
		python process-scalability-cook-twist.py "COOK" "${M}" "$DEG"  "$(ls output | grep COOK_SCALABILITY | grep "${M}_" | grep "P${DEG}")" "${OUT}"  
	done 
done


for M in Newton Newton-inexact BFGS BFGS-inexact
do 
	OUT="results/result-scalab-twist-${M}.csv"
	python process-scalability-cook-twist.py "TWIST" "${M}" "$DEG"  "$(ls output | grep TWIST_SCALABILITY | grep "${M}_")" "${OUT}"  
done 

for DEG in 1 2; do
	for M in Newton Newton-inexact BFGS-inexact BFGS; do
		OUT="results/result-scalab-heart-${M}-P${DEG}.csv"
		echo "cores,nl-its,avg-krylov,time" > $OUT
		for CORES in 1 2 4 8 16 32; do
			FILENAME="output/HEART_SCALABILITY_P${DEG}_${M}_${CORES}.csv"
			AVG=$(python heart-10steps-avg.py $FILENAME)
			echo "${CORES},${AVG}" >> $OUT
		done
	done
done

