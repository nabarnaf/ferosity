# Test Twist with all methods
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Load execution function 'solve_problem'
RUN_FILE=${SCRIPT_DIR}/solver-twist.sh
if [ "$#" -gt 0 ]; then  # Check if input file is given
  RUN_FILE=$1
fi
source ${RUN_FILE}

METHODS=''Newton' 'Newton-inexact' 'BFGS-inexact' 'BFGS''
NREFS='2 3 4 5 6 7 8'
TWISTFRACS='6 5 4 3 2'
CORES='4 8 16 32 64 128'

DEFAULT_DENOM=6
for METHOD in $METHODS
do


	echo ""
	echo "SIMULATION FOR METHOD $METHOD, P2 elements"
	for N in $NREFS
	do
		OUT="output/TWIST_PERF_${METHOD}_${N}"
		solve_problem 1 $N 2 $METHOD $OUT $DEFAULT_DENOM
		# mpirun -np 1 python3 Twist.py $N 2 $METHOD $DEFAULT_DENOM | tee $OUT
	done
done
echo "Twist robustness"

for METHOD in $METHODS
do

	echo "" > $OUT
	echo ""
	echo "SIMULATION FOR METHOD $METHOD, P2 elements"
	for val in $TWISTFRACS
	do
		OUT="output/TWIST_ROBUSTNESS_${METHOD}_${val}"
		echo ""
		echo "============================= val = $val"
		solve_problem 1 4 2 $METHOD $OUT $val
	done
done

echo "Twist Scalability"
for METHOD in $METHODS
do
	for NCORE in $CORES
	do
		echo ""
		echo "SIMULATION FOR SCALABILITY OF $METHOD, P2 elements"
		OUT="output/TWIST_SCALABILITY_${METHOD}_${NCORE}"
		echo ""
		#solve_problem $NCORE 8 2 $METHOD $OUT $DEFAULT_DENOM
		# mpirun -np $NCORE python3 Twist.py 8 2 $METHOD | tee $OUT
	done
done
