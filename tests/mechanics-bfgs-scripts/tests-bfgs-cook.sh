# Test Cook with all methods
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Load execution function 'solve_problem'
# Signature works like this: mpirun -np $1 python3 Cook.py $2 $3 $4 $6 | tee $5
RUN_FILE=${SCRIPT_DIR}/solver-cook.sh
if [ "$#" -gt 0 ]; then  # Check if input file is given
  RUN_FILE=$1
fi
source ${RUN_FILE}

METHODS=''Newton' 'Newton-inexact' 'BFGS' 'BFGS-inexact''
NREFS_P1='4 8 12 16 20 24'
NREFS_P2='2 4 6 8 10 12'
LOADS='1e5 1e6 1.25e6 1.5e6 1.75e6 2e6'

DEFAULT_LOAD='1e5'
DEFAULT_CORES=1
mkdir -p output
for METHOD in $METHODS
do
	echo ""
	echo "SIMULATION FOR METHOD $METHOD, P1 elements"
	for N in ${NREFS_P1}
	do
		OUT="output/COOK_PERF_P1_${METHOD}_${N}"
		solve_problem ${DEFAULT_CORES} $N 1 $METHOD $OUT $DEFAULT_LOAD
	done

	echo ""
	echo "SIMULATION FOR METHOD $METHOD, P2 elements"
	for N in ${NREFS_P2}
	do
		OUT="output/COOK_PERF_P2_${METHOD}_${N}"
		solve_problem ${DEFAULT_CORES} $N 2 $METHOD $OUT $DEFAULT_LOAD
	done
done

# Test Cook robustness
echo "Cook robustness P1"
for METHOD in $METHODS
do

	echo "" > $OUT
	echo ""
	echo "SIMULATION FOR METHOD $METHOD, P1 elements"
	for val in $LOADS
	do
		OUT="output/COOK_ROBUSTNESS_P1_${METHOD}_${val}"
		echo ""
		echo "============================= val = $val"
		#solve_problem ${DEFAULT_CORES} 12 1 $METHOD $OUT $val
	done
done


echo "Cook robustness P2"
for METHOD in 'Newton' 'Newton-inexact' 'BFGS-inexact' 'BFGS'
do
	echo "" > $OUT
	echo ""
	echo "SIMULATION FOR METHOD $METHOD, P2 elements"
	for val in $LOADS
	do
		OUT="output/COOK_ROBUSTNESS_P2_${METHOD}_${val}"
		echo ""
		echo "============================= val = $val"
		#solve_problem ${DEFAULT_CORES} 6 2 $METHOD $OUT $val
	done
done

echo "Cook Scalability"
for METHOD in 'Newton' 'Newton-inexact' 'BFGS-inexact' 'BFGS'
do
	for NCORE in 8 16 32 64 128
	do
		echo ""
		echo "SIMULATION FOR SCALABILITY OF $METHOD, P1 elements"
		OUT="output/COOK_SCALABILITY_P1_${METHOD}_${NCORE}"
		echo ""
		#solve_problem $NCORE 24 1 $METHOD $OUT $DEFAULT_LOAD
		# mpirun -np $NCORE python3 Cook.py 24 1 $METHOD | tee $OUT

		echo ""
		echo "SIMULATION FOR SCALABILITY OF $METHOD, P2 elements"
		OUT="output/COOK_SCALABILITY_P2_${METHOD}_${NCORE}"
		echo ""
		#solve_problem $NCORE 12 2 $METHOD $OUT $DEFAULT_LOAD
		# mpirun -np $NCORE python3 Cook.py 12 2 $METHOD | tee $OUT
	done
done
