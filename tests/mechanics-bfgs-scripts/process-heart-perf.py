from sys import argv

# Initialize arrays
nl_its = None
tk_its = None
ak_its = None
s_its = None

# iteration files
with open(argv[1], 'r') as f:
    nl_its = f.read().split('\n')

with open(argv[2], 'r') as f:
    tk_its = f.read().split('\n')

with open(argv[3], 'r') as f:
    ak_its = f.read().split('\n')

with open(argv[4], 'r') as f:
    s_its = f.read().split('\n')

# Filter empty strings
nl_its = [i for i in nl_its if len(i) > 0]
tk_its = [i for i in tk_its if len(i) > 0]
ak_its = [i for i in ak_its if len(i) > 0]
s_its = [i for i in s_its if len(i) > 0]

OUTNAME = argv[5]

OUT = ["nonlinear_its,total_krylov,average_krylov,time\n"]
for nl, tk, ak, s in zip(nl_its, tk_its, ak_its, s_its):
    OUT.append("{},{},{},{}\n".format(nl, tk, ak, s))

if len(OUT) > 0:
    OUT[-1].replace("\n", "")
    with open(OUTNAME, "w") as f:
        f.writelines(OUT)
