# Process output for timings and iterations
mkdir -p results

bash process-perf.sh
bash process-rob.sh
bash remove-diverged-from-results.sh
