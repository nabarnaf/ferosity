# Must be run as ". setup.sh", so that it considers the new variables
# in the current shell and not within the scope of the script.
# Also feasible: "source setup.py"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

export PYTHONPATH="$(echo $PYTHONPATH):${SCRIPT_DIR}/lib"
export FEROSITY_MESHES_DIR="${SCRIPT_DIR}/meshes"
export FEROSITY_OUTPUT_DIR="${SCRIPT_DIR}/output"
