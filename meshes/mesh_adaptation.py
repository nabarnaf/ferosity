# Routine by Francesco Regazzoni
#!/usr/bin/env python3
import numpy
import dolfin as df
import numpy as np
import meshio
import os

# TO DO IN SERIAL
# move into the right dir
# os.chdir("../meshes")

#filename = "sa_left_ventricle_smooth_1p5mm_fenics"
#tags = "gmsh:geometrical"
# filename = "prolate_h4_v2_ASCII"
filename = "prolate_4mm_ref"
tags = "gmsh:physical"
#    filename = "data_geometries/prolate/structure_mm"
#    tags = "gmsh:physical"


mesh_from_file = meshio.read(filename + ".msh")
# THIS FROM DISCOURSE.FENICSPROJECT


def create_mesh(mesh, cell_type, prune_z=False, has_data=False):
    cells = mesh.get_cells_type(cell_type)
    if has_data:
        cell_data = mesh.get_cell_data("gmsh:geometrical", cell_type)
        out_mesh = meshio.Mesh(points=mesh.points, cells={cell_type: cells}, cell_data={
                               "name_to_read": [cell_data]})
    else:
        out_mesh = meshio.Mesh(points=mesh.points, cells={cell_type: cells})
    if prune_z:
        out_mesh.prune_z_0()
    return out_mesh


line_mesh = create_mesh(mesh_from_file, "triangle", prune_z=False, has_data=True)
meshio.write(filename + "_bm.xdmf", line_mesh)

triangle_mesh = create_mesh(mesh_from_file, "tetra", prune_z=False, has_data=False)
meshio.write(filename + ".xdmf", triangle_mesh)


# THIS FROM REGA
# meshio.write(filename + ".xdmf", meshio.Mesh(points=msh.points,
# cells={"tetra": msh.cells["tetra"]}))

# Apparently interface changed a bit. 06/2021 [NB], cells[0]: triangles, cells[1]: tetra.
# meshio.write(filename + ".xdmf", meshio.Mesh(points=msh.points,
# cells={"tetrahedron", msh.cells[1]}))
# meshio.write(filename + "_bm.xdmf", meshio.Mesh(points=msh.points, cells={"triangle": msh.cells[0]},
# cell_data={"triangle": {"name_to_read": msh.cell_data["triangle"][tags]}}))

# os.chdir("../tests")

# CAN BE DONE IN PARALLEL

# xdmf_meshfile = "../meshes/" + filename + ".xdmf"
# xdmf_meshfile_bm = "../meshes/" + filename + "_bm.xdmf"
# mesh = df.Mesh()
# with df.XDMFFile(xdmf_meshfile) as infile:
#     infile.read(mesh)
# mvc = df.MeshValueCollection("size_t", mesh, 2)
# with df.XDMFFile(xdmf_meshfile_bm) as infile:
#     infile.read(mvc, "name_to_read")
# boundary_markers = df.cpp.mesh.MeshFunctionSizet(mesh, mvc)
#
#
#
#
# V = df.FunctionSpace(mesh,"CG",1)
# bc1 = df.DirichletBC(V, df.Constant(0.0), boundary_markers, 20)
# bc2 = df.DirichletBC(V, df.Constant(1.0), boundary_markers, 10)
# bc3 = df.DirichletBC(V, df.Constant(1.0), boundary_markers, 50)
#
# phi = df.Function(V)
# phi_trial = df.TrialFunction(V)
# psi = df.TestFunction(V)
#
# df.solve(df.dot(df.grad(phi_trial), df.grad(psi))*df.dx == df.Constant(0.0)*psi*df.dx, phi, [bc1, bc2])
# print("DONE")
