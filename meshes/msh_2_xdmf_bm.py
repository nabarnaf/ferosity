from sys import argv
assert len(argv)>=1, "Please provide mesh name (no extension)"
filename = argv[1]

import meshio


def create_mesh(mesh, cell_type, prune_z=False):
    cells = mesh.get_cells_type(cell_type)
    cell_data = mesh.get_cell_data("gmsh:physical", cell_type)
    out_mesh = meshio.Mesh(points=mesh.points, cells={
                           cell_type: cells}, cell_data={"name_to_read": [cell_data]})
    if prune_z:
        out_mesh.prune_z_0()
    return out_mesh

mesh_from_file = meshio.read("{}.msh".format(filename))
triangle_mesh = create_mesh(mesh_from_file, "tetra", prune_z=False)
line_mesh = create_mesh(mesh_from_file, "triangle", prune_z=False)
meshio.write("{}.xdmf".format(filename), triangle_mesh)
meshio.write("{}_bm.xdmf".format(filename), line_mesh)

